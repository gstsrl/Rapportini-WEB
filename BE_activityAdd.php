<?php
    error_reporting(0);
    require_once './SYS_validatingPostFunction.php';
    require_once './SYS_paramClass.php';
    require_once './SYS_mysqliConnClass.php';
    require_once './SYS_queryClass.php';
    require_once './SYS_loginClass.php';
    require_once './SYS_statusCode.php';


    $submit_POST = is_valid_post_string('submit');
    $activityOfferta_POST = is_valid_post_string('activityOfferta');
    $activityDuration_POST = is_valid_post_string('activityDuration');
    $activityDescription_POST = is_valid_post_string('activityDescription');
    $activityDay_POST = is_valid_post_string('activityDay');
    $activityMonth_POST = is_valid_post_string('activityMonth');
    $activityYear_POST = is_valid_post_string('activityYear');
    $activityUser_POST = is_valid_post_string('activityUser');
    $activityType_POST = is_valid_post_string('activityType');
    

    if ($submit_POST['isSetted'] && $submit_POST['isValid']) {

        //This makes sure they did not leave any fields blank
        if (!( $activityOfferta_POST['isSetted'] && $activityOfferta_POST['isValid']) ||
            !( $activityDuration_POST['isSetted'] && $activityDuration_POST['isValid']) ||
            !( $activityDescription_POST['isSetted'] && $activityDescription_POST['isValid']) 
        ) {
            $return["retCode"] = statusCode::$fieldNotSetted;
        }else{
          
            $parametri = new Params();
            $mysqlConn = new mysqliConnClass($parametri);
            $mysqliConn = $mysqlConn->connect();
            
            $month = date('n');
            if ($activityMonth_POST['isSetted'] && $activityMonth_POST['isValid']) {
                $month = $activityMonth_POST['value'];
            }
    
            $year = date('Y');
            if ($activityYear_POST['isSetted'] && $activityYear_POST['isValid']) {
                $year = $activityYear_POST['value'];
            }

            $day = date('j');
            if ($activityDay_POST['isSetted'] && $activityDay_POST['isValid']) {
                $day = $activityDay_POST['value'];
            }
            
            $user = loginClass::getUidUser();
            if ($activityUser_POST['isSetted'] && $activityUser_POST['isValid']) {
                $user = $activityUser_POST['value'];
            }

            //if (!queryClass::checkOfficeExistence($mysqliConn, $codCustomer_POST['value'],$codOffice_POST['value']) ) {
         
                // now we insert it into the database
                if (queryClass::addUserDailyActivities($mysqliConn, $activityOfferta_POST['value'],$activityType_POST['value'],$activityDuration_POST['value'], $activityDescription_POST['value'],$year,$month,$day,$user)) {
                    $return["retCode"] = statusCode::$inserted;
                }else{
                    $return["retCode"] = statusCode::$notInserted; 
                }
            //}else{
            //    $return["retCode"] = statusCode::$olreadyUsed;
            //}
        }
    } else {
        $return["retCode"] = statusCode::$actionNotSetted;
    }
    $mysqlConn->disconnect();
    $return["json"] = json_encode($return);
    echo json_encode($return);
?> 