
<?php
error_reporting(0);

require_once './SYS_validatingPostFunction.php';
require_once './SYS_paramClass.php';
require_once './SYS_mysqliConnClass.php';
require_once './SYS_queryClass.php';
require_once './SYS_loginClass.php';
require_once './SYS_statusCode.php';


$month_POST = is_valid_post_integer('month');
$month = date('n');
if ($month_POST['isSetted'] && $month_POST['isValid']) {
    $month=$month_POST['value'];
}

$year_POST = is_valid_post_integer('year');
$year = date('Y');
if ($year_POST['isSetted'] && $year_POST['isValid']) {
   $year =$year_POST['value'];
}

$day_POST = is_valid_post_integer('day');
$day = date('j');
if ($day_POST['isSetted'] && $day_POST['isValid']) {
    $day=$day_POST['value'];
}

$user_POST = is_valid_post_string('user');
$user = loginClass::getUidUser();
if ($user_POST['isSetted'] && $user_POST['isValid']) {
    $user=$user_POST['value'];
}

$limit_POST = is_valid_post_integer('limit');
$offset_POST = is_valid_post_integer('offset');
$sort_POST = is_valid_post_string('sort');
$order_POST = is_valid_post_string('order');
$search_POST = is_valid_post_string('search');

$limit = 5;
if ($limit_POST['isSetted'] && $limit_POST['isValid']) {
    if(limit <0){
        $limit ="";
    }else{
        $limit = $limit_POST['value'];
    }
}

$offset = 0;
if ($offset_POST['isSetted'] && $offset_POST['isValid']) {
    $offset = $offset_POST['value'];
}

$sort = "";
if ($sort_POST['isSetted'] && $sort_POST['isValid']) {
    $sort = $sort_POST['value'];
}

$order = "asc";
if ($order_POST['isSetted'] && $order_POST['isValid']) {
    $order = $order_POST['value'];
}

$search = "";
if ($search_POST['isSetted'] && $search_POST['isValid']) {
    $search = $search_POST['value'];
}

$parametri = new Params();
$mysqlConn = new mysqliConnClass($parametri);
$mysqliConn = $mysqlConn->connect();

$tot_element=queryClass::countUserDailyActivities($mysqliConn, $search,$day,$month, $year, $user);
$list_element=queryClass::listUserDailyActivities($mysqliConn, $limit, $offset, $sort, $order, $search,$day,$month, $year, $user);

$mysqlConn->disconnect();
echo "{" ;
echo '"total": ' . $tot_element . ',';
echo '"rows": ';
echo json_encode($list_element);
echo "}"; 
?>