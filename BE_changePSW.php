<?php

    error_reporting(0);

    require_once './SYS_validatingPostFunction.php';
    require_once './SYS_paramClass.php';
    require_once './SYS_mysqliConnClass.php';
    require_once './SYS_queryClass.php';
    require_once './SYS_loginClass.php';
    require_once './SYS_statusCode.php';


    $submit_passOld = is_valid_post_string('passOld');
    $submit_pass = is_valid_post_string('pass');
    $submit_pass2 = is_valid_post_string('pass2');
    $submit_username = is_valid_post_string('username');
    $submit_POST = is_valid_post_string('submit');
   
   

    

    if ($submit_POST['isSetted'] && $submit_POST['isValid']) {

        //This makes sure they did not leave any fields blank
        if (!( $submit_username['isSetted'] && $submit_username['isValid']) ||
                !( $submit_passOld['isSetted'] && $submit_passOld['isValid']) ||
                !( $submit_pass['isSetted'] && $submit_pass['isValid']) ||
                !( $submit_pass2['isSetted'] && $submit_pass2['isValid'])
        ) {
            $return["retCode"] = statusCode::$fieldNotSetted;
        }else{
          

            $parametri = new Params();
            $mysqlConn = new mysqliConnClass($parametri);
            $mysqliConn = $mysqlConn->connect();

            if (!queryClass::checkUsernameExistence($mysqliConn, $submit_username['value']) ) {
                
                

                // this makes sure both passwords entered match
                if ($submit_pass['value'] === $submit_pass2['value']) {
                    
                                       
                    if (queryClass::changePassword($mysqliConn, $submit_username['value'],$submit_passOld['value'], $submit_pass['value'])) {
                        $return["retCode"] = statusCode::$inserted;
                    }else{
                       $return["retCode"] = statusCode::$notInserted; 
                    }
                
                 
                }else{
                    $return["retCode"] = statusCode::$passwordNotMatch;
                }
                
            
                
            }else{
                $return["retCode"] = statusCode::$userOlreadyExist;
            }
            
        }
    } else {
        $return["retCode"] = statusCode::$actionNotSetted;
    }
    $mysqlConn->disconnect();
    $return["json"] = json_encode($return);
    echo json_encode($return);
?> 