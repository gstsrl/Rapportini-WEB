<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of BE_color
 *
 * @author riccardo
 */
class BE_color {
    public static $bianco = null;
    public static $nero = null;
    public static $grigio = null;
    public static $giallo = null;
    public static $rosso = null;
    public static $blue = null;
    public static $celeste = null;
    public static $viola = null;
    public static $verde = null;

    public static function init() {
       
        self::$bianco= array(255, 255, 255);
        self::$nero=   array(0, 0, 0);
        self::$grigio= array(230, 230, 230);
        self::$giallo= array(225, 223, 52);
        self::$rosso=  array(212, 161, 144);
        self::$blue=   array(152, 187, 224);
        self::$celeste=array(152, 224, 210);
        self::$viola=  array(174, 152, 224);
        self::$verde=  array(202, 224, 152);
       
    }
    
    
   
    
    
}
