<?php
    error_reporting(0);
    require_once './SYS_validatingPostFunction.php';
    require_once './SYS_paramClass.php';
    require_once './SYS_mysqliConnClass.php';
    require_once './SYS_queryClass.php';
    require_once './SYS_loginClass.php';
    require_once './SYS_statusCode.php';


    $submit_POST = is_valid_post_string('submit');
    $codCustomer_POST = is_valid_post_string('codCustomer');
    $descrCustomer_POST = is_valid_post_string('descrCustomer');
   
    $newCustomerIsEnabled = TRUE;

    if ($submit_POST['isSetted'] && $submit_POST['isValid']) {

        //This makes sure they did not leave any fields blank
        if (!( $codCustomer_POST['isSetted'] && $codCustomer_POST['isValid']) ||
                !( $descrCustomer_POST['isSetted'] && $descrCustomer_POST['isValid']) 
                
        ) {
            $return["retCode"] = statusCode::$fieldNotSetted;
        }else{
          
            $parametri = new Params();
            $mysqlConn = new mysqliConnClass($parametri);
            $mysqliConn = $mysqlConn->connect();

            if (!queryClass::checkCustomerExistence($mysqliConn, $codCustomer_POST['value']) ) {
         
                // now we insert it into the database
                if (queryClass::addCustomer($mysqliConn, $codCustomer_POST['value'], $descrCustomer_POST['value'],$newCustomerIsEnabled)) {
                    $return["retCode"] = statusCode::$inserted;
                }else{
                    $return["retCode"] = statusCode::$notInserted; 
                }
            }else{
                $return["retCode"] = statusCode::$olreadyUsed;
            }
        }
    } else {
        $return["retCode"] = statusCode::$actionNotSetted;
    }
    $mysqlConn->disconnect();
    $return["json"] = json_encode($return);
    echo json_encode($return);
?> 