<?php
error_reporting(0);
require_once './SYS_validatingPostFunction.php';
require_once './SYS_paramClass.php';
require_once './SYS_mysqliConnClass.php';
require_once './SYS_queryClass.php';
require_once './SYS_loginClass.php';
require_once './SYS_statusCode.php';
$codCustomer_POST = is_valid_post_string('codCustomer');
$codCustomer= "";
if ($codCustomer_POST['isSetted'] && $codCustomer_POST['isValid']) {
    $codCustomer = $codCustomer_POST['value'];
}
$parametri = new Params();
$mysqlConn = new mysqliConnClass($parametri);
$mysqliConn = $mysqlConn->connect();
$exist=queryClass::checkCustomerExistence($mysqliConn, $codCustomer);
$mysqlConn->disconnect();
if($exist){
    $return["retCode"] = statusCode::$olreadyUsed; 
}else{
    $return["retCode"] = statusCode::$notUsed; 
}
$return["json"] = json_encode($return);
echo json_encode($return);
?>