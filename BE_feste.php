<?php
class Feste {
    public static function isFestaDate($date){
        
        return(self::isSabato($date)||self::isDomenica($date)|| self::isInFestivita($date));
    }
    
    public static function isFestaYMd($YYYY,$MM,$dd){
        $date=strtotime("$YYYY-$MM-$dd");
        return(self::isSabato($date)||self::isDomenica($date)|| self::isInFestivita($date));
    }
    
    private static function isSabato($date){
        $weekDay=date('w', $date);
        return ($weekDay == 6);
    }
    
    private static function isDomenica($date){
        $weekDay=date('w', $date);
        return ($weekDay == 0);
    }
    
    private static function isInFestivita($date){
        $year=date('Y', $date);
        $dateFestivi=self::calcolaFesta($year);
        foreach($dateFestivi as $d=>$v){
    	    
            if($v==$date){
                return true;
            }
        }
        return false;
    }




    private static function calcolaFesta($YYYY){
        
        //PRENDO IN INPUT L'ANNO DA ELABORARE. 
        //SE VUOTO PRENDO L'ANNO IN CORSO
        $anno_input=$YYYY!=''?$YYYY:date("Y");
        //INIZIALIZZIAMO LE FESTIVITA' CON LA LORO ETICHETTA.
        $feste = Array(
            "01-01"=>"Capodanno", 
            "01-06"=>"Epifania", 
            "04-25"=>"Liberazione", 
            "05-01"=>"Festa Lavoratori", 
            "06-02"=>"Festa della Repubblica", 
            "08-15"=>"Ferragosto", 
            "11-01"=>"Tutti Santi", 
            "12-08"=>"Immacolata", 
            "12-25"=>"Natale", 
            "12-26"=>"St. Stefano"); 

        $march21=date("$anno_input-03-21");
        $gPasquetta=easter_days($anno_input)+1;
        $dataPasquetta = strtotime(date("Y-m-d", strtotime($march21)) . " +$gPasquetta day");
        $K=date("m-d",$dataPasquetta);
        $feste[$K]="Pasquetta";
        //ARRAY FINALE CON LE DATE DEI FESTIVI
        $festivita=array();
        
        foreach ($feste as $key => $value) {
           $festivita[]= strtotime("$anno_input-$key");
        }
              
        return $festivita;
    }
   
    
}
   	
?>
		