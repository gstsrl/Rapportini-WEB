
<?php

error_reporting(0);
require_once './SYS_validatingPostFunction.php';
require_once './SYS_paramClass.php';
require_once './SYS_mysqliConnClass.php';
require_once './SYS_queryClass.php';
require_once './SYS_loginClass.php';
require_once './SYS_statusCode.php';

$parametri= new Params();
$mysqlConn= new mysqliConnClass($parametri);
$mysqliConn = $mysqlConn->connect();





//if the login form is submitted 
$submit_POST = is_valid_post_string('submit');

if ($submit_POST['isSetted'] && $submit_POST['isValid']) { // if form has been submitted
    $username_POST = is_valid_post_string('username');
    $pass_POST = is_valid_post_string('pass');

    // makes sure they filled it in
    if (
            (!$username_POST['isSetted'] || !$username_POST['isValid']) ||
            (!$pass_POST['isSetted'] || !$pass_POST['isValid'])
    ) {

        $return["retCode"] = statusCode::$fieldNotSetted;
    } else {
        $userCHECK_USR_ON_DB = stripslashes($username_POST['value']);
        $passCHECK_USR_ON_DB = stripslashes($pass_POST['value']);

        if(loginClass::beginUserSession($mysqliConn,$userCHECK_USR_ON_DB, $passCHECK_USR_ON_DB)){
            $return["retCode"] = statusCode::$loginOK;
        }else{
          $return["retCode"] = statusCode::$loginKO;  
        }
    }
} else {
   
    $return["retCode"] = statusCode::$actionNotSetted;
}

$mysqlConn->disconnect();
$return["json"] = json_encode($return);
echo json_encode($return);
?>