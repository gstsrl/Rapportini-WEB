<?php
    error_reporting(0);
    require_once './SYS_validatingPostFunction.php';
    require_once './SYS_paramClass.php';
    require_once './SYS_mysqliConnClass.php';
    require_once './SYS_queryClass.php';
    require_once './SYS_loginClass.php';
    require_once './SYS_statusCode.php';


    $submit_POST      = is_valid_post_string('submit');
    $codCustomer_POST = is_valid_post_string('codCustomer');
    $codOffice_POST   = is_valid_post_string('codOffice');
    $codResp_POST     = is_valid_post_string('codResp');
    $codOffer_POST    = is_valid_post_string('codOffer');
    $descrOffer_POST  = is_valid_post_string('descrOffer');
    $hour_POST        = is_valid_post_integer('hour');
    $type_POST        = is_valid_post_integer('type');
    $isEnabled = TRUE;

    if ($submit_POST['isSetted'] && $submit_POST['isValid']) {

        //This makes sure they did not leave any fields blank
        if (!( $codCustomer_POST['isSetted'] && $codCustomer_POST['isValid']) ||
            !( $codOffice_POST['isSetted'] && $codOffice_POST['isValid']) ||
            !( $codResp_POST['isSetted'] && $codResp_POST['isValid']) ||
            !( $codOffer_POST['isSetted'] && $codOffer_POST['isValid']) ||
            !( $hour_POST['isSetted'] && $hour_POST['isValid']) ||
            !( $descrOffer_POST['isSetted'] && $descrOffer_POST['isValid'])|| 
            !( $type_POST['isSetted'] && $type_POST['isValid']) 
                
        ) {
            $return["retCode"] = statusCode::$fieldNotSetted;
        }else{
          
            $parametri = new Params();
            $mysqlConn = new mysqliConnClass($parametri);
            $mysqliConn = $mysqlConn->connect();

            if (!queryClass::checkOfferExistence($mysqliConn, $codOffer_POST['value']) ) {
         
                // now we insert it into the database
                if (queryClass::addOffer($mysqliConn,$codOffer_POST['value'], $descrOffer_POST['value'], $codOffice_POST['value'], $codResp_POST['value'],$hour_POST['value'],$type_POST['value'],$isEnabled)) {
                    $return["retCode"] = statusCode::$inserted;
                }else{
                    $return["retCode"] = statusCode::$notInserted; 
                }
            }else{
                $return["retCode"] = statusCode::$olreadyUsed;
            }
        }
    } else {
        $return["retCode"] = statusCode::$actionNotSetted;
    }
    $mysqlConn->disconnect();
    $return["json"] = json_encode($return);
    echo json_encode($return);
?> 