<?php
error_reporting(0);
require_once './SYS_validatingPostFunction.php';
require_once './SYS_paramClass.php';
require_once './SYS_mysqliConnClass.php';
require_once './SYS_queryClass.php';
require_once './SYS_loginClass.php';
require_once './SYS_statusCode.php';
$codCustomer_POST = is_valid_post_string('codCustomer');
$codOffice_POST = is_valid_post_string('codOffice');
$codOffer_POST = is_valid_post_string('codOffer');


$codCustomer= "";
$codOffice= "";
$codOffer="";
if ($codOffer_POST['isSetted'] && $codOffer_POST['isValid']) {
    $codOffer = $codOffer_POST['value'];
}


$parametri = new Params();
$mysqlConn = new mysqliConnClass($parametri);
$mysqliConn = $mysqlConn->connect();
$exist=queryClass::checkOfferExistence($mysqliConn,$codOffer);
$mysqlConn->disconnect();
if($exist){
    $return["retCode"] = statusCode::$olreadyUsed; 
}else{
    $return["retCode"] = statusCode::$notUsed; 
}
$return["json"] = json_encode($return);
echo json_encode($return);
?>