<?php
    error_reporting(0);
    require_once './SYS_validatingPostFunction.php';
    require_once './SYS_paramClass.php';
    require_once './SYS_mysqliConnClass.php';
    require_once './SYS_queryClass.php';
    require_once './SYS_loginClass.php';
    require_once './SYS_statusCode.php';


    $submit_POST = is_valid_post_string('submit');
    $codCustomer_POST = is_valid_post_string('codCustomer');
    $codOffice_POST = is_valid_post_string('codOffice');
    $descrOffice_POST = is_valid_post_string('descrOffice');
   
    $newOfficeIsEnabled = TRUE;

    if ($submit_POST['isSetted'] && $submit_POST['isValid']) {

        //This makes sure they did not leave any fields blank
        if (!( $codCustomer_POST['isSetted'] && $codCustomer_POST['isValid']) ||
            !( $codOffice_POST['isSetted'] && $codOffice_POST['isValid']) ||
            !( $descrOffice_POST['isSetted'] && $descrOffice_POST['isValid']) 
                
        ) {
            $return["retCode"] = statusCode::$fieldNotSetted;
        }else{
          
            $parametri = new Params();
            $mysqlConn = new mysqliConnClass($parametri);
            $mysqliConn = $mysqlConn->connect();

            if (!queryClass::checkOfficeExistence($mysqliConn, $codCustomer_POST['value'],$codOffice_POST['value']) ) {
         
                // now we insert it into the database
                if (queryClass::addOffice($mysqliConn, $codCustomer_POST['value'],$codOffice_POST['value'], $descrOffice_POST['value'],$newOfficeIsEnabled)) {
                    $return["retCode"] = statusCode::$inserted;
                }else{
                    $return["retCode"] = statusCode::$notInserted; 
                }
            }else{
                $return["retCode"] = statusCode::$olreadyUsed;
            }
        }
    } else {
        $return["retCode"] = statusCode::$actionNotSetted;
    }
    $mysqlConn->disconnect();
    $return["json"] = json_encode($return);
    echo json_encode($return);
?> 