<?php
error_reporting(0);
require_once './SYS_validatingPostFunction.php';
require_once './SYS_paramClass.php';
require_once './SYS_mysqliConnClass.php';
require_once './SYS_queryClass.php';
require_once './SYS_loginClass.php';
require_once './SYS_statusCode.php';


$month_POST = is_valid_post_integer('month');
$month = date('n');
if ($month_POST['isSetted'] && $month_POST['isValid']) {
    $month=$month_POST['value'];
}

$year_POST = is_valid_post_integer('year');
$year = date('Y');
if ($year_POST['isSetted'] && $year_POST['isValid']) {
   $year =$year_POST['value'];
}



$user_POST = is_valid_post_string('user');
$user =loginClass::getUidUser();
if ($user_POST['isSetted'] && $user_POST['isValid']) {
    $user=$user_POST['value'];
}

$parametri = new Params();
$mysqlConn = new mysqliConnClass($parametri);
$mysqliConn = $mysqlConn->connect();

$list_element=queryClass::listReportCombination($mysqliConn, $month, $year, $user);
$tot_element=count($list_element);
$mysqlConn->disconnect();
echo "{" ;
echo '"total": ' . $tot_element ;
echo  ',';
echo '"rows": ';
echo json_encode($list_element);
echo "}"; 
?>