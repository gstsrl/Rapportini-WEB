<?php
    error_reporting(0);
    require_once './SYS_validatingPostFunction.php';
    require_once './SYS_paramClass.php';
    require_once './SYS_mysqliConnClass.php';
    require_once './SYS_queryClass.php';
    require_once './SYS_loginClass.php';
    require_once './SYS_statusCode.php';
    
    $submit_POST    = is_valid_post_string('submit');
    $codRes_POST    = is_valid_post_string('codRes');
    $nome_POST      = is_valid_post_string('nome');
    $cognome_POST   = is_valid_post_string('cognome');
    
    $pass_POST      = is_valid_post_string('pass');
    $pass2_POST     = is_valid_post_string('pass2');
    $username_POST  = is_valid_post_string('username');
    
    
    
    //normailzation of checkboxes
    $newUsrIsAdmin  = FALSE;
    $newUsrIsUser   = FALSE;
    $newUsrIsAccount = FALSE;
    //default
    $newUsrIsResp   =FALSE;
    $newUsrIsEnabled = TRUE;

    $isAdmin_POST   = is_valid_post_boolean('isAdmin');
    $isUser_POST    = is_valid_post_boolean('isUser');
    $isAccount_POST = is_valid_post_boolean('isAccount');
    
    
   
    $newResIsEnabled = TRUE;

    if ($submit_POST['isSetted'] && $submit_POST['isValid']) {

        //This makes sure they did not leave any fields blank
        if (!( $codRes_POST['isSetted']     && $codRes_POST['isValid']  ) ||
            !( $nome_POST['isSetted']       && $nome_POST['isValid']    ) ||
            !( $cognome_POST['isSetted']    && $cognome_POST['isValid'] ) ||
            !( $username_POST['isSetted']   && $username_POST['isValid']) ||
            !( $pass_POST['isSetted']       && $pass_POST['isValid']    )||
            !( $pass2_POST['isSetted']      && $pass2_POST['isValid']   )
                           
        ) {
            $return["retCode"] = statusCode::$fieldNotSetted;
        }else{
            
            // this makes sure both passwords entered match
            if ($pass_POST['value'] != $pass2_POST['value']) {    
                $return["retCode"] = statusCode::$passwordNotMatch;
            }else{
        
                $parametri = new Params();
                $mysqlConn = new mysqliConnClass($parametri);
                $mysqliConn = $mysqlConn->connect();
             
                $existR=queryClass::checkResourceExistence($mysqliConn, $codCustomer,$codOffice,$codRes_POST['value']);
                $existU=queryClass::checkUsernameExistence($mysqliConn,$username_POST['value']);
                if($existR||$existU){
                    $return["retCode"] = statusCode::$olreadyUsed; 
                }else{
                    
                        //Imposto i campi se passati altrimenti il default farà testo
                        if ($isAdmin_POST['isSetted'] && $isAdmin_POST['isValid']) {
                            $newUsrIsAdmin = $isAdmin_POST['value'];
                        }
                        if ($isUser_POST['isSetted'] && $isUser_POST['isValid']) {
                            $newUsrIsUser = $isUser_POST['value'];
                        }
                        if ($isAccount_POST['isSetted'] && $isAccount_POST['isValid']) {
                            $newUsrIsAccount = $isAccount_POST['value'];
                        }
                       
                        $addR=queryClass::addResource($mysqliConn, $codRes_POST['value'],$nome_POST['value'], $cognome_POST['value'],$newResIsEnabled);
                        $addU=queryClass::addUserAndRoles($mysqliConn, $codRes_POST['value'],$username_POST['value'], $pass_POST['value'], $newUsrIsAdmin, $newUsrIsUser, $newUsrIsAccount,$newUsrIsResp, $newUsrIsEnabled);
                        if($addR && $addU){
                            $return["retCode"] = statusCode::$inserted;
                        }else if($addR && !$addU){

                        }else if(!$addR && $addU){

                        }else if(!$addR && !$addU){
                            $return["retCode"] = statusCode::$notInserted; 
                        }
                }
            }
        }
    } else {
        $return["retCode"] = statusCode::$actionNotSetted;
    }
    $mysqlConn->disconnect();
    $return["json"] = json_encode($return);
    echo json_encode($return);
?> 