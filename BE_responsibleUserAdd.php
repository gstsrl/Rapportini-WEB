<?php
    error_reporting(0);
    require_once './SYS_validatingPostFunction.php';
    require_once './SYS_paramClass.php';
    require_once './SYS_mysqliConnClass.php';
    require_once './SYS_queryClass.php';
    require_once './SYS_loginClass.php';
    require_once './SYS_statusCode.php';

    
    $submit_POST        = is_valid_post_string('submit');
    $codCustomer_POST   = is_valid_post_string('codCustomer');
    $codOffice_POST     = is_valid_post_string('codOffice');
    $codResp_POST       = is_valid_post_string('codResp');
    $descrResp_POST     = is_valid_post_string('descrResp');
    $createUser_POST    = is_valid_post_boolean('createUser');
   
    $newRespIsEnabled = TRUE;

    if ($submit_POST['isSetted'] && $submit_POST['isValid']) {

        //This makes sure they did not leave any fields blank
        if (!( $codCustomer_POST['isSetted'] && $codCustomer_POST['isValid']) ||
            !( $codOffice_POST['isSetted'] && $codOffice_POST['isValid']) ||
            !( $codResp_POST['isSetted'] && $codResp_POST['isValid']) ||
            !( $descrResp_POST['isSetted'] && $descrResp_POST['isValid'])||
            !( $createUser_POST['isSetted'] && $createUser_POST['isValid'])
                           
        ) {
            $return["retCode"] = statusCode::$fieldNotSetted;
        }else{
          
            $parametri = new Params();
            $mysqlConn = new mysqliConnClass($parametri);
            $mysqliConn = $mysqlConn->connect();

            
            $existR=queryClass::checkResponsibleExistence($mysqliConn, $codCustomer,$codOffice,$codResp);
            $existU=queryClass::checkUsernameExistence($mysqliConn,$codResp);
            if($existR||$existU){
                $return["retCode"] = statusCode::$olreadyUsed; 
            }else{
                $addR=queryClass::addResponsible($mysqliConn, $codOffice_POST['value'],$codResp_POST['value'], $descrResp_POST['value'],$newRespIsEnabled);
                $newUsrIsAdmin=FALSE;
                $newUsrIsUser=FALSE;
                $newUsrIsAccount=FALSE;
                $newUsrIsResp=TRUE;
                if($createUser_POST['value']){
                    $newUsrIsEnabled=TRUE;
                }else{
                    $newUsrIsFalse=TRUE; 
                }
                $addU=queryClass::addUserAndRoles($mysqliConn, $codResp_POST['value'],$codResp_POST['value'], $codResp_POST['value'], $newUsrIsAdmin, $newUsrIsUser, $newUsrIsAccount,$newUsrIsResp, $newUsrIsEnabled);
                 
                if($addR && $addU){
                    $return["retCode"] = statusCode::$inserted;
                }else if($addR && !$addU){
                    
                }else if(!$addR && $addU){
                    
                }else if(!$addR && !$addU){
                    $return["retCode"] = statusCode::$notInserted; 
                }
            }
        }
    } else {
        $return["retCode"] = statusCode::$actionNotSetted;
    }
    $mysqlConn->disconnect();
    $return["json"] = json_encode($return);
    echo json_encode($return);
?> 