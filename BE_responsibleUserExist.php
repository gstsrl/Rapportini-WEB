<?php
error_reporting(0);
require_once './SYS_validatingPostFunction.php';
require_once './SYS_paramClass.php';
require_once './SYS_mysqliConnClass.php';
require_once './SYS_queryClass.php';
require_once './SYS_loginClass.php';
require_once './SYS_statusCode.php';

$codCustomer_POST = is_valid_post_string('codCustomer');
$codOffice_POST = is_valid_post_string('codOffice');
$codResp_POST = is_valid_post_string('codResp');

$codCustomer= "";
$codOffice= "";
$codResp="";

if ($codCustomer_POST['isSetted'] && $codCustomer_POST['isValid']) {
    $codCustomer = $codCustomer_POST['value'];
}
if ($codOffice_POST['isSetted'] && $codOffice_POST['isValid']) {
    $codOffice = $codOffice_POST['value'];
}
if ($codResp_POST['isSetted'] && $codResp_POST['isValid']) {
    $codResp = $codResp_POST['value'];
}

$parametri = new Params();
$mysqlConn = new mysqliConnClass($parametri);
$mysqliConn = $mysqlConn->connect();
$existR=queryClass::checkResponsibleExistence($mysqliConn, $codCustomer,$codOffice,$codResp);
$existU=queryClass::checkUsernameExistence($mysqliConn,$codResp);
$mysqlConn->disconnect();
if($existR||$existU){
    $return["retCode"] = statusCode::$olreadyUsed; 
}else{
    $return["retCode"] = statusCode::$notUsed; 
}
$return["json"] = json_encode($return);
echo json_encode($return);
?>