<?php
error_reporting(0);

require_once './SYS_validatingPostFunction.php';
require_once './SYS_paramClass.php';
require_once './SYS_mysqliConnClass.php';
require_once './SYS_queryClass.php';
require_once './SYS_loginClass.php';
require_once './SYS_statusCode.php';
require_once './BE_feste.php';

function JSONshowMonth($month = null, $year = null, $user) {

    $feste = new Feste ();
    
    $calendar = '';
    if ($month == null || $year == null) {
        $month = date('n');
        $year = date('Y');
    }

    $parametri = new Params();
    $mysqlConn = new mysqliConnClass($parametri);
    $mysqliConn = $mysqlConn->connect();

    //recupero gli appuntamenti dello user
    $activityList=queryClass::listActivity($mysqliConn, $month, $year, $user);
    $mysqlConn->disconnect();






    $calendar = '';
    if ($month == null || $year == null) {
        $month = date('n');
        $year = date('Y');
    }

//creo il mese precedente e successivo
    $prevVis = mktime(0, 0, 0, $month - 1, 1, $year);
    $nextVis = mktime(0, 0, 0, $month + 1, 1, $year);
    $currVis = mktime(12, 0, 0, $month, 1, $year);
    $today = mktime(12, 0, 0, date('n'), date('j'), date('Y'));

//giorni nel mese 
    $daysInMonth = date("t", $currVis);
//il primo giorno del mese cosa è
    $offset = date("w", $currVis);
    $offsetIT = translateDate_IT($offset);


    $dayMatrix = array();

    $riga = 0;
//n celle vuote sino al primo giorno del mese escluso
    for ($j = 0; $j < $offsetIT; $j++) {
        $dayMatrix[$riga][$j] = createDay(" ", 0,0);
    }

//Stampa tutti i giorni del mese avendo cura
// di controllare che se domenica nuova riga
    $j = $offsetIT;
    for ($day = 1; $day <= $daysInMonth; $day++) {

        $giornoMese = date("w", mktime(0, 0, 0, $month, $day, $year));
        $giornoMeseIT = translateDate_IT($giornoMese);
        $isBeginOfWeek = beginOfWeek_IT($giornoMeseIT);
        if ($isBeginOfWeek && $day > 1) {
            $riga++;
            $j = 0;
        }
        $activityNumber=checkActivityInDate($month, $day, $year,$activityList);
        $totHour=checkActivityDurationInDate($month, $day, $year,$activityList);
        $isFesta=$feste->isFestaYMd((int)$year, (int)$month, (int)$day);
        
        $dayMatrix[$riga][$j] = createDay($day, $activityNumber,$totHour,$isFesta);
        $j++;
    }

    //Inserisco ora lo riempimento finale
    $ultimoGgMese = date("w", mktime(0, 0, 0, $month, $daysInMonth, $year));
    $ultimoGgMeseIT = translateDate_IT($ultimoGgMese);
    //n celle vuote sino al completamento della tabella
    for ($j = ($ultimoGgMeseIT + 1); $j < 7; $j++) {
        $dayMatrix[$riga][$j] = createDay(" ", 0,0);
    }






    $table = array();
    $table['today_month'] = date('n');
    $table['today_year'] = date('Y');
    $table['today_day'] = date('j');
    $table['currVis_month'] = date('n', $currVis);
    $table['currVis_year'] = date('Y', $currVis);
    $table['nextVis_month'] = date('n', $nextVis);
    $table['nextVis_year'] = date('Y', $nextVis);
    $table['prevVis_month'] = date('n', $prevVis);
    $table['prevVis_year'] = date('Y', $prevVis);
    $table['dayMatrix'] = $dayMatrix;



    $return["data"] = $table;
    $return["json"] = json_encode($return);
    echo json_encode($return);
}


function  checkActivityInDate($month, $day, $year,$activityList){
    $numberOfActivity=0;
    
    foreach ($activityList as $key => $value) {
       
        if( intval($value['activityDate_year'])===intval($year) &&
            intval($value['activityDate_month'])===intval($month) &&
            intval($value['activityDate_day'])===intval($day)){
            
            $numberOfActivity++;
        }
    }
    return $numberOfActivity;
    
}
function  checkActivityDurationInDate($month, $day, $year,$activityList){
    $hourOfActivity=floatval ( "00.00");
    
    foreach ($activityList as $key => $value) {
       
        if( intval($value['activityDate_year'])===intval($year) &&
            intval($value['activityDate_month'])===intval($month) &&
            intval($value['activityDate_day'])===intval($day)){
            $floatDuration=floatval ( $value['activityDuration']);
            
            $hourOfActivity=($hourOfActivity+$floatDuration);
        }
    }
    return $hourOfActivity;
    
}

function translateDate_IT($weekday) {
//Modifica per la data americana dove
//0 = Domenica  -->6
//1 = Lunedi    -->0
//2 = Martedi   -->1
//3 = Mercoledi -->2
//4 = Giovedi   -->3
//5 = Venerdi   -->4
//6 = Sabato    -->5

    if ($weekday < 7) {
        if ($weekday == 0) {
            $weekday = 6;
        } else {
            $weekday = $weekday - 1;
        }
    }
    return $weekday;
}

function beginOfWeek_IT($weekday) {
    if ($weekday == 0) {
        return TRUE;
    } else {
        return FALSE;
    }
}

function createDay($dayLbl = " ", $numActivity = 0, $totHour = 0,$isHoliday=FALSE) {
    $day = array();
    $day['dayLbl'] = $dayLbl;
    $day['numActivity'] = $numActivity;
    $day['totHour'] = $totHour;
    $day['isHoliday'] = $isHoliday;
    return $day;
}
?>