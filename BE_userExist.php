<?php
error_reporting(0);
require_once './SYS_validatingPostFunction.php';
require_once './SYS_paramClass.php';
require_once './SYS_mysqliConnClass.php';
require_once './SYS_queryClass.php';
require_once './SYS_loginClass.php';
require_once './SYS_statusCode.php';
$codUser_POST = is_valid_post_string('codUser');
$codUser= "";
if ($codUser_POST['isSetted'] && $codUser_POST['isValid']) {
    $codUser = $codUser_POST['value'];
}
$parametri = new Params();
$mysqlConn = new mysqliConnClass($parametri);
$mysqliConn = $mysqlConn->connect();
$exist=queryClass::checkUsernameExistence($mysqliConn, $codUser);
$mysqlConn->disconnect();
if($exist){
    $return["retCode"] = statusCode::$olreadyUsed; 
}else{
    $return["retCode"] = statusCode::$notUsed; 
}
$return["json"] = json_encode($return);
echo json_encode($return);
?>