<?php
error_reporting(0);

require_once './SYS_validatingPostFunction.php';
require_once './SYS_paramClass.php';
require_once './SYS_mysqliConnClass.php';
require_once './SYS_queryClass.php';
require_once './SYS_loginClass.php';
require_once './SYS_statusCode.php';




$from_POST = is_valid_post_string('from');
$to_POST = is_valid_post_string('to');
$types_POST = is_valid_post_array_string('types');




$from = 0;
if ($from_POST['isSetted'] && $from_POST['isValid']) {
    $from = $from_POST['value'];
}

$to = 0;
if ($to_POST['isSetted'] && $to_POST['isValid']) {
    $to = $to_POST['value'];
}

$types = "";
if ($types_POST['isSetted'] && $types_POST['isValid']) {
    $types = $types_POST['value'];
}


$parametri = new Params();
$mysqlConn = new mysqliConnClass($parametri);
$mysqliConn = $mysqlConn->connect();


$list_element=queryClass::listUsersOnActivity($mysqliConn, $from,$to, $types);

$mysqlConn->disconnect();
$tot_element=count($list_element);

echo "{" ;
echo '"total": ' . $tot_element . ',';
echo '"rows": ';
echo json_encode($list_element);
echo "}"; 
?>