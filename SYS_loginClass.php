<?php
require_once './SYS_queryClass.php';
/**
 * Description of loginClass
 *
 * @author Riccardo
 */
class loginClass {

    /**
     * Controlla se l'utente è sul DB
     * @param mysqli $mysqliConn
     * @return boolean
     */
    public static function checkUser2(mysqli $mysqliConn) {
        $authCHECK_LOGIN = FALSE;
        //recupera la sessione
        session_start();
        if (isset($_SESSION['user_username'])) {
            session_regenerate_id(true);
            $username = $_SESSION['user_username'];
            $pass = $_SESSION['user_pwd'];

            $presentCHECK_USR_ON_DB = queryClass::checkUserOnDb($mysqliConn, $username, $pass);

            if ($presentCHECK_USR_ON_DB) {
                $authCHECK_LOGIN = TRUE;
            }
        } else {
            $authCHECK_LOGIN = FALSE;
        }
        //chiude la sessione
        session_commit();

        return $authCHECK_LOGIN;
    }
    /**
     * Controlla se l'utente è sul DB
     * @param mysqli $mysqliConn
     * @return boolean
     */
    public static function checkUser() {

        //Si instaura una connessione da sola
        $parametri = new Params();
        $mysqlConn = new mysqliConnClass($parametri);
        $mysqliConn = $mysqlConn->connect();
        $authCHECK_LOGIN = loginClass::checkUser2($mysqliConn);
        //chiude la connessione
        $mysqlConn->disconnect();
        return $authCHECK_LOGIN;
    }

    /**
     * Rtorna lo username dell'utente
     * @return type
     */
    public static function getUser() {
        
        $status = session_status();
        if($status == PHP_SESSION_NONE){
            //There is no active session
            session_start();
        }else if($status == PHP_SESSION_DISABLED){
            //Sessions are not available
        }else if($status == PHP_SESSION_ACTIVE){
            //session_destroy();
            //session_start();
        }
        //session_start();
        if (isset($_SESSION['user_username'])) {
            session_regenerate_id(true);
            $username = $_SESSION['user_username'];
            return $username;
        }
        session_commit();
    }
    /**
     * Rtorna lo username dell'utente
     * @return type
     */
    public static function getUidUser() {
        
        $status = session_status();
        if($status == PHP_SESSION_NONE){
            //There is no active session
            session_start();
        }else if($status == PHP_SESSION_DISABLED){
            //Sessions are not available
        }else if($status == PHP_SESSION_ACTIVE){
            //session_destroy();
            //session_start();
        }
        //session_start();
        if (isset($_SESSION['uid_user'])) {
            session_regenerate_id(true);
            $uid_user = $_SESSION['uid_user'];
            return $uid_user;
        }
        session_commit();
    }

    public static function beginUserSession(mysqli $mysqliConn,$userCHECK_USR_ON_DB, $passCHECK_USR_ON_DB) {
        $presentCHECK_USR_ON_DB = queryClass::checkUserOnDb($mysqliConn, $userCHECK_USR_ON_DB,$passCHECK_USR_ON_DB);
        $codUser_USR_ON_DB = queryClass::getCodUsr($mysqliConn, $userCHECK_USR_ON_DB,$passCHECK_USR_ON_DB);
        if ($presentCHECK_USR_ON_DB) {
            // if login is ok then we add a cookie 
            session_start();
            $_SESSION['uid_user']       = $codUser_USR_ON_DB;//identificativo Utente
            $_SESSION['user_username']  = $userCHECK_USR_ON_DB;//Username
            $_SESSION['user_pwd']       = $passCHECK_USR_ON_DB;//Password
            session_commit();
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public static function endUserSession(){
        session_start();
        session_unset();
        session_destroy();
        header("Location: login.php");
    }

}
?>