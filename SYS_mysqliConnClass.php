<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of mysqliConnClass
 *
 * @author Riccardo
 */
require_once './SYS_paramClass.php';
class mysqliConnClass {
    
    private $parametri;
    private $mysqli;
    private $connected;
    
    function __construct(Params $parametri) {
        $this->parametri=$parametri;
        $this->connect();
    }

    public function connect() {
        if(!$this->connected){
        
            $lmysqli = new mysqli( $this->parametri->getHOST(),  $this->parametri->getDBUSER(),  $this->parametri->getPASS(),  $this->parametri->getDB());
            $this->mysqli=$lmysqli;
            $this->connected=TRUE;

            if ($this->mysqli->connect_errno ) {
                $this->connected=FALSE;
                return NULL;
            }

            $this->enableAutocommit();
            if(!$this->mysqli->query("SET CHARACTER SET 'utf8'")){
                return NULL;
            }
        }
        return  $this->mysqli;
    }
    
    public function getMysqli(){
        if ($this->connected){
            return $this->mysqli;
        }else {
            return NULL;
        }
    }


    public function isConnected(){
       return $this->connected;
    }
       
    public function enableAutocommit(){
        $this->mysqli->autocommit(TRUE);
    }
    
    public function disableAutocommit(){
        $this->mysqli->autocommit(FALSE);
    }
    
    public function disconnect() {
        $this->mysqli->close();
        $this->connected=FALSE;
    }

}
