<?php

/**
 * Description of queryClass
 *
 * @author Riccardo
 */
class queryClass {

    public static function getCodUsr(mysqli $mysqliConn, $userCHECK_USR_ON_DB, $passCHECK_USR_ON_DB) {
      
        $checkSQL_CHECK_USR_ON_DB = "SELECT `codUser` FROM `users` WHERE `username` = ? AND `password`=?";

        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($checkSQL_CHECK_USR_ON_DB)) {
            $codUsr_USR_ON_DB = "";
            $stmt->bind_param("ss", $userCHECK_USR_ON_DB,md5($passCHECK_USR_ON_DB));
            $stmt->bind_result($codUsr_USR_ON_DB);
            $stmt->execute();
            while ($stmt->fetch()) {
               
            }
        } else {
            
        }
        $stmt->free_result();
        $stmt->close();
        
        return $codUsr_USR_ON_DB;
    }
    public static function checkUserOnDb(mysqli $mysqliConn, $userCHECK_USR_ON_DB, $passCHECK_USR_ON_DB) {
        $presentCHECK_USR_ON_DB = FALSE;
        $checkSQL_CHECK_USR_ON_DB = "SELECT `password` FROM `users` WHERE `username` = ?";

        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($checkSQL_CHECK_USR_ON_DB)) {
            $passFieldCHECK_USR_ON_DB = "";
            $stmt->bind_param("s", $userCHECK_USR_ON_DB);
            $stmt->bind_result($passFieldCHECK_USR_ON_DB);
            $stmt->execute();
            while ($stmt->fetch()) {
                //da rivedere qua ho true o false a seconda dell'utente che trovo e in più si suppone ce l'utente sia uno solo  
                if (md5($passCHECK_USR_ON_DB) != $passFieldCHECK_USR_ON_DB) {
                    $presentCHECK_USR_ON_DB = FALSE;
                } else {
                    $presentCHECK_USR_ON_DB = TRUE;
                }
            }
        } else {
            $presentCHECK_USR_ON_DB = FALSE;
        }
        $stmt->free_result();
        $stmt->close();
        return $presentCHECK_USR_ON_DB;
    }
    public static function checkUserRoleOnDb(mysqli $mysqliConn, $codUser_USR_ON_DB) {
        $res['isAdminCHECK_ROLES'] = FALSE;
        $res['isUserCHECK_ROLES'] = FALSE;
        $res['isAccountCHECK_ROLES'] = FALSE;
        $res['isRespCHECK_ROLES'] = FALSE;
        $res['isEnabledCHECK_ROLES'] = FALSE;

        $checkSQL_CHECK_ROLES = "SELECT `isAdmin`, `isUser`, `isAccount`, `isResp`, `isEnabled` FROM `users` WHERE `codUser` = ?";
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($checkSQL_CHECK_ROLES)) {
            $stmt->bind_param("s", $codUser_USR_ON_DB);
            $stmt->bind_result($res['isAdminCHECK_ROLES'], $res['isUserCHECK_ROLES'], $res['isAccountCHECK_ROLES'], $res['isRespCHECK_ROLES'], $res['isEnabledCHECK_ROLES']);
            $stmt->execute();
            if ($stmt->fetch()) {
                //Sfrutto la validazione per avere dei dati corretti.
                //Il valore restituito è il dato correttamente convertito o FALSE 
                $res['isAdminCHECK_ROLES'] = filter_var($res['isAdminCHECK_ROLES'], FILTER_VALIDATE_BOOLEAN);
                $res['isUserCHECK_ROLES'] = filter_var($res['isUserCHECK_ROLES'], FILTER_VALIDATE_BOOLEAN);
                $res['isAccountCHECK_ROLES'] = filter_var($res['isAccountCHECK_ROLES'], FILTER_VALIDATE_BOOLEAN);
                $res['isRespCHECK_ROLES'] = filter_var($res['isRespCHECK_ROLES'], FILTER_VALIDATE_BOOLEAN);
                $res['isEnabledCHECK_ROLES'] = filter_var($res['isEnabledCHECK_ROLES'], FILTER_VALIDATE_BOOLEAN);
            }
        }
         $stmt->free_result();
        $stmt->close();
        return $res;
    }
    public static function checkResourceExistence(mysqli $mysqliConn, $resCHECK_ON_DB) {
         $check2 = 0;
        $userCheckSQL = "SELECT Count(codRes) FROM `resources` WHERE `codRes` = ?";
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($userCheckSQL)) {
            $stmt->bind_param("s", $resCHECK_ON_DB);
            $stmt->bind_result($check2);
            $stmt->execute();
            $stmt->fetch();
        }
         $stmt->free_result();
        $stmt->close();
        if ($check2 != 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    
    }
    public static function checkUsernameExistence(mysqli $mysqliConn, $userCHECK_USR_ON_DB) {
        $check2 = 0;
        $userCheckSQL = "SELECT Count(codUser) FROM `users` WHERE `username` = ?";
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($userCheckSQL)) {
            $stmt->bind_param("s", $userCHECK_USR_ON_DB);
            $stmt->bind_result($check2);
            $stmt->execute();
            $stmt->fetch();
        }
         $stmt->free_result();
        $stmt->close();
        if ($check2 != 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public static function changePassword(mysqli $mysqliConn, $codUser, $passOld, $pass  ) {
        
        $updateUserSQL ="UPDATE `users` SET `password`=? WHERE  `coduser`=? and `password`=?";
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($updateUserSQL)) {
            $stmt->bind_param("sss",  md5($pass),$codUser,md5($passOld));
            if ($stmt->execute()) {
                $user_updated = TRUE;
            }
            $stmt->close();
        }
        $stmt->free_result();
        $stmt->close();
        return $user_updated;
    }
    public static function addUserAndRoles(mysqli $mysqliConn, $codUser,$username, $pass, $newUsrIsAdmin, $newUsrIsUser, $newUsrIsAccount, $newUsrIsResp, $newUsrIsEnabled) {
        $newUsrIsAdminI = intval($newUsrIsAdmin);
        $newUsrIsUserI = intval($newUsrIsUser);
        $newUsrIsAccountI = intval($newUsrIsAccount);
        $newUsrIsRespI = intval($newUsrIsResp);
        $newUsrIsEnabledI = intval($newUsrIsEnabled);
        $insertUserSQL = "INSERT INTO `users` (`codUser`,`username`, `password`, `isAccount`, `isAdmin`, `isEnabled`, `isResp`, `isUser`) "
                . " VALUES "
                . "("
                . "?"
                . ", "
                . "?"
                . ", "
                . "?"
                . ", "
                . "?"
                . ", "
                . "?"
                . ", "
                . "?"
                . ", "
                . "?"
                . ", "
                . "?"
                . ")";


        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($insertUserSQL)) {
            $stmt->bind_param("sssiiiii", $codUser,$username, md5($pass), $newUsrIsAccountI, $newUsrIsAdminI, $newUsrIsEnabledI, $newUsrIsRespI, $newUsrIsUserI);

            if ($stmt->execute()) {
                $user_inserted = TRUE;
            }
            $stmt->close();
        }
        $stmt->free_result();
        $stmt->close();
        return $user_inserted;
    }
    public static function countUsers($mysqliConn, $search) {
        $select_part = "SELECT COUNT(*) FROM `users` ";

        $where_part = "";
        if ($search != "") {
            $where_part = " WHERE ( "
                    . "( `username` LIKE '" . $search . "' OR `username` LIKE '%" . $search . "' OR `username` LIKE '" . $search . "%' OR `username` LIKE '%" . $search . "%' ) "
                    . " ) ";
        }

        $countSQL_LIST_USR_ON_DB = $select_part . " " . $where_part;

        $totElement = 0;
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($countSQL_LIST_USR_ON_DB)) {
            $totElement = 0;
            //$stmt->bind_param("s", $userCHECK_USR_ON_DB);
            $stmt->bind_result($totElement);
            $stmt->execute();
            while ($stmt->fetch()) {
                
            }
        } else {
            $totElement = 0;
        }
        $stmt->free_result();
        $stmt->close();
        return $totElement;
    }
    public static function listUsers($mysqliConn, $limit, $offset, $sort, $order, $search) {
        $select_part = "SELECT 
                        `idUser`,
                        `nome`,
                        `cognome`, 
                        `codUser` ,
                        `username`,
                        `password`,
                        `isAccount` ,
                        `isAdmin` ,
                        `resources`.`isEnabled` ,
                        `isResp` ,
                        `isUser` 
                        FROM 
                        `users` 
                        JOIN 
                        `resources` 
                        ON 
                        `users`.`codUser`=`resources`.`codRes`";


        $limit_part = "";
        if ($limit > 0) {
            if ($offset > 0) {
                $limit_part = " LIMIT " . $offset . "," . $limit;
            } else {
                $limit_part = " LIMIT " . $limit;
            }
        }

        $where_part = "";
        if ($search != "") {
            $where_part = " WHERE ( "
                    . "( `username` LIKE '" . $search . "' OR `username` LIKE '%" . $search . "' OR `username` LIKE '" . $search . "%' OR `username` LIKE '%" . $search . "%' ) "
                    . " ) ";
        }

        $orderpart_part = "";
        if ($sort === "username" || $sort === "isAccount" || $sort === "isAdmin" || $sort === "isEnabled" || $sort === "isResp" || $sort === "isUser") {
            $orderpart_part = " ORDER BY " . $sort . " ";
            if ($order === "asc" || $order === "desc") {
                $orderpart_part = $orderpart_part . " " . $order . " ";
            }
        }

        $ResList = array();
        $res = array();
        $listSQL_LIST_USR = $select_part . " " . $where_part . " " . $orderpart_part . " " . $limit_part;
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($listSQL_LIST_USR)) {
            //$stmt->bind_param("s", $userCHECK_USR_ON_DB);
            $stmt->bind_result($res['idUser'],$res['nome'],$res['cognome'], $res['codUser'],$res['username'], $res['password'], $res['isAccount'], $res['isAdmin'], $res['isEnabled'], $res['isResp'], $res['isUser']);
            $stmt->execute();
            $counter = 0;
            while ($stmt->fetch()) {
                $ResList[$counter]['idUser'] = $res['idUser'];
                $ResList[$counter]['nome'] = $res['nome'];
                $ResList[$counter]['cognome'] = $res['cognome'];
                $ResList[$counter]['codUser'] = $res['codUser'];
                $ResList[$counter]['username'] = $res['username'];
                $ResList[$counter]['isAccount'] = $res['isAccount'];
                $ResList[$counter]['isAdmin'] = $res['isAdmin'];
                $ResList[$counter]['isEnabled'] = $res['isEnabled'];
                $ResList[$counter]['isResp'] = $res['isResp'];
                $ResList[$counter]['isUser'] = $res['isUser'];

                $counter++;
            }
        }
        $stmt->free_result();
        $stmt->close();
        return $ResList;
    }  
    public static function countResources($mysqliConn, $search) {
        $select_part = " SELECT COUNT(*) FROM `resources` ";
        $where_part = "";
        if ($search != "") {
            $str = composeSearch("pippo");
            
            $where_part = " WHERE ( "
                . composeSearch("codRes",$search). " OR "
                . composeSearch("Nome",$search). " OR "
                . composeSearch("Cognome",$search). " OR "
                . composeSearch("Indirizzo",$search). " OR "
                . composeSearch("Livello",$search). " OR "
                . composeSearch("Qualifica",$search). " OR "
                . composeSearch("PosizioneOrganigramma",$search). " OR "
                . composeSearch("DataNascita",$search). " OR "
                . composeSearch("Telefono",$search). " OR "
                . composeSearch("ProfiloScolastico",$search)
                . " ) ";
        }

        $countSQL_LIST_ON_DB = $select_part . " " . $where_part;

        $totElement = 0;
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($countSQL_LIST_ON_DB)) {
            $totElement = 0;
            $stmt->bind_result($totElement);
            $stmt->execute();
            while ($stmt->fetch()) {
                
            }
        } else {
            $totElement = 0;
        }
        $stmt->free_result();
        $stmt->close();
        return $totElement;
    }      
    public static function listResources($mysqliConn, $limit, $offset, $sort, $order, $search) {
        $select_part = "SELECT `idRes`,`codRes`,`Nome`,`Cognome`,`Indirizzo`,`Livello`,`Qualifica`,`PosizioneOrganigramma`,`DataNascita`,`Telefono`,`ProfiloScolastico`,`CorrispondenzaProfilo`,`Esperienza`,`isEnabled` FROM `resources` ";

        $limit_part = "";
        if ($limit > 0) {
            if ($offset > 0) {
                $limit_part = " LIMIT " . $offset . "," . $limit;
            } else {
                $limit_part = " LIMIT " . $limit;
            }
        }

        $where_part = "";
        if ($search != "") {
            $where_part = " WHERE ( "
                . composeSearch("codRes",$search). " OR "
                . composeSearch("Nome",$search). " OR "
                . composeSearch("Cognome",$search). " OR "
                . composeSearch("Indirizzo",$search). " OR "
                . composeSearch("Livello",$search). " OR "
                . composeSearch("Qualifica",$search). " OR "
                . composeSearch("PosizioneOrganigramma",$search). " OR "
                . composeSearch("DataNascita",$search). " OR "
                . composeSearch("Telefono",$search). " OR "
                . composeSearch("ProfiloScolastico",$search)
                . " ) ";
        }

        $orderpart_part = "";
        if ($sort === "codRes" || $sort === "Nome" ||$sort === "Cognome"|| $sort === "Indirizzo"||$sort === "Livello"||$sort === "Qualifica"||$sort === "PosizioneOrganigramma"||$sort === "DataNascita"||$sort === "ProfiloScolastico"||$sort === "isEnabled") {
            $orderpart_part = " ORDER BY " . $sort . " ";
            if ($order === "asc" || $order === "desc") {
                $orderpart_part = $orderpart_part . " " . $order . " ";
            }
        }

        $ResList = array();
        $res = array();
        $listSQL_LIST = $select_part . " " . $where_part . " " . $orderpart_part . " " . $limit_part;
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($listSQL_LIST)) {
            $stmt->bind_result($res['idRes'], $res['codRes'],$res['Nome'], $res['Cognome'], $res['Indirizzo'],$res['Livello'],$res['Qualifica'],$res['PosizioneOrganigramma'],$res['DataNascita'],$res['Telefono'],$res['ProfiloScolastico'],$res['CorrispondenzaProfilo'],$res['Esperienza'], $res['isEnabled']);
            $stmt->execute();
            $counter = 0;
            while ($stmt->fetch()) {
                $ResList[$counter]['idRes'] = $res['idRes'];
                $ResList[$counter]['codRes'] = $res['codRes'];
                $ResList[$counter]['Nome'] = $res['Nome'];
                $ResList[$counter]['Cognome'] = $res['Cognome'];
                $ResList[$counter]['Indirizzo'] = $res['Indirizzo'];
                $ResList[$counter]['Livello'] = $res['Livello'];
                $ResList[$counter]['Qualifica'] = $res['Qualifica'];
                $ResList[$counter]['PosizioneOrganigramma'] = $res['PosizioneOrganigramma'];
                $ResList[$counter]['DataNascita'] = $res['DataNascita'];
                $ResList[$counter]['Telefono'] = $res['Telefono'];
                $ResList[$counter]['ProfiloScolastico'] = $res['ProfiloScolastico'];
                $ResList[$counter]['CorrispondenzaProfilo'] = $res['CorrispondenzaProfilo'];
                $ResList[$counter]['Esperienza'] = $res['Esperienza'];
                $ResList[$counter]['isEnabled'] = $res['isEnabled'];
                $counter++;
            }
        }
        $stmt->free_result();
        $stmt->close();
        return $ResList;
    }      
    public static function disableResource($mysqliConn, $codUser) {
        $updated = FALSE;
        $customerDisableSQL = "UPDATE `resources` SET `isEnabled`=? WHERE codRes=? ";
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($customerDisableSQL)) {
            $disabled = intval(FALSE);
            $stmt->bind_param("is", $disabled, $codUser);
            $stmt->execute();
            if ($stmt->affected_rows > 0) {
                $updated = TRUE;
            }
        }
        $stmt->free_result();
        $stmt->close();
        return $updated;
    }
    public static function enableResource($mysqliConn, $codUser) {
        $updated = FALSE;
        $customerDisableSQL = "UPDATE `resources` SET `isEnabled`=? WHERE codRes=? ";
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($customerDisableSQL)) {
            $enabled = intval(TRUE);
            $stmt->bind_param("is", $enabled, $codUser);
            $stmt->execute();
            if ($stmt->affected_rows > 0) {
                $updated = TRUE;
            }
        }
        $stmt->free_result();
        $stmt->close();
        return $updated;
    }
    public static function checkCustomerExistence($mysqliConn, $codCustomer) {
        $countSQL_LIST_CUST_ON_DB = " SELECT COUNT(*) FROM `customers` WHERE `codCust` = ? ";

        $totElement = 0;
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($countSQL_LIST_CUST_ON_DB)) {
            $totElement = 0;
            $stmt->bind_param("s", $codCustomer);
            $stmt->bind_result($totElement);
            $stmt->execute();
            while ($stmt->fetch()) {
                
            }
        } else {
            $totElement = 0;
        }
        $stmt->free_result();
        $stmt->close();
        if ($totElement > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public static function countCustomers($mysqliConn, $search) {
        $select_part = " SELECT COUNT(*) FROM `customers` ";

        $where_part = "";
        if ($search != "") {
            $where_part = " WHERE ( "
                    . "( `codCust` LIKE '" . $search . "' OR `codCust` LIKE '%" . $search . "' OR `codCust` LIKE '" . $search . "%' OR `codCust` LIKE '%" . $search . "%' ) " . " OR "
                    . "( `descrCust` LIKE '" . $search . "' OR `descrCust` LIKE '%" . $search . "' OR `descrCust` LIKE '" . $search . "%' OR `descrCust` LIKE '%" . $search . "%' ) "
                    . " ) ";
        }

        $countSQL_LIST_CUST_ON_DB = $select_part . " " . $where_part;

        $totElement = 0;
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($countSQL_LIST_CUST_ON_DB)) {
            $totElement = 0;
            //$stmt->bind_param("s", $userCHECK_USR_ON_DB);
            $stmt->bind_result($totElement);
            $stmt->execute();
            while ($stmt->fetch()) {
                
            }
        } else {
            $totElement = 0;
        }
        $stmt->free_result();
        $stmt->close();
        return $totElement;
    }
    public static function listCustomers($mysqliConn, $limit, $offset, $sort, $order, $search) {
        $select_part = "SELECT `idCust`, `codCust`, `descrCust`, `isEnabled` FROM `customers` ";



        $limit_part = "";
        if ($limit > 0) {
            if ($offset > 0) {
                $limit_part = " LIMIT " . $offset . "," . $limit;
            } else {
                $limit_part = " LIMIT " . $limit;
            }
        }

        $where_part = "";
        if ($search != "") {
            $where_part = " WHERE ( "
                    . "( `codCust` LIKE '" . $search . "' OR `codCust` LIKE '%" . $search . "' OR `codCust` LIKE '" . $search . "%' OR `codCust` LIKE '%" . $search . "%' ) " . " OR "
                    . "( `descrCust` LIKE '" . $search . "' OR `descrCust` LIKE '%" . $search . "' OR `descrCust` LIKE '" . $search . "%' OR `descrCust` LIKE '%" . $search . "%' ) "
                    . " ) ";
        }

        $orderpart_part = "";
        if ($sort === "codCust" || $sort === "descrCust" || $sort === "isEnabled") {
            $orderpart_part = " ORDER BY " . $sort . " ";
            if ($order === "asc" || $order === "desc") {
                $orderpart_part = $orderpart_part . " " . $order . " ";
            }
        }

        $ResList = array();
        $res = array();
        $listSQL_LIST_CUSTOMERS = $select_part . " " . $where_part . " " . $orderpart_part . " " . $limit_part;
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($listSQL_LIST_CUSTOMERS)) {
            //$stmt->bind_param("s", $userCHECK_USR_ON_DB);
            $stmt->bind_result($res['idCust'], $res['codCust'], $res['descrCust'], $res['isEnabled']);
            $stmt->execute();
            $counter = 0;
            while ($stmt->fetch()) {
                $ResList[$counter]['idCust'] = $res['idCust'];
                $ResList[$counter]['codCust'] = $res['codCust'];
                $ResList[$counter]['descrCust'] = $res['descrCust'];
                $ResList[$counter]['isEnabled'] = $res['isEnabled'];
                $counter++;
            }
        }
       $stmt->free_result();
        $stmt->close();
        return $ResList;
    }
    public static function disableCustomers($mysqliConn, $codCust) {
        $updated = FALSE;
        $customerDisableSQL = "UPDATE `customers` SET `isEnabled`=? WHERE `codCust`=? ";
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($customerDisableSQL)) {
            $disabled = intval(FALSE);
            $stmt->bind_param("is", $disabled, $codCust);
            $stmt->execute();
            if ($stmt->affected_rows > 0) {
                $updated = TRUE;
            }
        }
        $stmt->free_result();
        $stmt->close();
        return $updated;
    }
    public static function addCustomer($mysqliConn, $codCust, $descrCust, $isEnabled) {
        $insertCustomerSQL = "INSERT INTO `customers`( `codCust`, `descrCust`, `isEnabled`) VALUES (?,?,?)";

        $customer_inserted = FALSE;
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($insertCustomerSQL)) {
            $stmt->bind_param("sss", $codCust, $descrCust, $isEnabled);
            if ($stmt->execute()) {
                $customer_inserted = TRUE;
            }
        }
        $stmt->free_result();
        $stmt->close();
        return $customer_inserted;
    }
    public static function countOffices($mysqliConn, $search) {
        $select_part = " SELECT COUNT(*) FROM `offices` JOIN `customers` ON (`offices`.`codCust`=`customers`.`codCust` AND `customers`.`isEnabled`<> 0)";
        $where_part = "";
        if ($search != "") {
            $where_part = " WHERE ( "
                    . "( `codOffice` LIKE '" . $search . "' OR `codOffice` LIKE '%" . $search . "' OR `codOffice` LIKE '" . $search . "%' OR `codOffice` LIKE '%" . $search . "%' ) " . " OR "
                    . "( `descrOffice` LIKE '" . $search . "' OR `descrOffice` LIKE '%" . $search . "' OR `descrOffice` LIKE '" . $search . "%' OR `descrOffice` LIKE '%" . $search . "%' ) " . " OR "
                    . "( `offices`.`codCust` LIKE '" . $search . "' OR `offices`.`codCust` LIKE '%" . $search . "' OR `offices`.`codCust` LIKE '" . $search . "%' OR `offices`.`codCust` LIKE '%" . $search . "%' ) " . " OR "
                    . "( `descrCust` LIKE '" . $search . "' OR `descrCust` LIKE '%" . $search . "' OR `descrCust` LIKE '" . $search . "%' OR `descrCust` LIKE '%" . $search . "%' ) "
                    . " ) ";
        }

        $countSQL_LIST_OFF_ON_DB = $select_part . " " . $where_part;

        $totElement = 0;
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($countSQL_LIST_OFF_ON_DB)) {
            $totElement = 0;
            //$stmt->bind_param("s", $userCHECK_USR_ON_DB);
            $stmt->bind_result($totElement);
            $stmt->execute();
            while ($stmt->fetch()) {
                
            }
        } else {
            $totElement = 0;
        }
        $stmt->free_result();
        $stmt->close();
        return $totElement;
    }
    public static function listOffices($mysqliConn, $limit, $offset, $sort, $order, $search) {
        $select_part = " SELECT `idOffice`, `codOffice`, `descrOffice`, `offices`.`isEnabled`, `offices`.`codCust`,`descrCust`,`customers`.`isEnabled` FROM `offices` JOIN `customers` ON (`offices`.`codCust`=`customers`.`codCust` AND `customers`.`isEnabled`<> 0) ";



        $limit_part = "";
        if ($limit > 0) {
            if ($offset > 0) {
                $limit_part = " LIMIT " . $offset . "," . $limit;
            } else {
                $limit_part = " LIMIT " . $limit;
            }
        }

        $where_part = "";
        if ($search != "") {
            $where_part = " WHERE ( "
                    . "( `codOffice` LIKE '" . $search . "' OR `codOffice` LIKE '%" . $search . "' OR `codOffice` LIKE '" . $search . "%' OR `codOffice` LIKE '%" . $search . "%' ) " . " OR "
                    . "( `descrOffice` LIKE '" . $search . "' OR `descrOffice` LIKE '%" . $search . "' OR `descrOffice` LIKE '" . $search . "%' OR `descrOffice` LIKE '%" . $search . "%' ) " . " OR "
                    . "( `offices`.`codCust` LIKE '" . $search . "' OR `offices`.`codCust` LIKE '%" . $search . "' OR `offices`.`codCust` LIKE '" . $search . "%' OR `offices`.`codCust` LIKE '%" . $search . "%' ) " . " OR "
                    . "( `descrCust` LIKE '" . $search . "' OR `descrCust` LIKE '%" . $search . "' OR `descrCust` LIKE '" . $search . "%' OR `descrCust` LIKE '%" . $search . "%' ) "
                    . " ) ";
        }

        $orderpart_part = "";
        if ($sort === "codOffice" || $sort === "descrOffice" || $sort === "isEnabled" || $sort === "codCust" || $sort === "descrCust") {
            if ($sort === "isEnabled" || $sort === "codCust") {
                $orderpart_part = " ORDER BY `offices`." . $sort . " ";
            } else {
                $orderpart_part = " ORDER BY " . $sort . " ";
            }

            if ($order === "asc" || $order === "desc") {
                $orderpart_part = $orderpart_part . " " . $order . " ";
            }
        }

        $ResList = array();
        $res = array();
        $listSQL_LIST_OFFICES = $select_part . " " . $where_part . " " . $orderpart_part . " " . $limit_part;
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($listSQL_LIST_OFFICES)) {
            $stmt->bind_result(
                    $res['idOffice'], $res['codOffice'], $res['descrOffice'], $res['isEnabled'], $res['codCust'], $res['descrCust'], $res['c.isEnabled']
            );
            $stmt->execute();
            $counter = 0;
            while ($stmt->fetch()) {
                $ResList[$counter]['idOffice'] = $res['idOffice'];
                $ResList[$counter]['codOffice'] = $res['codOffice'];
                $ResList[$counter]['descrOffice'] = $res['descrOffice'];
                $ResList[$counter]['isEnabled'] = $res['isEnabled'];
                $ResList[$counter]['codCust'] = $res['codCust'];
                $ResList[$counter]['descrCust'] = $res['descrCust'];
                $counter++;
            }
        }
        $stmt->free_result();
        $stmt->close();
        return $ResList;
    }
    public static function disableOffices($mysqliConn, $codOffice) {
        $updated = FALSE;
        $disableSQL = "UPDATE `offices` SET `isEnabled`=? WHERE `codOffice`=? ";
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($disableSQL)) {
            $disabled = intval(FALSE);
            $stmt->bind_param("is", $disabled, $codOffice);
            $stmt->execute();
            if ($stmt->affected_rows > 0) {
                $updated = TRUE;
            }
        }
        $stmt->free_result();
        $stmt->close();
        return $updated;
    }
    public static function checkOfficeExistence($mysqliConn, $codCustomer, $codOffice) {
        //$countSQL_LIST_CUST_ON_DB = " SELECT COUNT(*) FROM `offices` WHERE `codCust` = ? AND `codOffice` = ?";
        $countSQL_LIST_CUST_ON_DB = " SELECT COUNT(*) FROM `offices` WHERE `codOffice` = ?";
        $totElement = 0;
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($countSQL_LIST_CUST_ON_DB)) {
            $totElement = 0;
            $stmt->bind_param("s", $codOffice);
            $stmt->bind_result($totElement);
            $stmt->execute();
            while ($stmt->fetch()) {
                
            }
        } else {
            $totElement = 0;
        }
        $stmt->free_result();
        $stmt->close();
        if ($totElement > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public static function addOffice($mysqliConn, $codCust, $codOffice, $descrOffice, $isEnabled) {
        $insertOfficeSQL = "INSERT INTO `offices`( `codOffice`, `descrOffice`, `isEnabled`, `codCust`) VALUES (?,?,?,?)";

        $customer_inserted = FALSE;
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($insertOfficeSQL)) {
            $stmt->bind_param("ssss", $codOffice, $descrOffice, $isEnabled, $codCust);
            if ($stmt->execute()) {
                $customer_inserted = TRUE;
            }
        }
        $stmt->free_result();
        $stmt->close();
        return $customer_inserted;
    }
    public static function countResponsibles($mysqliConn, $search) {
        $select_part = " SELECT COUNT(*) FROM `responsibles` "
                . " JOIN `offices` ON (`responsibles`.`codOffice`=`offices`.`codOffice` AND `offices`.`isEnabled`<> 0) "
                . " JOIN `customers` ON (`offices`.`codCust`=`customers`.`codCust` AND `customers`.`isEnabled`<> 0) ";
        $where_part = "";
        if ($search != "") {
            $where_part = " WHERE ( "
                    . "( `codResp` LIKE '" . $search . "' OR `codResp` LIKE '%" . $search . "' OR `codResp` LIKE '" . $search . "%' OR `codResp` LIKE '%" . $search . "%' ) " . " OR "
                    . "( `descrResp` LIKE '" . $search . "' OR `descrResp` LIKE '%" . $search . "' OR `descrResp` LIKE '" . $search . "%' OR `descrResp` LIKE '%" . $search . "%' ) " . " OR "
                    . "( `responsibles`.`codOffice` LIKE '" . $search . "' OR `responsibles`.`codOffice` LIKE '%" . $search . "' OR `responsibles`.`codOffice` LIKE '" . $search . "%' OR `responsibles`.`codOffice` LIKE '%" . $search . "%' ) " . " OR "
                    . "( `offices`.`descrOffice` LIKE '" . $search . "' OR `offices`.`descrOffice` LIKE '%" . $search . "' OR `offices`.`descrOffice` LIKE '" . $search . "%' OR `offices`.`descrOffice` LIKE '%" . $search . "%' ) " . " OR "
                    . "( `offices`.`codCust` LIKE '" . $search . "' OR `offices`.`codCust` LIKE '%" . $search . "' OR `offices`.`codCust` LIKE '" . $search . "%' OR `offices`.`codCust` LIKE '%" . $search . "%' ) " . " OR "
                    . "( `customers`.`descrCust` LIKE '" . $search . "' OR `customers`.`descrCust` LIKE '%" . $search . "' OR `customers`.`descrCust` LIKE '" . $search . "%' OR `customers`.`descrCust` LIKE '%" . $search . "%' ) "
                    . " ) ";
        }

        $countSQL_LIST_OFF_ON_DB = $select_part . " " . $where_part;

        $totElement = 0;
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($countSQL_LIST_OFF_ON_DB)) {
            $totElement = 0;
            //$stmt->bind_param("s", $userCHECK_USR_ON_DB);
            $stmt->bind_result($totElement);
            $stmt->execute();
            while ($stmt->fetch()) {
                
            }
        } else {
            $totElement = 0;
        }
        $stmt->free_result();
        $stmt->close();
        return $totElement;
    }
    public static function listResponsibles($mysqliConn, $limit, $offset, $sort, $order, $search) {
        $select_part = " SELECT `idResp`,`codResp`,`descrResp`,`responsibles`.`isEnabled`,`responsibles`.`codOffice`,`offices`.`descrOffice`,`offices`.`isEnabled`,`offices`.`codCust`,`customers`.`descrCust`,`customers`.`isEnabled` FROM `responsibles` "
                . " JOIN `offices` ON (`responsibles`.`codOffice`=`offices`.`codOffice` AND `offices`.`isEnabled`<> 0) "
                . " JOIN `customers` ON (`offices`.`codCust`=`customers`.`codCust` AND `customers`.`isEnabled`<> 0) ";



        $limit_part = "";
        if ($limit > 0) {
            if ($offset > 0) {
                $limit_part = " LIMIT " . $offset . "," . $limit;
            } else {
                $limit_part = " LIMIT " . $limit;
            }
        }

        $where_part = "";
        if ($search != "") {
            $where_part = " WHERE ( "
                    . "( `codResp` LIKE '" . $search . "' OR `codResp` LIKE '%" . $search . "' OR `codResp` LIKE '" . $search . "%' OR `codResp` LIKE '%" . $search . "%' ) " . " OR "
                    . "( `descrResp` LIKE '" . $search . "' OR `descrResp` LIKE '%" . $search . "' OR `descrResp` LIKE '" . $search . "%' OR `descrResp` LIKE '%" . $search . "%' ) " . " OR "
                    . "( `responsibles`.`codOffice` LIKE '" . $search . "' OR `responsibles`.`codOffice` LIKE '%" . $search . "' OR `responsibles`.`codOffice` LIKE '" . $search . "%' OR `responsibles`.`codOffice` LIKE '%" . $search . "%' ) " . " OR "
                    . "( `offices`.`descrOffice` LIKE '" . $search . "' OR `offices`.`descrOffice` LIKE '%" . $search . "' OR `offices`.`descrOffice` LIKE '" . $search . "%' OR `offices`.`descrOffice` LIKE '%" . $search . "%' ) " . " OR "
                    . "( `offices`.`codCust` LIKE '" . $search . "' OR `offices`.`codCust` LIKE '%" . $search . "' OR `offices`.`codCust` LIKE '" . $search . "%' OR `offices`.`codCust` LIKE '%" . $search . "%' ) " . " OR "
                    . "( `customers`.`descrCust` LIKE '" . $search . "' OR `customers`.`descrCust` LIKE '%" . $search . "' OR `customers`.`descrCust` LIKE '" . $search . "%' OR `customers`.`descrCust` LIKE '%" . $search . "%' ) "
                    . " ) ";
        }

        $orderpart_part = "";
        if ($sort === "isEnabled" ||
                $sort === "codResp" || $sort === "descrResp" ||
                $sort === "codOffice" || $sort === "descrOffice" ||
                $sort === "codCust" || $sort === "descrCust") {


            if ($sort === "descrCust") {
                $orderpart_part = " ORDER BY `customers`." . $sort . " ";
            } else if ($sort === "codCust") {
                $orderpart_part = " ORDER BY `offices`." . $sort . " ";
            } else if ($sort === "descrOffice") {
                $orderpart_part = " ORDER BY `offices`." . $sort . " ";
            } else if ($sort === "codOffice") {
                $orderpart_part = " ORDER BY `responsibles`." . $sort . " ";
            } else if ($sort === "isEnabled") {
                $orderpart_part = " ORDER BY `responsibles`." . $sort . " ";
            } else {
                $orderpart_part = " ORDER BY " . $sort . " ";
            }

            if ($order === "asc" || $order === "desc") {
                $orderpart_part = $orderpart_part . " " . $order . " ";
            }
        }

        $ResList = array();
        $res = array();
        $listSQL_LIST_OFFICES = $select_part . " " . $where_part . " " . $orderpart_part . " " . $limit_part;
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($listSQL_LIST_OFFICES)) {
            $stmt->bind_result(
                    $res['idResp'], $res['codResp'], $res['descrResp'], $res['isEnabled'], $res['codOffice'], $res['descrOffice'], $res['o.isEnabled'], $res['codCust'], $res['descrCust'], $res['c.isEnabled']
            );
            $stmt->execute();
            $counter = 0;
            while ($stmt->fetch()) {
                $ResList[$counter]['idResp'] = $res['idResp'];
                $ResList[$counter]['codResp'] = $res['codResp'];
                $ResList[$counter]['descrResp'] = $res['descrResp'];
                $ResList[$counter]['isEnabled'] = $res['isEnabled'];
                $ResList[$counter]['codOffice'] = $res['codOffice'];
                $ResList[$counter]['descrOffice'] = $res['descrOffice'];
                $ResList[$counter]['codCust'] = $res['codCust'];
                $ResList[$counter]['descrCust'] = $res['descrCust'];
                $counter++;
            }
        }
        $stmt->free_result();
        $stmt->close();
        return $ResList;
    }
    public static function disableResponsibles($mysqliConn, $codResp) {
        $updated = FALSE;
        $disableSQL = "UPDATE `responsibles` SET `isEnabled`=? WHERE `codResp`=? ";
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($disableSQL)) {
            $disabled = intval(FALSE);
            $stmt->bind_param("is", $disabled, $codResp);
            $stmt->execute();
            if ($stmt->affected_rows > 0) {
                $updated = TRUE;
            }
        }
        $stmt->free_result();
        $stmt->close();
        return $updated;
    }
    public static function checkResponsibleExistence($mysqliConn, $codCustomer, $codOffice, $codResp) {
        $countSQL_LIST_CUST_ON_DB = " SELECT COUNT(*) FROM `responsibles` WHERE `codResp` = ?";

        $totElement = 0;
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($countSQL_LIST_CUST_ON_DB)) {
            $totElement = 0;
            $stmt->bind_param("s", $codResp);
            $stmt->bind_result($totElement);
            $stmt->execute();
            while ($stmt->fetch()) {
                
            }
        } else {
            $totElement = 0;
        }
       $stmt->free_result();
        $stmt->close();
        if ($totElement > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public static function addResource($mysqliConn, $codRes, $Nome, $Cognome, $isEnabled) {
        $insertOfficeSQL = "INSERT INTO `resources` (`codRes`, `Nome`, `Cognome`,`isEnabled`)  VALUES (?,?,?,?)";

        $resource_inserted = FALSE;
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($insertOfficeSQL)) {
            $stmt->bind_param("ssss", $codRes, $Nome, $Cognome, $isEnabled);
            if ($stmt->execute()) {
                $resource_inserted = TRUE;
            }
        }
        $stmt->free_result();
        $stmt->close();
        return $resource_inserted;
    }
    public static function addResponsible($mysqliConn, $codOffice, $codResp, $descrResp, $isEnabled) {
        $insertOfficeSQL = "INSERT INTO `responsibles`( `codResp`, `descrResp`, `isEnabled`, `codOffice`) VALUES (?,?,?,?)";

        $customer_inserted = FALSE;
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($insertOfficeSQL)) {
            $stmt->bind_param("ssss", $codResp, $descrResp, $isEnabled, $codOffice);
            if ($stmt->execute()) {
                $customer_inserted = TRUE;
            }
        }
        $stmt->free_result();
        $stmt->close();
        return $customer_inserted;
    }
    public static function countUserResponsibles($mysqliConn, $search) {
        $select_part = " SELECT COUNT(*) FROM `users` 
                        JOIN `responsibles` ON (`users`.`codUser`=`responsibles`.`codResp` AND `responsibles`.`isEnabled`<> 0 ) 
                        JOIN `offices` ON (`responsibles`.`codOffice`=`offices`.`codOffice` AND `offices`.`isEnabled`<> 0)
                        JOIN `customers` ON (`offices`.`codCust`=`customers`.`codCust` AND `customers`.`isEnabled`<> 0) ";
        $where_part = "";
        if ($search != "") {
            $where_part = " WHERE ( "
                    . "( `users`.`codUser`          LIKE '" . $search . "' OR `users`.`codUser`             LIKE '%" . $search . "' OR `users`.`codUser`            LIKE '" . $search . "%' OR `users`.`codUser`            LIKE '%" . $search . "%' ) " . " OR "
                    . "( `responsibles`.`codResp`   LIKE '" . $search . "' OR `responsibles`.`codResp`      LIKE '%" . $search . "' OR `responsibles`.`codResp`     LIKE '" . $search . "%' OR `responsibles`.`codResp`     LIKE '%" . $search . "%' ) " . " OR "
                    . "( `responsibles`.`descrResp` LIKE '" . $search . "' OR `responsibles`.`descrResp`    LIKE '%" . $search . "' OR `responsibles`.`descrResp`   LIKE '" . $search . "%' OR `responsibles`.`descrResp`   LIKE '%" . $search . "%' ) " . " OR "
                    . "( `responsibles`.`codOffice` LIKE '" . $search . "' OR `responsibles`.`codOffice`    LIKE '%" . $search . "' OR `responsibles`.`codOffice`   LIKE '" . $search . "%' OR `responsibles`.`codOffice`   LIKE '%" . $search . "%' ) " . " OR "
                    . "( `offices`.`descrOffice`    LIKE '" . $search . "' OR `offices`.`descrOffice`       LIKE '%" . $search . "' OR `offices`.`descrOffice`      LIKE '" . $search . "%' OR `offices`.`descrOffice`      LIKE '%" . $search . "%' ) " . " OR "
                    . "( `offices`.`codCust`        LIKE '" . $search . "' OR `offices`.`codCust`           LIKE '%" . $search . "' OR `offices`.`codCust`          LIKE '" . $search . "%' OR `offices`.`codCust`          LIKE '%" . $search . "%' ) " . " OR "
                    . "( `customers`.`descrCust`    LIKE '" . $search . "' OR `customers`.`descrCust`       LIKE '%" . $search . "' OR `customers`.`descrCust`      LIKE '" . $search . "%' OR `customers`.`descrCust`      LIKE '%" . $search . "%' ) "
                    . " ) ";
        }

        $countSQL_LIST_OFF_ON_DB = $select_part . " " . $where_part;

        $totElement = 0;
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($countSQL_LIST_OFF_ON_DB)) {
            $totElement = 0;
            //$stmt->bind_param("s", $userCHECK_USR_ON_DB);
            $stmt->bind_result($totElement);
            $stmt->execute();
            while ($stmt->fetch()) {
                
            }
        } else {
            $totElement = 0;
        }
       $stmt->free_result();
        $stmt->close();
        return $totElement;
    }
    public static function listUserResponsibles($mysqliConn, $limit, $offset, $sort, $order, $search) {
        $select_part = "SELECT `users`.`idUser`,`users`.`codUser`,`users`.`isEnabled`,`responsibles`.`idResp`,`responsibles`.`codResp`,`responsibles`.`descrResp`,`responsibles`.`isEnabled`,`responsibles`.`codOffice`,`offices`.`descrOffice`,`offices`.`isEnabled`,`offices`.`codCust`,`customers`.`descrCust`,`customers`.`isEnabled` "
                . " FROM `users` "
                . " JOIN `responsibles` ON (`users`.`codUser`=`responsibles`.`codResp` AND `responsibles`.`isEnabled`<> 0 )  "
                . " JOIN `offices` ON (`responsibles`.`codOffice`=`offices`.`codOffice` AND `offices`.`isEnabled`<> 0) "
                . " JOIN `customers` ON (`offices`.`codCust`=`customers`.`codCust` AND `customers`.`isEnabled`<> 0) ";

        $limit_part = "";
        if ($limit > 0) {
            if ($offset > 0) {
                $limit_part = " LIMIT " . $offset . "," . $limit;
            } else {
                $limit_part = " LIMIT " . $limit;
            }
        }

        $where_part = "";
        if ($search != "") {
            $where_part = " WHERE ( "
                    . "( `users`.`codUser`          LIKE '" . $search . "' OR `users`.`codUser`             LIKE '%" . $search . "' OR `users`.`codUser`            LIKE '" . $search . "%' OR `users`.`codUser`            LIKE '%" . $search . "%' ) " . " OR "
                    . "( `responsibles`.`codResp`   LIKE '" . $search . "' OR `responsibles`.`codResp`      LIKE '%" . $search . "' OR `responsibles`.`codResp`     LIKE '" . $search . "%' OR `responsibles`.`codResp`     LIKE '%" . $search . "%' ) " . " OR "
                    . "( `responsibles`.`descrResp` LIKE '" . $search . "' OR `responsibles`.`descrResp`    LIKE '%" . $search . "' OR `responsibles`.`descrResp`   LIKE '" . $search . "%' OR `responsibles`.`descrResp`   LIKE '%" . $search . "%' ) " . " OR "
                    . "( `responsibles`.`codOffice` LIKE '" . $search . "' OR `responsibles`.`codOffice`    LIKE '%" . $search . "' OR `responsibles`.`codOffice`   LIKE '" . $search . "%' OR `responsibles`.`codOffice`   LIKE '%" . $search . "%' ) " . " OR "
                    . "( `offices`.`descrOffice`    LIKE '" . $search . "' OR `offices`.`descrOffice`       LIKE '%" . $search . "' OR `offices`.`descrOffice`      LIKE '" . $search . "%' OR `offices`.`descrOffice`      LIKE '%" . $search . "%' ) " . " OR "
                    . "( `offices`.`codCust`        LIKE '" . $search . "' OR `offices`.`codCust`           LIKE '%" . $search . "' OR `offices`.`codCust`          LIKE '" . $search . "%' OR `offices`.`codCust`          LIKE '%" . $search . "%' ) " . " OR "
                    . "( `customers`.`descrCust`    LIKE '" . $search . "' OR `customers`.`descrCust`       LIKE '%" . $search . "' OR `customers`.`descrCust`      LIKE '" . $search . "%' OR `customers`.`descrCust`      LIKE '%" . $search . "%' ) "
                    . " ) ";
        }

        $orderpart_part = "";
        if ($sort === "isEnabled" ||
                $sort === "codResp" || $sort === "descrResp" ||
                $sort === "codOffice" || $sort === "descrOffice" ||
                $sort === "codCust" || $sort === "descrCust") {


            if ($sort === "descrCust") {
                $orderpart_part = " ORDER BY `customers`." . $sort . " ";
            } else if ($sort === "codCust") {
                $orderpart_part = " ORDER BY `offices`." . $sort . " ";
            } else if ($sort === "descrOffice") {
                $orderpart_part = " ORDER BY `offices`." . $sort . " ";
            } else if ($sort === "codOffice") {
                $orderpart_part = " ORDER BY `responsibles`." . $sort . " ";
            } else if ($sort === "isEnabled") {
                $orderpart_part = " ORDER BY `users`." . $sort . " ";
            } else {
                $orderpart_part = " ORDER BY " . $sort . " ";
            }

            if ($order === "asc" || $order === "desc") {
                $orderpart_part = $orderpart_part . " " . $order . " ";
            }
        }

        $ResList = array();
        $res = array();
        $listSQL_LIST_OFFICES = $select_part . " " . $where_part . " " . $orderpart_part . " " . $limit_part;
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($listSQL_LIST_OFFICES)) {
            $stmt->bind_result(
                    $res['idUser'], $res['codUser'], $res['isEnabled'], $res['idResp'], $res['codResp'], $res['descrResp'], $res['r.isEnabled'], $res['codOffice'], $res['descrOffice'], $res['o.isEnabled'], $res['codCust'], $res['descrCust'], $res['c.isEnabled']
            );
            $stmt->execute();
            $counter = 0;
            while ($stmt->fetch()) {
                $ResList[$counter]['idUser'] = $res['idUser'];
                $ResList[$counter]['codUser'] = $res['codUser'];
                $ResList[$counter]['isEnabled'] = $res['isEnabled'];
                $ResList[$counter]['idResp'] = $res['idResp'];
                $ResList[$counter]['codResp'] = $res['codResp'];
                $ResList[$counter]['descrResp'] = $res['descrResp'];
                $ResList[$counter]['codOffice'] = $res['codOffice'];
                $ResList[$counter]['descrOffice'] = $res['descrOffice'];
                $ResList[$counter]['codCust'] = $res['codCust'];
                $ResList[$counter]['descrCust'] = $res['descrCust'];
                $counter++;
            }
        }
        $stmt->free_result();
        $stmt->close();
        return $ResList;
    }
    public static function disableUser($mysqliConn, $codUser) {
        $updated = FALSE;
        $customerDisableSQL = "UPDATE `users` SET `isEnabled`=? WHERE codUser=? ";
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($customerDisableSQL)) {
            $disabled = intval(FALSE);
            $stmt->bind_param("is", $disabled, $codUser);
            $stmt->execute();
            if ($stmt->affected_rows > 0) {
                $updated = TRUE;
            }
        }
        $stmt->free_result();
        $stmt->close();
        return $updated;
    }
    public static function enableUser($mysqliConn, $codUser) {
        $updated = FALSE;
        $customerDisableSQL = "UPDATE `users` SET `isEnabled`=? WHERE codUser=? ";
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($customerDisableSQL)) {
            $enabled = intval(TRUE);
            $stmt->bind_param("is", $enabled, $codUser);
            $stmt->execute();
            if ($stmt->affected_rows > 0) {
                $updated = TRUE;
            }
        }
        $stmt->free_result();
        $stmt->close();
        return $updated;
    }
    public static function countOffers($mysqliConn, $search) {
        $select_part = "  SELECT COUNT(*) FROM `offers` "
                . " JOIN `responsibles` ON (`offers`.`codResp`=`responsibles`.`codResp` AND `responsibles`.`isEnabled`<> 0) "
                . " JOIN `offices` ON (`offers`.`codOffice`=`offices`.`codOffice` AND `offices`.`isEnabled`<> 0) "
                . " JOIN `customers` ON (`offices`.`codCust`=`customers`.`codCust` AND `customers`.`isEnabled`<> 0) ";
        $where_part = "";
        if ($search != "") {
            $where_part = " WHERE ( "
                    . "( `offers`.`codOffer`        LIKE '" . $search . "' OR `offers`.`codOffer`           LIKE '%" . $search . "' OR `offers`.`codOffer`          LIKE '" . $search . "%' OR `offers`.`codOffer`          LIKE '%" . $search . "%' ) " . " OR "
                    . "( `offers`.`descrOffer`      LIKE '" . $search . "' OR `offers`.`descrOffer`         LIKE '%" . $search . "' OR `offers`.`descrOffer`        LIKE '" . $search . "%' OR `offers`.`descrOffer`        LIKE '%" . $search . "%' ) " . " OR "
                    . "( `offers`.`codResp`         LIKE '" . $search . "' OR `offers`.`codResp`            LIKE '%" . $search . "' OR `offers`.`codResp`           LIKE '" . $search . "%' OR `offers`.`codResp`           LIKE '%" . $search . "%' ) " . " OR "
                    . "( `responsibles`.`descrResp` LIKE '" . $search . "' OR `responsibles`.`descrResp`    LIKE '%" . $search . "' OR `responsibles`.`descrResp`   LIKE '" . $search . "%' OR `responsibles`.`descrResp`   LIKE '%" . $search . "%' ) " . " OR "
                    . "( `offers`.`codOffice`       LIKE '" . $search . "' OR `offers`.`codOffice`          LIKE '%" . $search . "' OR `offers`.`codOffice`         LIKE '" . $search . "%' OR `offers`.`codOffice`         LIKE '%" . $search . "%' ) " . " OR "
                    . "( `offices`.`descrOffice`    LIKE '" . $search . "' OR `offices`.`descrOffice`       LIKE '%" . $search . "' OR `offices`.`descrOffice`      LIKE '" . $search . "%' OR `offices`.`descrOffice`      LIKE '%" . $search . "%' ) " . " OR "
                    . "( `offices`.`codCust`        LIKE '" . $search . "' OR `offices`.`codCust`           LIKE '%" . $search . "' OR `offices`.`codCust`          LIKE '" . $search . "%' OR `offices`.`codCust`          LIKE '%" . $search . "%' ) " . " OR "
                    . "( `customers`.`descrCust`    LIKE '" . $search . "' OR `customers`.`descrCust`       LIKE '%" . $search . "' OR `customers`.`descrCust`      LIKE '" . $search . "%' OR `customers`.`descrCust`      LIKE '%" . $search . "%' ) "
                    . " ) ";
        }

        $countSQL_LIST_OFF_ON_DB = $select_part . " " . $where_part;

        $totElement = 0;
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($countSQL_LIST_OFF_ON_DB)) {
            $totElement = 0;
            //$stmt->bind_param("s", $userCHECK_USR_ON_DB);
            $stmt->bind_result($totElement);
            $stmt->execute();
            while ($stmt->fetch()) {
                
            }
        } else {
            $totElement = 0;
        }
        $stmt->free_result();
        $stmt->close();
        return $totElement;
    }
    public static function listOffers($mysqliConn, $limit, $offset, $sort, $order, $search) {
        $select_part = "SELECT  `offers`.`codOffer`,`offers`.`descrOffer`,`offers`.`hour`,`offers`.`isEnabled`,`offers`.`type`,`responsibles`.`codResp`,`responsibles`.`descrResp`,`responsibles`.`isEnabled`,`offices`.`codOffice`,`offices`.`descrOffice`,`offices`.`isEnabled`,`customers`.`codCust`,`customers`.`descrCust`,`customers`.`isEnabled`"
                . "  FROM `offers` "
                . " JOIN `responsibles` ON (`offers`.`codResp`=`responsibles`.`codResp` AND `responsibles`.`isEnabled`<> 0) "
                . " JOIN `offices` ON (`offers`.`codOffice`=`offices`.`codOffice` AND `offices`.`isEnabled`<> 0) "
                . " JOIN `customers` ON (`offices`.`codCust`=`customers`.`codCust` AND `customers`.`isEnabled`<> 0) ";

        $limit_part = "";
        if ($limit > 0) {
            if ($offset > 0) {
                $limit_part = " LIMIT " . $offset . "," . $limit;
            } else {
                $limit_part = " LIMIT " . $limit;
            }
        }

        $where_part = "";
        if ($search != "") {
            $where_part = " WHERE ( "
                    . "( `offers`.`codOffer`        LIKE '" . $search . "' OR `offers`.`codOffer`           LIKE '%" . $search . "' OR `offers`.`codOffer`          LIKE '" . $search . "%' OR `offers`.`codOffer`          LIKE '%" . $search . "%' ) " . " OR "
                    . "( `offers`.`descrOffer`      LIKE '" . $search . "' OR `offers`.`descrOffer`         LIKE '%" . $search . "' OR `offers`.`descrOffer`        LIKE '" . $search . "%' OR `offers`.`descrOffer`        LIKE '%" . $search . "%' ) " . " OR "
                    . "( `offers`.`codResp`         LIKE '" . $search . "' OR `offers`.`codResp`            LIKE '%" . $search . "' OR `offers`.`codResp`           LIKE '" . $search . "%' OR `offers`.`codResp`           LIKE '%" . $search . "%' ) " . " OR "
                    . "( `offers`.`type`            LIKE '" . $search . "' OR `offers`.`type`               LIKE '%" . $search . "' OR `offers`.`type`              LIKE '" . $search . "%' OR `offers`.`type`              LIKE '%" . $search . "%' ) " . " OR "
                    . "( `responsibles`.`descrResp` LIKE '" . $search . "' OR `responsibles`.`descrResp`    LIKE '%" . $search . "' OR `responsibles`.`descrResp`   LIKE '" . $search . "%' OR `responsibles`.`descrResp`   LIKE '%" . $search . "%' ) " . " OR "
                    . "( `offers`.`codOffice`       LIKE '" . $search . "' OR `offers`.`codOffice`          LIKE '%" . $search . "' OR `offers`.`codOffice`         LIKE '" . $search . "%' OR `offers`.`codOffice`         LIKE '%" . $search . "%' ) " . " OR "
                    . "( `offices`.`descrOffice`    LIKE '" . $search . "' OR `offices`.`descrOffice`       LIKE '%" . $search . "' OR `offices`.`descrOffice`      LIKE '" . $search . "%' OR `offices`.`descrOffice`      LIKE '%" . $search . "%' ) " . " OR "
                    . "( `offices`.`codCust`        LIKE '" . $search . "' OR `offices`.`codCust`           LIKE '%" . $search . "' OR `offices`.`codCust`          LIKE '" . $search . "%' OR `offices`.`codCust`          LIKE '%" . $search . "%' ) " . " OR "
                    . "( `customers`.`descrCust`    LIKE '" . $search . "' OR `customers`.`descrCust`       LIKE '%" . $search . "' OR `customers`.`descrCust`      LIKE '" . $search . "%' OR `customers`.`descrCust`      LIKE '%" . $search . "%' ) "
                    . " ) ";
        }

        $orderpart_part = "";
        if ($sort === "isEnabled" ||
                $sort === "codOffer" || $sort === "descrOffer" || $sort === "hour" ||
                $sort === "codResp" || $sort === "descrResp" ||
                $sort === "codOffice" || $sort === "descrOffice" ||
                $sort === "codCust" || $sort === "descrCust") {


            if ($sort === "descrCust") {
                $orderpart_part = " ORDER BY `customers`." . $sort . " ";
            } else if ($sort === "codCust") {
                $orderpart_part = " ORDER BY `offices`." . $sort . " ";
            } else if ($sort === "descrOffice") {
                $orderpart_part = " ORDER BY `offices`." . $sort . " ";
            } else if ($sort === "codOffice") {
                $orderpart_part = " ORDER BY `offers`." . $sort . " ";
            } else if ($sort === "descrResp") {
                $orderpart_part = " ORDER BY `responsibles`." . $sort . " ";
            } else if ($sort === "codResp") {
                $orderpart_part = " ORDER BY `offers`." . $sort . " ";
            } else if ($sort === "descrOffer") {
                $orderpart_part = " ORDER BY `offers`." . $sort . " ";
            } else if ($sort === "codOffer") {
                $orderpart_part = " ORDER BY `offers`." . $sort . " ";
            } else if ($sort === "isEnabled") {
                $orderpart_part = " ORDER BY `offers`." . $sort . " ";
            } else if ($sort === "hour") {
                $orderpart_part = " ORDER BY `offers`." . $sort . " ";
            } else if ($sort === "type") {
                $orderpart_part = " ORDER BY `offers`." . $sort . " ";       
            } else {
                $orderpart_part = " ORDER BY " . $sort . " ";
            }

            if ($order === "asc" || $order === "desc") {
                $orderpart_part = $orderpart_part . " " . $order . " ";
            }
        }

        $ResList = array();
        $res = array();
        $listSQL_LIST_OFFICES = $select_part . " " . $where_part . " " . $orderpart_part . " " . $limit_part;
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($listSQL_LIST_OFFICES)) {
            $stmt->bind_result(
                    $res['codOffer'], $res['descrOffer'], $res['hour'], $res['isEnabled'],$res['type'], $res['codResp'], $res['descrResp'], $res['r.isEnabled'], $res['codOffice'], $res['descrOffice'], $res['o.isEnabled'], $res['codCust'], $res['descrCust'], $res['c.isEnabled']
            );
            $stmt->execute();
            $counter = 0;
            while ($stmt->fetch()) {
                $ResList[$counter]['codOffer'] = $res['codOffer'];
                $ResList[$counter]['descrOffer'] = $res['descrOffer'];
                $ResList[$counter]['hour'] = $res['hour'];
                $ResList[$counter]['isEnabled'] = $res['isEnabled'];
                $ResList[$counter]['type'] = $res['type'];
                $ResList[$counter]['codResp'] = $res['codResp'];
                $ResList[$counter]['descrResp'] = $res['descrResp'];
                $ResList[$counter]['codOffice'] = $res['codOffice'];
                $ResList[$counter]['descrOffice'] = $res['descrOffice'];
                $ResList[$counter]['codCust'] = $res['codCust'];
                $ResList[$counter]['descrCust'] = $res['descrCust'];
                $counter++;
            }
        }
       $stmt->free_result();
        $stmt->close();
        return $ResList;
    }
    public static function disableOffers($mysqliConn, $codOffer) {
        $updated = FALSE;
        $disableSQL = "UPDATE `offers` SET `isEnabled`=? WHERE `codOffer`=? ";
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($disableSQL)) {
            $disabled = intval(FALSE);
            $stmt->bind_param("is", $disabled, $codOffer);
            $stmt->execute();
            if ($stmt->affected_rows > 0) {
                $updated = TRUE;
            }
        }
        $stmt->free_result();
        $stmt->close();
        return $updated;
    }
    public static function checkOfferExistence($mysqliConn, $codOffer) {

        $countSQL_LIST_CUST_ON_DB = " SELECT COUNT(*) FROM `offers` WHERE `codOffer` = ?";
        $totElement = 0;
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($countSQL_LIST_CUST_ON_DB)) {
            $totElement = 0;
            $stmt->bind_param("s", $codOffer);
            $stmt->bind_result($totElement);
            $stmt->execute();
            while ($stmt->fetch()) {
                
            }
        } else {
            $totElement = 0;
        }
        $stmt->free_result();
        $stmt->close();
        if ($totElement > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
    public static function addOffer($mysqliConn, $codOffer, $descrOffer, $codOffice, $codResp, $hour,$type, $isEnabled) {
        $insertOfficeSQL = "INSERT INTO `offers`( `codOffer`, `descrOffer`, `codOffice`, `codResp`, `hour`, `type`, `isEnabled`) VALUES (?,?,?,?,?,?,?)";
        $customer_inserted = FALSE;
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($insertOfficeSQL)) {
            $stmt->bind_param("ssssiis", $codOffer, $descrOffer, $codOffice, $codResp, $hour, $type, $isEnabled);
            if ($stmt->execute()) {
                $customer_inserted = TRUE;
            }
        }
        $stmt->free_result();
        $stmt->close();
        return $customer_inserted;
    }
    public static function countListActivity($mysqliConn, $month, $year, $user) {

        $countSQL_LIST_Activity = " SELECT COUNT(*) FROM `activities` "
                . " WHERE "
                . " YEAR(`activityDate`)=? "
                . " AND "
                . " MONTH(`activityDate`)=? "
                . " AND "
                . " codUser = ?";
        $totElement = 0;
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($countSQL_LIST_Activity)) {
            $stmt->bind_param("iis", $year, $month, $user);
            $totElement = 0;
            $stmt->bind_result($totElement);
            $stmt->execute();
            while ($stmt->fetch()) {
                
            }
        } else {
            $totElement = 0;
        }
        $stmt->free_result();
        $stmt->close();
        return $totElement;
    }
    public static function listActivity($mysqliConn, $month, $year, $user) {

        $ResList = array();
        $res = array();

        $listActivitySQL = "SELECT `idAct`,`codUser`,`activityDate`, YEAR(`activityDate`), MONTH(`activityDate`),DAY(`activityDate`),`activityDuration`,`activityOfferta`,`activityDescription` "
                . " FROM `activities`"
                . " WHERE "
                . " YEAR(`activityDate`)=? "
                . " AND "
                . " MONTH(`activityDate`)=? "
                . " AND "
                . " codUser = ?";

        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($listActivitySQL)) {
            $stmt->bind_param("iis", $year, $month, $user);

            $stmt->bind_result(
                    $res['idAct'], $res['codUser'], $res['activityDate'], $res['activityDate_year'], $res['activityDate_month'], $res['activityDate_day'], $res['activityDuration'], $res['activityOfferta'], $res['activityDescription']
            );
            $stmt->execute();
            $counter = 0;

            while ($stmt->fetch()) {
                $ResList[$counter]['idAct'] = $res['idAct'];
                $ResList[$counter]['codUser'] = $res['codUser'];
                $ResList[$counter]['activityDate'] = $res['activityDate'];
                $ResList[$counter]['activityDate_year'] = $res['activityDate_year'];
                $ResList[$counter]['activityDate_month'] = $res['activityDate_month'];
                $ResList[$counter]['activityDate_day'] = $res['activityDate_day'];
                $ResList[$counter]['activityDuration'] = $res['activityDuration'];
                $ResList[$counter]['activityOfferta'] = $res['activityOfferta'];
                $ResList[$counter]['activityDescription'] = $res['activityDescription'];

                $counter++;
            }
        }
        $stmt->free_result();
        $stmt->close();
        return $ResList;
    }
    public static function listReportCombination($mysqliConn, $month, $year, $user) {

        $ResList = array();
        $res = array();

        $listActivitySQL = " SELECT Distinct "
                . " customers.codCust, "
                . " customers.descrCust, "
                . " offices.codOffice, "
                . " offices.descrOffice, "
                . " responsibles.codResp, "
                . " responsibles.descrResp "
                . " FROM (users "
                . " JOIN activities ON users.coduser = activities.coduser "
                . " JOIN offers ON activities.activityOfferta=offers.codOffer "
                . " JOIN offices ON offers.codOffice=offices.codOffice "
                . " JOIN customers ON offices.codCust= customers.codCust "
                . " JOIN responsibles ON offers.codResp=responsibles.codResp) "
                . " WHERE "
                . " users.coduser=? "
                . " AND "
                . " YEAR(activities.activityDate)=? "
                . " AND "
                . " MONTH(activities.activityDate)=?";

        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($listActivitySQL)) {
            $stmt->bind_param("sii", $user, $year, $month);

            $stmt->bind_result(
                    $res['codCust'], $res['descrCust'], $res['codOffice'], $res['descrOffice'], $res['codResp'], $res['descrResp']
            );
            $stmt->execute();
            $counter = 0;

            while ($stmt->fetch()) {
                $ResList[$counter]['codCust'] = $res['codCust'];
                $ResList[$counter]['descrCust'] = $res['descrCust'];
                $ResList[$counter]['codOffice'] = $res['codOffice'];
                $ResList[$counter]['descrOffice'] = $res['descrOffice'];
                $ResList[$counter]['codResp'] = $res['codResp'];
                $ResList[$counter]['descrResp'] = $res['descrResp'];

                $counter++;
            }
        }
        $stmt->free_result();
        $stmt->close();
        return $ResList;
    }
    public static function countUserDailyActivities($mysqliConn, $search, $day, $month, $year, $user) {
        $select_part = " SELECT COUNT(*) "
                . " FROM `activities` "
                . " WHERE "
                . " DAY(`activityDate`)=" . $day . " "
                . " AND "
                . " YEAR(`activityDate`)=" . $year . " "
                . " AND "
                . " MONTH(`activityDate`)=" . $month . " "
                . " AND "
                . " codUser ='" . $user . "' ";

        $where_part = "";
        if ($search != "") {
            $where_part = " AND ( "
                    . "( `activityDescription` LIKE '" . $search . "' OR `activityDescription` LIKE '%" . $search . "' OR `activityDescription` LIKE '" . $search . "%' OR `activityDescription` LIKE '%" . $search . "%' ) " . " OR "
                    . "( `activityType` LIKE '" . $search . "' OR `activityType` LIKE '%" . $search . "' OR `activityType` LIKE '" . $search . "%' OR `activityType` LIKE '%" . $search . "%' ) " . " OR "
                    . "( `activityOfferta` LIKE '" . $search . "' OR `activityOfferta` LIKE '%" . $search . "' OR `activityOfferta` LIKE '" . $search . "%' OR `activityOfferta` LIKE '%" . $search . "%' ) "
                    . " ) ";
        }

        $countSQL_LIST_CUST_ON_DB = $select_part . " " . $where_part;

        $totElement = 0;
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($countSQL_LIST_CUST_ON_DB)) {
            $totElement = 0;
            //$stmt->bind_param("s", $userCHECK_USR_ON_DB);
            $stmt->bind_result($totElement);
            $stmt->execute();
            while ($stmt->fetch()) {
                
            }
        } else {
            $totElement = 0;
        }
        $stmt->free_result();
        $stmt->close();
        return $totElement;
    }
    public static function listUserDailyActivities($mysqliConn, $limit, $offset, $sort, $order, $search, $day, $month, $year, $user) {
        $select_part = "SELECT `idAct`,`codUser`,`activityDate`, YEAR(`activityDate`), MONTH(`activityDate`),DAY(`activityDate`),activityType,`activityDuration`,`activityOfferta`,`activityDescription` "
                . " FROM `activities` "
                . " WHERE "
                . " DAY(`activityDate`)=" . $day . " "
                . " AND "
                . " YEAR(`activityDate`)=" . $year . " "
                . " AND "
                . " MONTH(`activityDate`)=" . $month . " "
                . " AND "
                . " codUser ='" . $user . "' ";


        $limit_part = "";
        if ($limit > 0) {
            if ($offset > 0) {
                $limit_part = " LIMIT " . $offset . "," . $limit;
            } else {
                $limit_part = " LIMIT " . $limit;
            }
        }

        $where_part = "";
        if ($search != "") {
            $where_part = " AND ( "
                    . "( `activityDescription` LIKE '" . $search . "' OR `activityDescription` LIKE '%" . $search . "' OR `activityDescription` LIKE '" . $search . "%' OR `activityDescription` LIKE '%" . $search . "%' ) " . " OR "
                    . "( `activityOfferta` LIKE '" . $search . "' OR `activityOfferta` LIKE '%" . $search . "' OR `activityOfferta` LIKE '" . $search . "%' OR `activityOfferta` LIKE '%" . $search . "%' ) " . " OR "
                    . "( `activityType` LIKE '" . $search . "' OR `activityType` LIKE '%" . $search . "' OR `activityType` LIKE '" . $search . "%' OR `activityType` LIKE '%" . $search . "%' ) "
                    . " ) ";
        }

        $orderpart_part = "";
        if ($sort === "activityDate" || $sort === "activityDuration" || $sort === "activityOfferta") {
            $orderpart_part = " ORDER BY " . $sort . " ";
            if ($order === "asc" || $order === "desc") {
                $orderpart_part = $orderpart_part . " " . $order . " ";
            }
        }

        $ResList = array();
        $res = array();
        $listSQL_LIST_CUSTOMERS = $select_part . " " . $where_part . " " . $orderpart_part . " " . $limit_part;
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($listSQL_LIST_CUSTOMERS)) {
            //$stmt->bind_param("s", $userCHECK_USR_ON_DB);
            $stmt->bind_result($res['idAct'], $res['codUser'], $res['activityDate'], $res[' YEAR_activityDate'], $res['MONTH_activityDate'], $res['DAY_activityDate'], $res['activityType'], $res['activityDuration'], $res['activityOfferta'], $res['activityDescription']);
            $stmt->execute();
            $counter = 0;
            while ($stmt->fetch()) {
                $ResList[$counter]['idAct'] = $res['idAct'];
                $ResList[$counter]['codUser'] = $res['codUser'];
                $ResList[$counter]['activityDate'] = $res['activityDate'];
                $ResList[$counter]['activityDuration'] = $res['activityDuration'];
                $ResList[$counter]['activityOfferta'] = $res['activityOfferta'];
                $ResList[$counter]['activityDescription'] = $res['activityDescription'];
                $ResList[$counter]['activityType'] = $res['activityType'];

                $counter++;
            }
        }
        $stmt->free_result();
        $stmt->close();
        return $ResList;
    }
    public static function addUserDailyActivities($mysqliConn, $activityOfferta, $activityType, $activityDuration, $activityDescription, $year, $month, $day, $user) {
        $insertActivitiesSQL = "INSERT INTO `activities` (`codUser`, `activityDate`,`activityType`, `activityDuration`, `activityOfferta`, `activityDescription`) VALUES (?, ?, ?, ?, ?, ?);";

        $activity_inserted = FALSE;
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($insertActivitiesSQL)) {

            $MKT = mktime(12, 0, 0, intval($month), intval($day), intval($year));
            $activityDate = date('Y-m-d', $MKT);
            $stmt->bind_param("sssdss", $user, $activityDate, $activityType, $activityDuration, $activityOfferta, $activityDescription);
            if ($stmt->execute()) {
                $activity_inserted = TRUE;
            }
        }
        $stmt->free_result();
        $stmt->close();
        return $activity_inserted;
    }
    public static function deleteActivity($mysqliConn, $idAct) {
        $updated = FALSE;
        $disableSQL = "DELETE FROM `activities` WHERE  `idAct`=? ";
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($disableSQL)) {
            $disabled = intval(FALSE);
            $stmt->bind_param("i", $idAct);
            $stmt->execute();
            if ($stmt->affected_rows > 0) {
                $updated = TRUE;
            }
        }
        $stmt->free_result();
        $stmt->close();
        return $updated;
    }
    public static function  editUserDailyActivities($mysqliConn,$activityId, $activityOfferta, $activityType, $activityDuration, $activityDescription, $year, $month, $day, $user) {
        $updateSQL=" UPDATE `activities` SET `codUser`=?, `activityDate`=?, `activityType`=?, `activityDuration`=?, `activityOfferta`=?, `activityDescription`=? WHERE `idAct`=?;";
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($updateSQL)) {
            $MKT = mktime(12, 0, 0, intval($month), intval($day), intval($year));
            $activityDate = date('Y-m-d', $MKT);
            $stmt->bind_param("sssdsss", $user, $activityDate, $activityType, $activityDuration, $activityOfferta, $activityDescription,$activityId);
            if ($stmt->execute()) {
                $activity_updated = TRUE;
            }
         }
        $stmt->free_result();
        $stmt->close();
        return $activity_updated;
    }
    public static function getDescrCust($mysqliConn, $codCust) {

        $countSQL_LIST_CUST_ON_DB = " Select customers.descrCust from customers where customers.codCust=?";
        $descrCust = "";
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($countSQL_LIST_CUST_ON_DB)) {

            $stmt->bind_param("s", $codCust);
            $stmt->bind_result($descrCust);
            $stmt->execute();
            while ($stmt->fetch()) {
                
            }
        } else {
            
        }
        $stmt->free_result();
        $stmt->close();
        return $descrCust;
    }
    public static function getDescrResp($mysqliConn, $codResp) {

        $countSQL_LIST_CUST_ON_DB = " Select responsibles.descrResp from responsibles where responsibles.codResp=?";
        $descrResp = "";
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($countSQL_LIST_CUST_ON_DB)) {

            $stmt->bind_param("s", $codResp);
            $stmt->bind_result($descrResp);
            $stmt->execute();
            while ($stmt->fetch()) {
                
            }
        } else {
            
        }
        $stmt->free_result();
        $stmt->close();
        return $descrResp;
    }
    public static function getDescrUsr($mysqliConn, $codUsr) {

        $countSQL_LIST_CUST_ON_DB = " Select `nome`,`cognome` from `resources` where `codRes`=?";
        $descrUsr = "";
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($countSQL_LIST_CUST_ON_DB)) {

            $stmt->bind_param("s", $codUsr);
            $stmt->bind_result($descrUsrNome,$descrUsrCognome);
            $stmt->execute();
            while ($stmt->fetch()) {
                
            }
        } else {
            
        }
        $stmt->free_result();
        $stmt->close();
        return ($descrUsrNome.' '.$descrUsrCognome);
    }
    public static function listActivitiesRapp($mysqliConn, $coduser,$codOffice,$codCust, $codResp,$YEAR,$MONTH) {
        $select_part = "SELECT activities.activityDate,activities.activityDuration,activities.activityType,activities.activityDescription,activities.activityOfferta"
                . " FROM (users "
                . " JOIN activities ON users.coduser = activities.coduser "
                . " JOIN offers ON activities.activityOfferta=offers.codOffer "
                . " JOIN offices ON offers.codOffice=offices.codOffice "
                . " JOIN customers ON offices.codCust= customers.codCust "
                . " JOIN responsibles ON offers.codResp=responsibles.codResp) " 
                . " WHERE "
                . " users.coduser=? "
                . " AND "
                . " offices.codOffice=? "
                . " AND "
                . " customers.codCust=? "
                . " AND "
                . " responsibles.codResp=? "
                . " AND "
                . " YEAR(activities.activityDate)=? "
                . " AND "
                . " MONTH(activities.activityDate)=? "
                . " ORDER BY activities.activityDate,activities.activityOfferta ";

        $ResList = array();
        $res = array();
        
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($select_part)) {
            
            $stmt->bind_param("ssssii",$coduser,$codOffice,$codCust, $codResp,$YEAR,$MONTH);
            $stmt->bind_result($res['activityDate'], $res['activityDuration'], $res['activityType'], $res['activityDescription'], $res['activityOfferta']);
            $stmt->execute();
            $counter = 0;
            while ($stmt->fetch()) {
               
               
                $ResList[$counter]['activityDate'] = $res['activityDate'];
                $ResList[$counter]['activityDuration'] = $res['activityDuration'];
                $ResList[$counter]['activityType'] = $res['activityType'];
                $ResList[$counter]['activityDescription'] = $res['activityDescription'];// htmlentities(html_entity_decode($res['activityDescription'],ENT_QUOTES))
                $ResList[$counter]['activityOfferta'] = $res['activityOfferta'];
                
                

                $counter++;
            }
        }
        $stmt->free_result();
        $stmt->close();
        return $ResList;
    }
    public static function countActivityType($mysqliConn, $search) {
        $select_part = " SELECT COUNT(*) FROM `activitytypes` ";
        $where_part = "";
        if ($search != "") {

            $where_part = " WHERE ( "
                . composeSearch("codType",$search). " OR "
                . composeSearch("descrType",$search)
                . " ) ";
        }

        $countSQL_LIST_ON_DB = $select_part . " " . $where_part;

        $totElement = 0;
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($countSQL_LIST_ON_DB)) {
            $totElement = 0;
            $stmt->bind_result($totElement);
            $stmt->execute();
            while ($stmt->fetch()) {
                
            }
        } else {
            $totElement = 0;
        }
        $stmt->free_result();
        $stmt->close();
        return $totElement;
    }
    public static function listActivityType($mysqliConn, $limit, $offset, $sort, $order, $search) {
        $select_part = "SELECT `idType`,`codType`,`descrType`,`order` FROM `activitytypes` ";

        $limit_part = "";
        if ($limit > 0) {
            if ($offset > 0) {
                $limit_part = " LIMIT " . $offset . "," . $limit;
            } else {
                $limit_part = " LIMIT " . $limit;
            }
        }

        $where_part = "";
        if ($search != "") {
            $where_part = " WHERE ( "
                . composeSearch("codType",$search). " OR "
                . composeSearch("descrType",$search)
                . " ) ";
        }

        $orderpart_part = "";
        if ($sort === "codType" || $sort === "descrType" || $sort === "order") {
            $orderpart_part = " ORDER BY " . $sort . " ";
            if ($order === "asc" || $order === "desc") {
                $orderpart_part = $orderpart_part . " " . $order . " ";
            }
        }

        $ResList = array();
        $res = array();
        $listSQL_LIST = $select_part . " " . $where_part . " " . $orderpart_part . " " . $limit_part;
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($listSQL_LIST)) {
            $stmt->bind_result($res['idType'], $res['codType'],$res['descrType'], $res['order']);
            $stmt->execute();
            $counter = 0;
            while ($stmt->fetch()) {
                $ResList[$counter]['idType'] = $res['idType'];
                $ResList[$counter]['codType'] = $res['codType'];
                $ResList[$counter]['descrType'] = $res['descrType'];
                $ResList[$counter]['order'] = $res['order'];
               
                $counter++;
            }
        }
        $stmt->free_result();
        $stmt->close();
        return $ResList;
    }
    
    public static function listUsersOnActivity($mysqliConn, $from,$to,$offerTypes) {
        
        $select_part = "SELECT DISTINCT 
                        `activities`.`codUser`,
                        `resources`.`cognome`,
                        `resources`.`nome`
                        FROM 
                        `activities` JOIN `users` ON `activities`.`codUser`= `users`.`codUser`
                        JOIN `offers` ON `activities`.`activityOfferta`=`offers`.`codOffer`
                        JOIN `resources` ON `users`.`codUser`=`resources`.`codRes`
                        ";

        $limit_part = "";
       
        $where_part = "";
        
        $where_part = " WHERE ( "
                        . " `activities`.`activityDate` >= '".$from."' "
                        . " AND "
                        . " `activities`.`activityDate` <= '".$to."' "
                        //. " AND "
                        //. " `activities`.`activityOfferta`='".$offer."'"
                        . " AND "  
                        . " `offers`.`type` IN (".implode(",", $offerTypes).") " 
                        . " ) ";


        $orderpart_part = " ORDER BY "
                . " `resources`.`cognome` , "
                . " `resources`.`nome` ";
        
        $ResList = array();
        $res = array();
        $listSQL_LIST_USR = $select_part . " " . $where_part . " " . $orderpart_part . " " . $limit_part;
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($listSQL_LIST_USR)) {
            //$stmt->bind_param("s", $userCHECK_USR_ON_DB);
            $stmt->bind_result($res['codUser'],$res['cognome'],$res['nome']);
            $stmt->execute();
            $counter = 0;
            while ($stmt->fetch()) {
                
                $ResList[$counter]['nome'] = $res['nome'];
                $ResList[$counter]['cognome'] = $res['cognome'];
                $ResList[$counter]['codUser'] = $res['codUser'];
                

                $counter++;
            }
        }
        $stmt->free_result();
        $stmt->close();
        return $ResList;
    }  
    
    public static function listHolidays($mysqliConn, $from,$to,$offerTypes,$codUsers) {
        
       
        $select_part = " SELECT "
                //. " `activities`.`idAct` "
                //. ", "
                . " `activities`.`codUser` "
                . ", ". " `activities`.`activityDate` "
                //. ", ". " `activities`.`activityType`"
                . ", ". " `activities`.`activityDuration` "
                //. ", ". " `activities`.`activityOfferta` "
                //. ", ". " `activities`.`activityDescription` "
                //. ", ". " `activities`.`activityOffertaOld` "
                . ", ". " `offers`.`type` "
            ." FROM "
            ." `activities` "
            ." JOIN `offers` ON `activities`.`activityOfferta`=`offers`.`codOffer` ";
           
        
        
        $limit_part = "";
       
        
        
        $where_part = " WHERE ( "
                        . " `activities`.`activityDate` >= '".$from."' "
                        . " AND "
                        . " `activities`.`activityDate` <= '".$to."' "
                        . " AND "
                        //. " `activities`.`activityOfferta`='".$offer."'"
                        . " `offers`.`type` IN (".implode(",", $offerTypes).") " 
                        . " AND "          
                        . " `activities`.`codUser` IN (".implode(",", $codUsers).") "
                        . " ) ";


        $orderpart_part = " ORDER BY "
                . " `activities`.`codUser` "
                . " , "
                . " `activities`.`activityDate` "
                ;
        
        $ResList = array();
        $res = array();
        $listSQL_LIST_USR = $select_part . " " . $where_part . " " . $orderpart_part . " " . $limit_part;
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($listSQL_LIST_USR)) {
            //$stmt->bind_param("s", $userCHECK_USR_ON_DB);
            $stmt->bind_result($res['codUser'],$res['activityDate'],$res['activityDuration'],$res['type']);
            $stmt->execute();
            $counter = 0;
            while ($stmt->fetch()) {
                
                $ResList[$counter]['codUser'] = $res['codUser'];
                $ResList[$counter]['activityDate'] = $res['activityDate'];
                $ResList[$counter]['activityDuration'] = $res['activityDuration'];
                $ResList[$counter]['type'] = $res['type'];
                

                $counter++;
            }
        }
        $stmt->free_result();
        $stmt->close();
        return $ResList;
    } 
    
    
    public static function listOffersByTypes($mysqliConn, $offerTypes) {
        $select_part = "SELECT `descrOffer`,`type` FROM offers ";

        $limit_part = "";

       $where_part = " WHERE ( "
                        . " `offers`.`type` IN (".implode(",", $offerTypes).") "
                        . " ) ";

        $orderpart_part = "";
       
        
        $ResList = array();
        $res = array();
        $listSQL_LIST_OFFICES = $select_part . " " . $where_part . " " . $orderpart_part . " " . $limit_part;
        $stmt = $mysqliConn->stmt_init();
        if ($stmt->prepare($listSQL_LIST_OFFICES)) {
            $stmt->bind_result($res['descrOffer'], $res['type']);
            $stmt->execute();
            $counter = 0;
            while ($stmt->fetch()) {
                $ResList[$counter]['descrOffer'] = $res['descrOffer'];
                $ResList[$counter]['type'] = $res['type'];
                $counter++;
            }
        }
       $stmt->free_result();
        $stmt->close();
        return $ResList;
    }
    
    
    
    private static function composeSearch($fieldName,$search){
        return "( `".$fieldName."` LIKE '" . $search . "' OR `".$fieldName."` LIKE '%" . $search . "' OR `".$fieldName."` LIKE '" . $search . "%' OR `".$fieldName."` LIKE '%" . $search . "%' ) " ;
        
    }
}

?>