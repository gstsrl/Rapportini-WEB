<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Description of statusCode
 *
 * @author Riccardo
 */
class statusCode {
    static $actionNotSetted     = "E0001"; 
    static $fieldNotSetted      = "E0002";
    static $loginKO             = "E0003";
    static $loginOK             = "R0001";
    static $userOlreadyExist    = "E0004";
    static $passwordNotMatch    = "E0005";
   
    
    
    static $notInserted         = "E0006";
    static $partiallyInserted   = "E0007";
    
    static $notUpdated         = "E0008";
    static $partiallyUpdated   = "E0009";
    
    
    static $inserted            = "R0002";
    static $updated             = "R0005";
    
    static $olreadyUsed         = "R0003"; 
    static $notUsed             = "R0004";
    
}
?>