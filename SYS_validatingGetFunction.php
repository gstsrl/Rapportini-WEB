<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function is_valid_get_integer( $postVariableName ) {
    $esitAray = array(
        "isSetted" => FALSE,
        "isValid" => FALSE,
        "value" => NULL
    );

    if( filter_has_var(INPUT_GET, $postVariableName)){
        $esitAray["isSetted"]= TRUE;
            //Controllo se valido 
            $tmp_ASDIAJKS =filter_var(filter_input(INPUT_GET, $postVariableName,FILTER_SANITIZE_NUMBER_INT), FILTER_VALIDATE_INT);
            
            if($tmp_ASDIAJKS===FALSE){
                $esitAray["isValid"]=FALSE;
            }else{
                $esitAray["isValid"]=TRUE;
                $esitAray["value"]=$tmp_ASDIAJKS;
            }        
    }else{
        $esitAray["isSetted"]=FALSE;
    }
    return $esitAray;
}
function is_valid_get_string( $postVariableName ) {
    $esitAray = array(
        "isSetted" => FALSE,
        "isValid" => FALSE,
        "value" => NULL
    );

    if( filter_has_var(INPUT_GET, $postVariableName)){
        $esitAray["isSetted"]= TRUE;
            //Controllo se valido 
            $tmp_ASDIAJKS =filter_input(INPUT_GET, $postVariableName,FILTER_SANITIZE_STRING);
            
            if($tmp_ASDIAJKS===FALSE){
                $esitAray["isValid"]=FALSE;
            }else{
                $esitAray["isValid"]=TRUE;
                $esitAray["value"]=$tmp_ASDIAJKS;
            }        
    }else{
        $esitAray["isSetted"]=FALSE;
    }
    return $esitAray;
}
function is_valid_get_boolean( $postVariableName ) {
    $esitAray = array(
        "isSetted" => FALSE,
        "isValid" => FALSE,
        "value" => NULL
    );

    if( filter_has_var(INPUT_GET, $postVariableName)){
        $esitAray["isSetted"]= TRUE;
          
            //Controllo se valido 
            $tmp_ASDIAJKS =filter_input(INPUT_GET, $postVariableName,FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
            
            if($tmp_ASDIAJKS===NULL){
                $esitAray["isValid"]=FALSE;
            }else{
                $esitAray["isValid"]=TRUE;
                $esitAray["value"]=$tmp_ASDIAJKS;
            }        
    }else{
        $esitAray["isSetted"]=FALSE;
    }
    return $esitAray;
}
function is_valid_get_stringArray( $postVariableName ) {
    $esitAray = array(
        "isSetted" => FALSE,
        "isValid" => FALSE,
        "value" => NULL
    );

    if( filter_has_var(INPUT_GET, $postVariableName)){
        $esitAray["isSetted"]= TRUE;
            //Controllo se valido 
            
            
            
            $filter = array(''.$postVariableName => array('name' => $postVariableName,'filter' => FILTER_SANITIZE_STRING,'flags'  => FILTER_REQUIRE_ARRAY));            
            

            
            $tmp_ASDIAJKS = filter_input_array(INPUT_GET, $filter); 
            
            
            
            
            
            if($tmp_ASDIAJKS===FALSE){
                $esitAray["isValid"]=FALSE;
            }else{
                $esitAray["isValid"]=TRUE;
                $esitAray["value"]=$tmp_ASDIAJKS[$postVariableName];
            }        
    }else{
        $esitAray["isSetted"]=FALSE;
    }
    return $esitAray;
}
?>