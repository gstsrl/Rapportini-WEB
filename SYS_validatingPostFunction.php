<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
function is_valid_post_integer( $postVariableName ) {
    $esitAray = array(
        "isSetted" => FALSE,
        "isValid" => FALSE,
        "value" => NULL
    );

    if( filter_has_var(INPUT_POST, $postVariableName)){
        $esitAray["isSetted"]= TRUE;
            //Controllo se valido 
            $tmp_ASDIAJKS =filter_var(filter_input(INPUT_POST, $postVariableName,FILTER_SANITIZE_NUMBER_INT), FILTER_VALIDATE_INT);
            
            if($tmp_ASDIAJKS===FALSE){
                $esitAray["isValid"]=FALSE;
            }else{
                $esitAray["isValid"]=TRUE;
                $esitAray["value"]=$tmp_ASDIAJKS;
            }        
    }else{
        $esitAray["isSetted"]=FALSE;
    }
    return $esitAray;
}
function is_valid_post_string( $postVariableName ) {
    $esitAray = array(
        "isSetted" => FALSE,
        "isValid" => FALSE,
        "value" => NULL
    );

    if( filter_has_var(INPUT_POST, $postVariableName)){
        $esitAray["isSetted"]= TRUE;
            //Controllo se valido 
            $tmp_ASDIAJKS =filter_input(INPUT_POST, $postVariableName,FILTER_SANITIZE_STRING);
            
            if($tmp_ASDIAJKS===FALSE){
                $esitAray["isValid"]=FALSE;
            }else{
                $esitAray["isValid"]=TRUE;
                $esitAray["value"]=$tmp_ASDIAJKS;
            }        
    }else{
        $esitAray["isSetted"]=FALSE;
    }
    return $esitAray;
}
function is_valid_post_boolean( $postVariableName ) {
    $esitAray = array(
        "isSetted" => FALSE,
        "isValid" => FALSE,
        "value" => NULL
    );

    if( filter_has_var(INPUT_POST, $postVariableName)){
        $esitAray["isSetted"]= TRUE;
          
            //Controllo se valido 
            $tmp_ASDIAJKS =filter_input(INPUT_POST, $postVariableName,FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
            
            if($tmp_ASDIAJKS===NULL){
                $esitAray["isValid"]=FALSE;
            }else{
                $esitAray["isValid"]=TRUE;
                $esitAray["value"]=$tmp_ASDIAJKS;
            }        
    }else{
        $esitAray["isSetted"]=FALSE;
    }
    return $esitAray;
}
function is_valid_post_array_string( $postVariableName ) {
    $esitAray = array(
        "isSetted" => FALSE,
        "isValid" => FALSE,
        "value" => NULL
    );

    if( filter_has_var(INPUT_POST, $postVariableName)){
        $esitAray["isSetted"]= TRUE;
            //Controllo se valido 
        $args = array(    
          $postVariableName    => array('filter'    => FILTER_SANITIZE_STRING,
                            'flags'     => FILTER_REQUIRE_ARRAY, 
                            'options'   => array('min_range' => 1, 'max_range' => 10)
                           )
        );
        
        
            $tmp_ASDIAJKS =filter_input_array(INPUT_POST,$args)[$postVariableName];
            
            if($tmp_ASDIAJKS===FALSE){
                $esitAray["isValid"]=FALSE;
            }else{
                $esitAray["isValid"]=TRUE;
                $esitAray["value"]=$tmp_ASDIAJKS;
            }        
    }else{
        $esitAray["isSetted"]=FALSE;
    }
    return $esitAray;
}
?>