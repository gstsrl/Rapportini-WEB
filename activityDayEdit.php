<?php
require_once './SYS_validatingPostFunction.php';
require_once './SYS_loginClass.php';
require_once './SYS_paramClass.php';
require_once './SYS_mysqliConnClass.php';
require_once './SYS_queryClass.php';
require_once './BE_feste.php';

$parametri = new Params();
$mysqlConn = new mysqliConnClass($parametri);
$mysqliConn = $mysqlConn->connect();
$authCHECK_LOGIN = loginClass::checkUser2($mysqliConn);
if ($authCHECK_LOGIN) {

    $usernameCHECK_ROLES = loginClass::getUidUser();
    $res = queryClass::checkUserRoleOnDb($mysqliConn, $usernameCHECK_ROLES);
    $mysqlConn->disconnect();
    
    $month_POST = is_valid_post_integer('month');
    $month = date('n');
    if ($month_POST['isSetted'] && $month_POST['isValid']) {
        $month = $month_POST['value'];
    }

    $year_POST = is_valid_post_integer('year');
    $year = date('Y');
    if ($year_POST['isSetted'] && $year_POST['isValid']) {
        $year = $year_POST['value'];
    }

    $day_POST = is_valid_post_integer('day');
    $day = date('j');
    if ($day_POST['isSetted'] && $day_POST['isValid']) {
        $day = $day_POST['value'];
    }

    $user_POST = is_valid_post_string('user');
    $user = loginClass::getUidUser();
    if ($user_POST['isSetted'] && $user_POST['isValid']) {
        $user = $user_POST['value'];
    }
    
    
    $idAct_POST = is_valid_post_string('idAct');
    $activityId="";
    if ($idAct_POST['isSetted'] && $idAct_POST['isValid']) {
        $activityId = $idAct_POST['value'];
    }
    
    
    $setday=mktime(0, 0, 0, $month  , $day, $year);
    $tomorrow=mktime(0, 0, 0, $month  , date("d",$setday)+1, $year);
    $yesterday=mktime(0, 0, 0, $month  , date("d",$setday)-1, $year);
    
    $tD=date ("j", $tomorrow);
    $tM=date ("n", $tomorrow);
    $tY=date ("Y", $tomorrow);
    
    $yD=date ("j", $yesterday);        
    $yM=date("n", $yesterday);
    $yY=date("Y", $yesterday);
    
    
    
    $activityDescription_POST   = is_valid_post_string('activityDescription');
    $activityDuration_POST       = is_valid_post_string('activityDuration');
    $activityType_POST          = is_valid_post_string('activityType');
    $activityOfferta_POST       = is_valid_post_string('activityOfferta');
    

    
    $feste = new Feste ();
    
    $isFesta=$feste->isFestaYMd((int)$year, (int)$month, (int)$day);
    ?>
    <!DOCTYPE html>
    <!--[if IE 8]><html class="no-js lt-ie9" lang="en" ><![endif]-->
    <!--[if gt IE 8]><!--><html class="no-js" ><!--<![endif]-->
        <html>
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">

                <title>Edit Activity</title>
                <!-- Fogli di stile -->
                <link href="css/bootstrap.css" rel="stylesheet" media="screen">
                <link href="css/bootstrap-table.min.css" rel="stylesheet" media="screen">
                <link href="css/stili-custom.css" rel="stylesheet" media="screen">
                <!-- Modernizr -->
                <script src="js/modernizr.custom.js"></script>
                <!-- respond.js per IE8 -->
                <!--[if lt IE 9]>
                <script src="js/respond.min.js"></script>
                <![endif]-->
            </head>
            <body>
                <nav class="navbar navbar-default navbar-static-top">
                    <div class="container">
                        <ol class="breadcrumb">
                           <li><a href="menu.php"> Menu</a></li>
                           <li><a href='activtyMonthView.php' id='post-link' year="<?php print $year;?>" month="<?php print $month;?>" day="<?php print $day;?>" >Attività mensili</a></li>
                           <li><a href='activityDayView.php' id='post-link' year="<?php print $year;?>" month="<?php print $month;?>" day="<?php print $day;?>" >Attività giornaliere</a></li>
                           <li class="active">Edit Activity</li>
                        </ol>
                    </div>
                </nav>
                <!--<div class="wrapper ">-->
                <div class="container">

                    <div class="row vertical-center-row">   


                        <?php
                        if ($res['isEnabledCHECK_ROLES'] ) {
                            ?>         


                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="panel panel-default">    
                                    
                                    
                                    <div class="panel-heading text-center">
                                        <div class="row">
                                            <div class="col-md-3 col-xs-4">
                                                <!--<a 
                                                    class=" ajax-navigation btn btn-default btn-sm" 
                                                    href='activityDayAdd.php' 
                                                    id='post-link' 
                                                    year="<?php print $yY;?>" 
                                                    month="<?php print $yM;?>" 
                                                    day="<?php print $yD;?>" 
                                                    user="<?php print $user;?>">
                                                    <span class="glyphicon glyphicon-arrow-left"></span>
                                                </a>
                                                <a 
                                                    class=" ajax-navigation btn btn-default btn-sm" 
                                                    href='activityDayAdd.php' 
                                                    id='post-link' 
                                                    user="<?php print $user;?>" >
                                                    <span class="glyphicon glyphicon-arrow-down">
                                                </a>!-->
                                                <a 
                                                    class=" ajax-navigation btn btn-default btn-sm"
                                                    href='activityDayAdd.php' 
                                                    id='post-form-link' 
                                                     year="<?php print $yY;?>" 
                                                    month="<?php print $yM;?>" 
                                                    day="<?php print $yD;?>" 
                                                    user="<?php print $user;?>" >
                                                    <span class="glyphicon glyphicon-arrow-left">&nbsp;<span class="glyphicon glyphicon glyphicon-floppy-disk"></span>
                                                </a>
                                                
                                            </div>
                                            <div class="col-md-6 col-xs-4"><strong>Edit Activity <?php print $day; ?>-<?php print $month; ?>-<?php print $year; ?></strong></div>
                                            <div class="col-md-3 col-xs-4">
                                                
                                                <a class=" ajax-navigation btn btn-default btn-sm" 
                                                   href='activityDayAdd.php' 
                                                   id='post-form-link' 
                                                   year="<?php print $tY;?>" 
                                                    month="<?php print $tM;?>" 
                                                    day="<?php print $tD;?>"
                                                   user="<?php print $user;?>">
                                                    <span class="glyphicon glyphicon glyphicon-floppy-disk">&nbsp;<span class="glyphicon glyphicon-arrow-right"></span>
                                                </a>
                                                <!--<a 
                                                    class=" ajax-navigation btn btn-default btn-sm" 
                                                    href='activityDayAdd.php' 
                                                    id='post-link' 
                                                    user="<?php print $user;?>" >
                                                    <span class="glyphicon glyphicon-arrow-down">
                                                </a>
                                                <a 
                                                    class=" ajax-navigation btn btn-default btn-sm" 
                                                    href='activityDayAdd.php' 
                                                    id='post-link' 
                                                    year="<?php print $tY;?>" 
                                                    month="<?php print $tM;?>" 
                                                    day="<?php print $tD;?>"
                                                    user="<?php print $user;?>" >
                                                    <span class="glyphicon glyphicon-arrow-right">
                                                </a>-->
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    if($isFesta){
                                    ?>    
                                            <div style='background: #1203;' class="panel-body">
                                    <?php
                                    }else{
                                    ?>  
                                       <div class="panel-body">
                                    <?php
                                    }
                                    ?>
                                        <form id="editActivityForm" accept-charset="UTF-8" role="form" class="form-horizontal">
                                            <fieldset>
                                                <!-- Form Name -->
                                                <!--<legend>Form Name</legend>-->
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" for="activityOfferta">activityOfferta</label>  
                                                    <div class="col-md-4">
                                                        <select 
                                                            id="activityOfferta" 
                                                            name="activityOfferta" 
                                                            type="text" 
                                                            placeholder="activityOfferta" 
                                                            class="form-control input-md" 
                                                            required=""
                                                            <?php  
                                                            if ($activityOfferta_POST['isSetted'] && $activityOfferta_POST['isValid']) { 
                                                                echo 'value="', $activityOfferta_POST['value'], '"', ' ';
                                                            }
                                                            ?>
                                                        >
                                                        </select>
                                                        <span class="help-block">Insert activityOfferta</span>  
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" for="activityType">activityType</label>  
                                                    <div class="col-md-4">
                                                        <select 
                                                            id="activityType" 
                                                            name="activityType" 
                                                            type="text" 
                                                            placeholder="activityType" 
                                                            class="form-control input-md" 
                                                            required=""
                                                            <?php  
                                                            if ($activityType_POST['isSetted'] && $activityType_POST['isValid']) { 
                                                                echo 'value="', $activityType_POST['value'], '"', ' ';
                                                            }
                                                            ?>
                                                        >
                                                           
                                                        </select>
                                                        <span class="help-block">Insert activityType</span>  
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" for="activityDuration">activityDuration</label>  
                                                    <div class="col-md-4">
                                                        <input 
                                                            id="activityDuration" 
                                                            name="activityDuration" 
                                                            type="text" 
                                                            placeholder="activityDuration" 
                                                            class="form-control input-md" 
                                                            required=""
                                                            <?php  
                                                            if ($activityDuration_POST['isSetted'] && $activityDuration_POST['isValid']) { 
                                                                echo 'value="', $activityDuration_POST['value'], '"', ' ';
                                                            }
                                                            ?> 
                                                        >
                                                        <span class="help-block">Insert activityDuration</span>  
                                                       
                                                    </div>
                                                </div>

                                                <!-- Text input-->
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" for="activityDescription">activityDescription</label>
                                                    <div class="col-md-4">
                                                        <input 
                                                            id="activityDescription" 
                                                            name="activityDescription" 
                                                            type="text" 
                                                            placeholder="activityDescription" 
                                                            class="form-control input-md" 
                                                            required="" 
                                                            <?php  
                                                            if ($activityDescription_POST['isSetted'] && $activityDescription_POST['isValid']) { 
                                                                echo 'value="', $activityDescription_POST['value'], '"', ' ';
                                                            }
                                                            ?>
                                                        >
                                                        <span class="help-block">activityDescription</span>
                                                    </div>
                                                </div>
                                                
                                                
                                                <input id="activityDay"     name="activityDay"      type="hidden"   value="<?php print $day;?>">
                                                <input id="activityMonth"   name="activityMonth"    type="hidden"   value="<?php print $month;?>">
                                                <input id="activityYear"    name="activityYear"     type="hidden"   value="<?php print $year;?>">
                                                <input id="activityUser"    name="activityUser"     type="hidden"   value="<?php print $user;?>">
                                                <input id="activityId"      name="activityId"       type="hidden"   value="<?php print $activityId;?>">
                                                

                                                <!-- Button (Double) -->
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" for="Submit"></label>
                                                    <div class="col-md-8">
                                                        <button id="Submit" type="submit"  name="Submit" value="Submit" class="btn btn-primary">Submit</button>
                                                        <button id="Clear"  type="reset"   name="Reset" value="Reset" class="btn btn-default">Reset</button>
                                                    </div>
                                                </div>

                                            </fieldset>
                                        </form>


                                    </div>
                                </div>

                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12" >
                                <div id="le-alert" class="alert alert-warning alert-block fade" role="alert">
                                    <button type="button" class="close">&times;</button>
                                    <h4 id="warnTtl" ></h4>
                                    <p id="warnMsg" ></p>
                                    <p id="warnMsgCause" ></p>
                                    <p id="warnMsgCod" ></p>
                                </div>
                            </div>
                            <?php
                        } else {
                            ?>                    
                            <p>Utente non abilitato</p>    
                            <?php
                        }
                        ?>                
                    </div>
                </div>
                <!-- jQuery e plugin JavaScript  -->
                <script src="js/jquery-2.1.3.min.js"></script>
                <script src="js/i18next-1.7.7.min.js"></script>
                <script src="js/bootstrap.min.js"></script>
                <script src="js/bootstrap-table.min.js"></script>
                <script src="js/jsscript/SYS_boot_Alert.js"></script>
                <script src="js/translation/activityDayEditT.js"></script>
                <script src="js/jsscript/activityDayEdit.js"></script>
            </body>
        </html>         
        <?php
    } else {
        $mysqlConn->disconnect();
        header("Location: logout.php");
    }
    ?>
