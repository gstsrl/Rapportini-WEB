<?php
require_once './SYS_validatingPostFunction.php';
require_once './SYS_loginClass.php';
require_once './SYS_paramClass.php';
require_once './SYS_mysqliConnClass.php';
require_once './SYS_queryClass.php';
require_once './BE_feste.php';
//






$parametri = new Params();
$mysqlConn = new mysqliConnClass($parametri);
$mysqliConn = $mysqlConn->connect();
$authCHECK_LOGIN = loginClass::checkUser2($mysqliConn);
if ($authCHECK_LOGIN) {

    $usernameCHECK_ROLES = loginClass::getUidUser();
    $res = queryClass::checkUserRoleOnDb($mysqliConn, $usernameCHECK_ROLES);
    $mysqlConn->disconnect();





    $month_POST = is_valid_post_integer('month');
    $month = date('n');
    if ($month_POST['isSetted'] && $month_POST['isValid']) {
        $month = $month_POST['value'];
    }

    $year_POST = is_valid_post_integer('year');
    $year = date('Y');
    if ($year_POST['isSetted'] && $year_POST['isValid']) {
        $year = $year_POST['value'];
    }

    $day_POST = is_valid_post_integer('day');
    $day = date('j');
    if ($day_POST['isSetted'] && $day_POST['isValid']) {
        $day = $day_POST['value'];
    }

    $user_POST = is_valid_post_string('user');
    $user = loginClass::getUidUser();
    if ($user_POST['isSetted'] && $user_POST['isValid']) {
        $user = $user_POST['value'];
    }
    
    $setday=mktime(0, 0, 0, $month  , $day, $year);
    $tomorrow=mktime(0, 0, 0, $month  , date("d",$setday)+1, $year);
    $yesterday=mktime(0, 0, 0, $month  , date("d",$setday)-1, $year);
    
    $tD=date ("j", $tomorrow);
    $tM=date ("n", $tomorrow);
    $tY=date ("Y", $tomorrow);
    
    $yD=date ("j", $yesterday);        
    $yM=date("n", $yesterday);
    $yY=date("Y", $yesterday);
    
    $feste = new Feste ();
    
    $isFesta=$feste->isFestaYMd((int)$year, (int)$month, (int)$day);
    
    
    ?>
    <!DOCTYPE html>
    <!--[if IE 8]><html class="no-js lt-ie9" lang="en" ><![endif]-->
    <!--[if gt IE 8]><!--><html class="no-js" ><!--<![endif]-->
        <html>
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">

                <title>Attività giornaliere</title>
                <!-- Fogli di stile -->
                <link href="css/bootstrap.css" rel="stylesheet" media="screen">
                <link href="css/bootstrap-table.min.css" rel="stylesheet" media="screen">
                <link href="css/stili-custom.css" rel="stylesheet" media="screen">
                <!-- Modernizr -->
                <script src="js/modernizr.custom.js"></script>
                <!-- respond.js per IE8 -->
                <!--[if lt IE 9]>
                <script src="js/respond.min.js"></script>
                <![endif]-->
                <script>
                    function queryParams(params) {

                        var data = {
                            month: <?php print $month; ?>,
                            year: <?php print $year; ?>,
                            day: <?php print $day; ?>,
                            user: '<?php print $user; ?>'
                        };
                        //return params;
                        return $.extend(params, data);
                    }
                </script>

            </head>
            <body>
                <nav class="navbar navbar-default navbar-static-top">
                    <div class="container">
                        <ol class="breadcrumb">
                            <li><a href="menu.php">Menu</a></li>
                            <li><a a href='activtyMonthView.php' id='post-link' year="<?php print $year;?>" month="<?php print $month;?>" day="<?php print $day;?>" >Attività mensili</a></li>
                            <li class="active">Attività giornaliere</li>
                        </ol>
                    </div>
                </nav>
                <!--<div class="wrapper ">-->
                <div class="container">
                    <div class="row vertical-center-row">   


                        <?php
                        if ($res['isEnabledCHECK_ROLES'] && $res['isUserCHECK_ROLES']) {
                            ?>         

                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="panel panel-default">
                                    <!--<div class="panel-heading">Activity List <?php print $day; ?>-<?php print $month; ?>-<?php print $year; ?></div>-->
                                    
                                    <div class="panel-heading text-center">
                                        <div class="row">
                                            <div class="col-md-3 col-xs-4">
                                                <a 
                                                    class=" ajax-navigation btn btn-default btn-sm" 
                                                    href='activityDayView.php' 
                                                    id='post-link' 
                                                    year="<?php print $yY;?>" 
                                                    month="<?php print $yM;?>" 
                                                    day="<?php print $yD;?>" 
                                                    user="<?php print $user;?>">
                                                    <span class="glyphicon glyphicon-arrow-left"></span>
                                                </a>
                                                <a 
                                                    class=" ajax-navigation btn btn-default btn-sm" 
                                                    href='activityDayView.php' 
                                                    id='post-link' 
                                                    user="<?php print $user;?>" >
                                                    <span class="glyphicon glyphicon-arrow-down">
                                                </a>
                                               
                                                
                                            </div>
                                            <div class="col-md-6 col-xs-4"><strong>Activity List <?php print $day; ?>-<?php print $month; ?>-<?php print $year; ?></strong></div>
                                            <div class="col-md-3 col-xs-4">
                                                
                                                
                                                <a 
                                                    class=" ajax-navigation btn btn-default btn-sm" 
                                                    href='activityDayView.php' 
                                                    id='post-link' 
                                                    user="<?php print $user;?>" >
                                                    <span class="glyphicon glyphicon-arrow-down">
                                                </a>
                                                <a 
                                                    class=" ajax-navigation btn btn-default btn-sm" 
                                                    href='activityDayView.php' 
                                                    id='post-link' 
                                                    year="<?php print $tY;?>" 
                                                    month="<?php print $tM;?>" 
                                                    day="<?php print $tD;?>"
                                                    user="<?php print $user;?>" >
                                                    <span class="glyphicon glyphicon-arrow-right">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <?php
                                    if($isFesta){
                                    ?>    
                                            <div style='background: #1203;' class="panel-body">
                                    <?php
                                    }else{
                                    ?>  
                                       <div class="panel-body">
                                    <?php
                                    }
                                    ?> 
                                        <div id="toolbar" class="btn-group">

                                            <a class="btn btn-default" href="activityDayAdd.php" id='post-link' year="<?php print $year;?>" month="<?php print $month;?>" day="<?php print $day;?>" ><i class="glyphicon glyphicon-plus"></i>Add Activity</a>

                                            <!--<button type="button" class="btn btn-default">
                                                <i class="glyphicon glyphicon-trash"></i>
                                            </button>-->
                                        </div>
                                        <!--                                               data-query-params="queryParams" -->
                                        <table id="tableId"
                                               data-toggle="table"
                                               data-url="./BE_activityListDaily.php"
                                               data-method="post"
                                               data-content-type="application/x-www-form-urlencoded"
                                               data-query-params="queryParams" 
                                               data-query-params-type="limit" 
                                               data-pagination="true"
                                               data-side-pagination="server"
                                               data-page-size="4"
                                               data-page-list="[2,4,5, 10, 20, 50, 100, 200]"
                                               data-search="true"
                                               data-show-refresh="true"
                                               data-show-toggle="true"
                                               data-show-columns="true"
                                               data-height="300"
                                               data-toolbar="#toolbar">
                                            <thead>
                                                <tr>
                                                   <!-- <th data-field="state"  data-checkbox="true"></th>-->
                                                    <th data-field="activityDuration"    data-align="center" data-sortable="true">activityDuration</th>
                                                    <th data-field="activityType"  data-align="center" data-sortable="true">activityType</th>
                                                    <th data-field="activityDescription"  data-align="left" data-sortable="true">activityDescription</th>
                                                    <th data-field="activityOfferta"  data-align="center" data-sortable="true">activityOfferta</th>
                                                    <!--<th data-field="isEnabled"  data-sortable="false">isEnabled</th>-->
                                                    <th data-field="action"     data-align="center" data-formatter="actionFormatter" data-events="actionEvents">Action</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <?php
                        } else {
                            ?>                    
                            <p>Utente non abilitato</p>    
                            <?php
                        }
                        ?>                
                    </div>
                </div>
                
                <!-- jQuery e plugin JavaScript  -->
                <script src="js/jquery-2.1.3.min.js"></script>
                <script src="js/i18next-1.7.7.min.js"></script>
                <script src="js/bootstrap.min.js"></script>
                <script src="js/bootstrap-table.min.js"></script>

                <script src="js/translation/activityManageT.js"></script>
                <script src="js/jsscript/activityManage.js"></script>
            </body>
        </html>         
        <?php
    } else {
        $mysqlConn->disconnect();
        header("Location: logout.php");
    }
    ?>