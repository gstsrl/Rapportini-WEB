<?php
require_once './SYS_validatingPostFunction.php';
require_once './SYS_loginClass.php';
require_once './SYS_paramClass.php';
require_once './SYS_mysqliConnClass.php';
require_once './SYS_queryClass.php';

$parametri = new Params();
$mysqlConn = new mysqliConnClass($parametri);
$mysqliConn = $mysqlConn->connect();
$authCHECK_LOGIN = loginClass::checkUser2($mysqliConn);
if ($authCHECK_LOGIN) {

    $usernameCHECK_ROLES = loginClass::getUidUser();
    $res = queryClass::checkUserRoleOnDb($mysqliConn, $usernameCHECK_ROLES);
    $mysqlConn->disconnect();
    
    
    
    $month_POST = is_valid_post_integer('month');
    $month = date('n');
    if ($month_POST['isSetted'] && $month_POST['isValid']) {
        $month = $month_POST['value'];
    }

    $year_POST = is_valid_post_integer('year');
    $year = date('Y');
    if ($year_POST['isSetted'] && $year_POST['isValid']) {
        $year = $year_POST['value'];
    }

    $day_POST = is_valid_post_integer('day');
    $day = date('j');
    if ($day_POST['isSetted'] && $day_POST['isValid']) {
        $day = $day_POST['value'];
    }

    $user_POST = is_valid_post_string('user');
    $user = loginClass::getUidUser();
    if ($user_POST['isSetted'] && $user_POST['isValid']) {
        $user = $user_POST['value'];
    }
    
    
    ?>
    <!DOCTYPE html>
    <!--[if IE 8]><html class="no-js lt-ie9" lang="en" ><![endif]-->
    <!--[if gt IE 8]><!--><html class="no-js" ><!--<![endif]-->
        <html>
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">

                <title>Attività Mensili</title>
                <!-- Fogli di stile -->
                <link href="css/bootstrap.css" rel="stylesheet" media="screen">
                <link href="css/bootstrap-table.min.css" rel="stylesheet" media="screen">
                
                <link href="css/stili-custom.css" rel="stylesheet" media="screen">
                <!-- Modernizr -->
                <script src="js/modernizr.custom.js"></script>
                <!-- respond.js per IE8 -->
                <!--[if lt IE 9]>
                <script src="js/respond.min.js"></script>
                <![endif]-->
                <script>
                    function queryParams(params) {

                        var data = {
                            month: <?php print $month; ?>,
                            year: <?php print $year; ?>,
                            day: <?php print $day; ?>,
                            user: '<?php print $user; ?>'
                        };
                        //return params;
                        return $.extend(params, data);
                    }
                </script>
                
            </head>
            <body>
                <nav class="navbar navbar-default navbar-static-top">
                    <div class="container">
                        <ol class="breadcrumb">
                            <li><a href="menu.php"> Menu</a></li>
                            <li class="active">Atttività mensili</li>
                        </ol>
                    </div>
                </nav>
                <!--<div class="wrapper ">-->
                <div class="container">

                    <div class="row vertical-center-row">   


                        <?php
                        if ($res['isEnabledCHECK_ROLES'] && $res['isUserCHECK_ROLES']) {
                            ?>         

                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel panel-default calendar">

                                    </div>
                                </div>
                            </div>

                            <?php
                        } else {
                            ?>                    
                            <p>Utente non abilitato</p>    
                            <?php
                        }
                        ?>                
                    </div>
                </div>
                <!-- jQuery e plugin JavaScript  -->
                <script src="js/jquery-2.1.3.min.js"></script>
                <script src="js/i18next-1.7.7.min.js"></script>
                <script src="js/bootstrap.min.js"></script>
                <script src="js/bootstrap-table.min.js"></script>
                
                <script src="js/translation/monthViewT.js"></script>
                <script src="js/jsscript/monthView.js"></script>
            </body>
        </html>         
        <?php
    } else {
        $mysqlConn->disconnect();
        header("Location: logout.php");
    }
    ?>






