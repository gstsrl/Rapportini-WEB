<?php
    //Connects to your Database 
    require_once ('./DB_CONNECT.INC.PHP');
    require_once ('./CHECK_LOGIN.INC.PHP');
    if ($authCHECK_LOGIN) {
        $usernameCHECK_ROLES = $username;
        require_once ('./CHECK_ROLES.INC.PHP');
        if ($isEnabledCHECK_ROLES && $isUserCHECK_ROLES) {
            $addOperation="add";
            //$addOperation0="add0";
            //$addOperation1="add1";
            $delOperation="del";
            $disableOperation="dis";
            $editOperation="edit";
            $enableOperation="enable";
            $selectResponsible=FALSE;
            require_once ('./validatingPostFunction.php');
?>
<?php
            //Recupero l'elenco di customers
            $customersListSQL = "SELECT `idCust`, `codCust`, `descrCust`, `isEnabled` FROM `customers` ";
            $stmt_customersList = mysqli_stmt_init($link)or die(mysqli_error($link));
            mysqli_stmt_prepare($stmt_customersList, $customersListSQL)or die(mysqli_error($link));
            //mysqli_stmt_bind_param($stmt_activitiesDetail, "siii", $username,$y,$m,$d)or die(mysqli_error($link));
            mysqli_stmt_bind_result($stmt_customersList, $idCust,$codCust,$descrCust,$isEnabled)or die(mysqli_error($link));
            mysqli_stmt_execute($stmt_customersList)or die(mysqli_error($link));
            
            $custListArray= [];
            while (mysqli_stmt_fetch($stmt_customersList)) {
                $custListArray[$codCust] = [];
                $custListArray[$codCust]["desc"]=$descrCust;
                $custListArray[$codCust]["enabled"]=$isEnabled;
                //echo("    <option value='$codCust'>$descrCust</option>");
            }
            
            
            //Recupero l'elenco di responsibles
            $responsiblesListSQL = "SELECT `idResp`, `codResp`, `descrResp`, `isEnabled`, `codOffice` FROM `responsibles` ";
            $stmt_responsiblesList = mysqli_stmt_init($link)or die(mysqli_error($link));
            mysqli_stmt_prepare($stmt_responsiblesList, $responsiblesListSQL)or die(mysqli_error($link));
            //mysqli_stmt_bind_param($stmt_activitiesDetail, "siii", $username,$y,$m,$d)or die(mysqli_error($link));
            mysqli_stmt_bind_result($stmt_responsiblesList, $id,$codResp,$descrResp,$isEnabled,$codOffice)or die(mysqli_error($link));
            mysqli_stmt_execute($stmt_responsiblesList)or die(mysqli_error($link));
            
            $respListArray= [];
            while (mysqli_stmt_fetch($stmt_responsiblesList)) {
                $respListArray[$codResp] = [];
                $respListArray[$codResp]["desc"]=$descrResp;
                $respListArray[$codResp]["enabled"]=$isEnabled;
                $respListArray[$codResp]["codOffice"]=$codOffice;
            }
            
            //Recupero l'elenco di offices
            $officesListSQL = "SELECT `idOffice`, `codOffice`, `descrOffice`, `isEnabled`, `codCust` FROM `offices` ";
            $stmt_officesList = mysqli_stmt_init($link)or die(mysqli_error($link));
            mysqli_stmt_prepare($stmt_officesList, $officesListSQL)or die(mysqli_error($link));
            //mysqli_stmt_bind_param($stmt_activitiesDetail, "siii", $username,$y,$m,$d)or die(mysqli_error($link));
            mysqli_stmt_bind_result($stmt_officesList, $id,$codOffice,$descrOffice,$isEnabled,$codCust)or die(mysqli_error($link));
            mysqli_stmt_execute($stmt_officesList)or die(mysqli_error($link));
            $officeListArray=[];
            while (mysqli_stmt_fetch($stmt_officesList)) {
                $officeListArray[$codOffice] = [];
                $officeListArray[$codOffice]["desc"]=$descrOffice;
                $officeListArray[$codOffice]["enabled"]=$isEnabled;
                $officeListArray[$codOffice]["codCust"]=$codCust;
            }
?>

<?php 
            $operation_POST=is_valid_post_string('operation'); 
            if($operation_POST['isSetted'] && $operation_POST['isValid']){
                
                
                if($operation_POST['value'] ===$addOperation){
                    
                    
                    //Check post values
                    $codOffer_POST   = is_valid_post_string('codOffer');
                    $codOffice_POST   = is_valid_post_string('codOffice');
                    $codResp_POST   = is_valid_post_string('codResp');
                    $hour_POST      = is_valid_post_integer('hour');
                    
                    $insertable=TRUE;
                    if(!($codOffer_POST['isSetted'] && $codOffer_POST['isValid'])){
                        echo 'Error setting Offert code';
                        $insertable=FALSE;
                    }
                    if(!($codOffice_POST['isSetted'] && $codOffice_POST['isValid'])){
                        echo 'Error setting customer code';
                        $insertable=FALSE;
                    }
                    
?>
<?php
                    //If not posted 
                    if(!($codResp_POST['isSetted'])){
                        //I check if there is only one resp for the office if one autoselect and inserted  
                        $respOfOfficeSQL = "SELECT `idResp`, `codResp`, `descrResp`, `isEnabled`, `codOffice` FROM `responsibles` WHERE `codOffice`= ?";
                        $stmt_respOfOffice = mysqli_stmt_init($link)or die(mysqli_error($link));
                        mysqli_stmt_prepare     ($stmt_respOfOffice, $respOfOfficeSQL)or die(mysqli_error($link));
                        mysqli_stmt_bind_param  ($stmt_respOfOffice, "s", $codOffice_POST['value'])or die(mysqli_error($link));
                        mysqli_stmt_bind_result ($stmt_respOfOffice, $id,$codResp,$descrResp,$isEnabled,$codOffice)or die(mysqli_error($link));
                        mysqli_stmt_execute     ($stmt_respOfOffice)or die(mysqli_error($link));
                        $respOfOfficeList=[];
                        $pos=0;
                        $count=0;
                        while (mysqli_stmt_fetch($stmt_respOfOffice)) { 
                            $pos++;
                            $respOfOfficeList[$pos]=[]; 
                            $respOfOfficeList[$pos]["id"]=$id;
                            $respOfOfficeList[$pos]["codResp"]=$codResp;
                            $respOfOfficeList[$pos]["descrResp"]=$descrResp;
                            $respOfOfficeList[$pos]["isEnabled"]=$isEnabled;
                            $respOfOfficeList[$pos]["codOffice"]=$codOffice;
                            if($isEnabled){
                                $count++;
                            }
                            
                        }
                        
                        if($count==1){
                            $insertable=TRUE; 
                            $selectResponsible=FALSE;
                            $codResp_POST['value']=$respOfOfficeList[1]["codResp"];
                        }else if($count>1){
                            $insertable=FALSE;
                            $selectResponsible=TRUE;
                        }else{
                           $insertable=FALSE; 
                           $selectResponsible=FALSE;
                        } 
                    }else{
                        if(!($codResp_POST['isSetted'] && $codResp_POST['isValid'])){
                            echo 'Error setting responsible description';
                            $insertable=FALSE;
                        }
                    }
?>
<?php
                    if(!($hour_POST['isSetted'] && $hour_POST['isValid'])){
                        echo 'Error setting hour value';
                        $insertable=FALSE;
                    } 
?>
<?php
                    //Test if codes exists
                    
                    //T2
                    $offerCheckSQL = "SELECT count(`codOffer`) FROM `offers` WHERE `codOffer`= ?";
                    $stmt_offerCheck = mysqli_stmt_init($link)or die(mysqli_error($link));
                    mysqli_stmt_prepare     ($stmt_offerCheck, $offerCheckSQL)or die(mysqli_error($link));
                    mysqli_stmt_bind_param  ($stmt_offerCheck, "s", $codOffer_POST['value'])or die(mysqli_error($link));
                    mysqli_stmt_bind_result ($stmt_offerCheck, $check2)or die(mysqli_error($link));
                    mysqli_stmt_execute     ($stmt_offerCheck)or die(mysqli_error($link));
                    mysqli_stmt_fetch       ($stmt_offerCheck);
                    mysqli_stmt_close       ($stmt_offerCheck)or die(mysqli_error($link));
                    if ($check2 != 0) {
                        $insertable=FALSE;
                        die('Sorry, the offer code ' . $codOffer_POST['value'] . ' is already in use.');
                    }
                    //T3
                    $customerCheckSQL = "SELECT count(`codOffice`) FROM `offices` WHERE `codOffice`= ?";
                    $stmt_customerCheck = mysqli_stmt_init($link)or die(mysqli_error($link));
                    mysqli_stmt_prepare     ($stmt_customerCheck, $customerCheckSQL)or die(mysqli_error($link));
                    mysqli_stmt_bind_param  ($stmt_customerCheck, "s", $codOffice_POST['value'])or die(mysqli_error($link));
                    mysqli_stmt_bind_result ($stmt_customerCheck, $check3)or die(mysqli_error($link));
                    mysqli_stmt_execute     ($stmt_customerCheck)or die(mysqli_error($link));
                    mysqli_stmt_fetch       ($stmt_customerCheck);
                    mysqli_stmt_close       ($stmt_customerCheck)or die(mysqli_error($link));
                    if ($check3 == 0) {
                         $insertable=FALSE;
                        die('Sorry, the office code ' . $codOffice_POST['value'] . ' not exist.');
                    }
                    //T4
                    if(!$selectResponsible){
                        $responsibleCheckSQL = "SELECT count(`codResp`) FROM `responsibles` WHERE `codResp`= ?";
                        $stmt_responsibleCheck = mysqli_stmt_init($link)or die(mysqli_error($link));
                        mysqli_stmt_prepare     ($stmt_responsibleCheck, $responsibleCheckSQL)or die(mysqli_error($link));
                        mysqli_stmt_bind_param  ($stmt_responsibleCheck, "s", $codResp_POST['value'])or die(mysqli_error($link));
                        mysqli_stmt_bind_result ($stmt_responsibleCheck, $check4)or die(mysqli_error($link));
                        mysqli_stmt_execute     ($stmt_responsibleCheck)or die(mysqli_error($link));
                        mysqli_stmt_fetch       ($stmt_responsibleCheck);
                        mysqli_stmt_close       ($stmt_responsibleCheck)or die(mysqli_error($link));
                        if ($check4 == 0) {
                             $insertable=FALSE;
                            die('Sorry, the responsible code ' . $codResp_POST['value'] . ' not exist.');
                        }
                    }
                    
                   
                    if( $insertable){
                        $newOfferIsEnabled =  TRUE;
                        $newOfferIsEnabledI = intval($newOfferIsEnabled);
                    
                        $responsibleInsertSQL="INSERT INTO `offers`"
                                . "(`codOffer`, `codOffice`, `codResp`, `hour`, `isEnabled`)"
                                . "VALUES (?,?,?,?,?)";
                        $stmt_responsibleInsert = mysqli_stmt_init($link)or die(mysqli_error($link));
                        mysqli_stmt_prepare($stmt_responsibleInsert, $responsibleInsertSQL)or die(mysqli_error($link));
                        mysqli_stmt_bind_param($stmt_responsibleInsert,
                                "sssis",
                                $codOffer_POST['value'],
                                $codOffice_POST['value'],
                                $codResp_POST['value'],
                                $hour_POST['value'],
                                $newOfferIsEnabledI
                                )or die(mysqli_error($link));
                       
                        mysqli_stmt_execute($stmt_responsibleInsert)or die(mysqli_error($link));
                        echo "Aggiunta riga";
                    }
                    
                    
                    
                }else if($operation_POST['value'] ===$disableOperation){
                                      
                    $uid_POST      = is_valid_post_integer('uid');
                     
                    $disabletable=TRUE;
                    if(!($uid_POST['isSetted'] && $uid_POST['isValid'])){
                        echo 'Error retriving row';
                        $disabletable=FALSE;
                    }
                     
                    if( $disabletable){
                        $responsibleDisableSQL="UPDATE `offers` SET `isEnabled`=? WHERE idOffer=? ";
                        
                        $stmt_responsibleDisable = mysqli_stmt_init($link)or die(mysqli_error($link));
                        mysqli_stmt_prepare($stmt_responsibleDisable, $responsibleDisableSQL)or die(mysqli_error($link));
                        $disabled= intval(FALSE);
                        mysqli_stmt_bind_param($stmt_responsibleDisable,
                                "ii",
                                $disabled,
                                $uid_POST['value']
                              )or die(mysqli_error($link));
                        mysqli_stmt_execute($stmt_responsibleDisable)or die(mysqli_error($link));
                    }
                   echo "Disabilitazione riga";
                }else{
                   echo "Nulla da fare";  
                }
                
            }
           
?>
            <html>
                <head>
                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                    <title></title>
                    <LINK href="table.css" rel="stylesheet" type="text/css"> 
                </head>
                <body>
<?php
                //If the user has NOT to select the responsible standard visualization
                if(!$selectResponsible){
                    echo "<h1>Elenco Offerte</h1>";
                    //Recupero l'elenco di offerte
                    $offersListSQL = "SELECT `idOffer`, `codOffer`, `codOffice`, `codResp`, `hour`, `isEnabled` FROM `offers` ";
                    $stmt_offersList = mysqli_stmt_init($link)or die(mysqli_error($link));
                    mysqli_stmt_prepare($stmt_offersList, $offersListSQL)or die(mysqli_error($link));
                    mysqli_stmt_bind_result($stmt_offersList, $idOffer,$codOffer,$codOffice,$codResp,$hour,$isEnabled)or die(mysqli_error($link));
                    mysqli_stmt_execute($stmt_offersList)or die(mysqli_error($link));
?>                    
                    <div class="table">
                        <div class="thead">
                            <div class="tr">
                                <div class="td">codOffer</div>
                                <div class="td">codOffice</div>
                                <div class="td">codResp</div>
                                <div class="td">hour</div>
                                <div class="td">isEnabled</div>
                                <div class="td">-</div>
                            </div>
                        </div>
                        <div class="tbody">
<?php
                            //visualizzazione
                            while (mysqli_stmt_fetch($stmt_offersList)) {                    
                                echo("<form class=\"tr\" action=\"addOffer.php\" method=\"POST\">");
                                echo("    <div class=\"td\">$codOffer</div>");
                                if(array_key_exists($codOffice, $officeListArray)){
                                    echo("    <div class=\"td\">".$officeListArray[$codOffice]["desc"]."</div>");
                                }else{
                                    echo("    <div class=\"td\">$codOffice</div>");
                                }
                                //echo("    <div class=\"td\">$codResp</div>");
                                if(array_key_exists($codResp, $respListArray)){
                                    echo("    <div class=\"td\">".$respListArray[$codResp]["desc"]."</div>");
                                }else{
                                    echo("    <div class=\"td\">$codResp</div>");
                                }
                                echo("    <div class=\"td\">$hour</div>");
                                echo("    <div class=\"td\">$isEnabled</div>");
                                echo("    <input type=\"hidden\" name=\"uid\" value=\"$idOffer\" >");
                                echo("    <input type=\"hidden\" name=\"username\" value=\"$username\" >");
                                if($isEnabled){
                                    echo("    <input type=\"hidden\" name=\"operation\" value=\"$disableOperation\" >");
                                    echo("    <div class=\"td\"><button type=\"submit\" >Disable</button></div>");
                                }else{
                                    //echo("    <input type=\"hidden\" name=\"operation\" value=\"$disableOperation\" >");
                                    echo("    <div class=\"td\">&nbsp</div>");
                                }
                                echo("</form>");
                            }
                            //Parte di inserimento
                            echo("<form class=\"tr\" action=\"addOffer.php\" method=\"POST\">");
                            echo("    <div class=\"td\"><input type=\"text\" name=\"codOffer\"></div>");
                            //echo("    <div class=\"td\"><input type=\"text\" name=\"codOffice\"></div>");
                            echo("    <div class=\"td\"> <select id=\"codOffice\" size=\"1\" name=\"codOffice\">");
                            foreach ($officeListArray as $key => $value) {
                                if($value["enabled"]){
                                    echo("    <option value='$key'>").$value["desc"].("</option>");
                                }
                            }
                            echo("   </select> </div>");
                            echo("    <div class=\"td\"><input DISABLED type=\"text\" name=\"codResp\"></div>");
                            /*echo("    <div class=\"td\"> <select id=\"codResp\" size=\"1\" name=\"codResp\">");
                            foreach ($respListArray as $key => $value) {
                                if($value["enabled"]){
                                    echo("    <option value='$key'>").$value["desc"].("</option>");
                                }
                            }
                            echo("   </select> </div>");
                            */
                            echo("    <div class=\"td\"><input type=\"text\" name=\"hour\"></div>");
                            echo("    <div class=\"td\"><input READONLY type=\"text\" name=\"isEnabled\" value=\"1\"></div>");
                            echo("    <input type=\"hidden\" name=\"username\" value=\"$username\" >");
                            echo("    <input type=\"hidden\" name=\"operation\" value=\"$addOperation\" >");
                            echo("    <div class=\"td\"><button type=\"submit\" >Add</button></div>");
                            echo("</form>");
?>                                    
		        </div>
                    </div>
                    <p><a href="members.php">Return to your menu</a>.</p>
 <?php                     
                //If the user HAS TO select a responsible
                }else{
 ?>                  
                    <h1>Select the main Responsible</h1>
<?php                     
//var_dump($respOfOfficeList);
//foreach ($respOfOfficeList as $key => $value) {
//    echo '<BR>';
//    
//    var_dump($key);
//    echo '<BR>';
//    var_dump($value);
//}
?>  
                    <div class="table">
                        <div class="thead">
                            <div class="tr">
                                <div class="td">codOffer</div>
                                <div class="td">codOffice</div>
                                <div class="td">codResp</div>
                                <div class="td">hour</div>
                                <div class="td">isEnabled</div>
                                <div class="td">-</div>
                            </div>
                        </div>
                        <div class="tbody">
<?php                 
                            //Parte di selezione del responsabile
                            echo("<form class=\"tr\" action=\"addOffer.php\" method=\"POST\">");
                            echo("    <div class=\"td\"><input READONLY type=\"text\" name=\"codOffer\" value=\"").$codOffer_POST['value'].("\"></div>");
                            echo("    <input type=\"hidden\" READONLY type=\"text\" name=\"codOffice\" value=\"").$codOffice_POST['value'].("\">");
                            echo("    <div class=\"td\"> <select disabled id=\"codOfficeT\" size=\"1\" name=\"codOfficeT\">");
                            foreach ($officeListArray as $key => $value) {
                                if($value["enabled"]){
                                    if($codOffice_POST['value']==$key){
                                        echo("    <option value='$key' selected>").$value["desc"].("</option>");
                                    }else{
                                        echo("    <option value='$key'>").$value["desc"].("</option>");
                                    }
                                }
                            }
                            echo("   </select> </div>");
                            echo("    <div class=\"td\"> <select id=\"codResp\" size=\"1\" name=\"codResp\">");
                                foreach ($respOfOfficeList as $key => $value) {
                                    if($value["isEnabled"]){
                                        echo("    <option value='").$value["codResp"].("'>").$value["descrResp"].("</option>");
                                    }
                                }
                            echo("   </select> </div>");
                            echo("    <div class=\"td\"><input READONLY type=\"text\" name=\"hour\" value=\"").$hour_POST['value'].("\"></div>");
                            echo("    <div class=\"td\"><input READONLY type=\"text\" name=\"isEnabled\" value=\"1\"></div>");
                            echo("    <input type=\"hidden\" name=\"username\" value=\"$username\" >");
                            echo("    <input type=\"hidden\" name=\"operation\" value=\"$addOperation\" >");
                            echo("    <div class=\"td\"><button type=\"submit\" >Add</button></div>");
                            echo("</form>");
?>                 
                  
                        </div>
                    </div>
<?php
                  }
?>
                </body>
            </html>     
<?php   
            require_once ('./DB_CLOSE.INC.php');
        }else{
            require_once ('./DB_CLOSE.INC.php');
            header("Location: members.php");
        }
    }  else {
        require_once ('./DB_CLOSE.INC.php');
        header("Location: login.php"); 
    }
?>