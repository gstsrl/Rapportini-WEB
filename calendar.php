<?php

error_reporting(0);
require_once './SYS_validatingPostFunction.php';
require_once './SYS_paramClass.php';
require_once './SYS_mysqliConnClass.php';
require_once './SYS_queryClass.php';
require_once './SYS_loginClass.php';
require_once './SYS_statusCode.php';

require_once './BE_showCalendar.php';

$month_POST = is_valid_post_integer('month');
$year_POST = is_valid_post_integer('year');

$user_POST = is_valid_post_string('user');

$user = loginClass::getUidUser();
if ($user_POST['isSetted'] && $user_POST['isValid']) {
    $user = $user_POST['value'];
}


if ($month_POST['isSetted'] && $month_POST['isValid'] && $year_POST['isSetted'] && $year_POST['isValid']) {
    echo JSONshowMonth($month_POST['value'], $year_POST['value'], $user);
} else {
    echo JSONshowMonth(null, null, $user);
}
?>