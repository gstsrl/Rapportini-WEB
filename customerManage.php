<?php
require_once './SYS_loginClass.php';
require_once './SYS_paramClass.php';
require_once './SYS_mysqliConnClass.php';
require_once './SYS_queryClass.php';

$parametri = new Params();
$mysqlConn = new mysqliConnClass($parametri);
$mysqliConn = $mysqlConn->connect();
$authCHECK_LOGIN = loginClass::checkUser2($mysqliConn);
if ($authCHECK_LOGIN) {

    $usernameCHECK_ROLES = loginClass::getUidUser();
    $res = queryClass::checkUserRoleOnDb($mysqliConn, $usernameCHECK_ROLES);
    $mysqlConn->disconnect();
    ?>
    <!DOCTYPE html>
    <!--[if IE 8]><html class="no-js lt-ie9" lang="en" ><![endif]-->
    <!--[if gt IE 8]><!--><html class="no-js" ><!--<![endif]-->
        <html>
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">

                <title>Gestione Customer</title>
                <!-- Fogli di stile -->
                <link href="css/bootstrap.css" rel="stylesheet" media="screen">
                <link href="css/bootstrap-table.min.css" rel="stylesheet" media="screen">
                <link href="css/stili-custom.css" rel="stylesheet" media="screen">
                <!-- Modernizr -->
                <script src="js/modernizr.custom.js"></script>
                <!-- respond.js per IE8 -->
                <!--[if lt IE 9]>
                <script src="js/respond.min.js"></script>
                <![endif]-->
            </head>
            <body>
                <nav class="navbar navbar-default navbar-static-top">
                    <div class="container">
                        <ol class="breadcrumb">
                            <li><a href="menu.php"> Menu</a></li>
                            <li class="active">Gestione Customer</li>
                        </ol>
                    </div>
                </nav>
                <!--<div class="wrapper ">-->
                <div class="container">

                    <div class="row vertical-center-row">   


                        <?php
                        if ($res['isEnabledCHECK_ROLES'] && $res['isAdminCHECK_ROLES']) {
                            ?>         

                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">Customer List</div>

                                    <div class="panel-body">


                                        <div id="toolbar" class="btn-group">
                                            
                                            <a class="btn btn-default" href="customerAdd.php"><i class="glyphicon glyphicon-plus"></i>Add Customer</a>
                                            
                                            <!--<button type="button" class="btn btn-default">
                                                <i class="glyphicon glyphicon-trash"></i>
                                            </button>-->
                                        </div>
                                        <table id="tableId"
                                            data-toggle="table"
                                               data-url="./BE_customerList.php"
                                               data-method="post"
                                               data-content-type="application/x-www-form-urlencoded"
                                               data-pagination="true"
                                               data-side-pagination="server"
                                               data-page-size="4"
                                               data-page-list="[2,4,5, 10, 20, 50, 100, 200]"
                                               data-search="true"
                                               data-show-refresh="true"
                                               data-show-toggle="true"
                                               data-show-columns="true"
                                               data-height="300"
                                               data-toolbar="#toolbar">
                                            <thead>
                                                <tr>
                                                   <!-- <th data-field="state"  data-checkbox="true"></th>-->
                                                    <th data-field="codCust"    data-align="center" data-sortable="true">codCust</th>
                                                    <th data-field="descrCust"  data-align="left" data-sortable="true">descrCust</th>
                                                    <!--<th data-field="isEnabled"  data-sortable="false">isEnabled</th>-->
                                                    <th data-field="action"     data-align="center" data-formatter="actionFormatter" data-events="actionEvents">Action</th>
                                                </tr>
                                            </thead>
                                        </table>

                                        <!--                                        
                                        <table id="mytable" class="table table-bordred table-striped table-hover">
                                             <thead>
                                             <th>codCust</th>
                                             <th>descrCust</th>
                                             <th>isEnabled</th>
                                             <th>-</th>    
                                             </thead>
                                             <tbody>
                                                 <tr>
                                                     <td>https://www.codeofaninja.com/2014/06/php-object-oriented-crud-example-oop.html</td>
                                                     <td>http://xcrud.com/demos/index.php?page=js_tricks&theme=bootstrap</td>
                                                     <td>mario rossi LTD</td>
                                                     <td>TRUE</td>
                                                     <td><p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-lock"></span></button></p></td>
                                                 </tr>
                                             </tbody>
                                         </table>-->


                                    </div>
                                </div>
                            </div>
                            
                            <?php
                        } else {
                            ?>                    
                            <p>Utente non abilitato</p>    
                            <?php
                        }
                        ?>                
                    </div>
                </div>
                <!-- jQuery e plugin JavaScript  -->
                <script src="js/jquery-2.1.3.min.js"></script>
                <script src="js/i18next-1.7.7.min.js"></script>
                <script src="js/bootstrap.min.js"></script>
                <script src="js/bootstrap-table.min.js"></script>

                <script src="js/translation/customerManageT.js"></script>
                <script src="js/jsscript/customerManage.js"></script>
            </body>
        </html>         
        <?php
    } else {
        $mysqlConn->disconnect();
        header("Location: logout.php");
    }
    ?>