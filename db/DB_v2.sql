-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mag 27, 2015 alle 23:33
-- Versione del server: 5.6.15-log
-- PHP Version: 5.5.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gstbase`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `activities`
--

CREATE TABLE IF NOT EXISTS `activities` (
  `idAct` int(11) NOT NULL AUTO_INCREMENT,
  `codUser` varchar(20) NOT NULL,
  `activityDate` date NOT NULL,
  `activityType` varchar(10) NOT NULL,
  `activityDuration` decimal(4,2) NOT NULL,
  `activityOfferta` varchar(20) NOT NULL,
  `activityDescription` varchar(100) NOT NULL,
  PRIMARY KEY (`idAct`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=178 ;

--
-- Dump dei dati per la tabella `activities`
--

INSERT INTO `activities` (`idAct`, `codUser`, `activityDate`, `activityType`, `activityDuration`, `activityOfferta`, `activityDescription`) VALUES
(61, '33', '2015-05-11', 'PR', '8.50', 'BPBD1419', 'INTERFACCIA CANCELLAZIONE MESSAGGI SU HOST'),
(62, '33', '2015-05-12', 'PR', '9.00', 'BPBD1419', 'VERIFICA MESSAGGI FIRMATI FIAT CONTROLLO CUC'),
(63, 'cc', '2015-01-05', 'PR', '8.00', 'BPBD1501', 'NUOVO PGM FATTURAZIONE YOUINVOICE (STFATTUR)'),
(64, '14', '2015-01-05', 'VA', '8.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(68, '14', '2015-01-08', 'VA', '8.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(67, '14', '2015-01-07', 'VA', '8.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(69, '14', '2015-01-09', 'VA', '8.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(70, '14', '2015-01-12', 'VA', '5.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(71, '14', '2015-01-13', 'VA', '5.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(72, '14', '2015-01-14', 'VA', '4.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(73, '14', '2015-01-15', 'VA', '8.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(74, '14', '2015-01-16', 'VA', '8.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(75, '14', '2015-01-19', 'VA', '6.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(76, '14', '2015-01-26', 'VA', '8.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(77, '14', '2015-01-27', 'VA', '8.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(78, '14', '2015-01-28', 'VA', '8.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(79, '14', '2015-01-29', 'VA', '8.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(80, '14', '2015-01-30', 'VA', '5.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(98, '14', '2015-02-02', 'VA', '8.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(99, '14', '2015-02-03', 'VA', '5.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(100, '14', '2015-02-04', 'VA', '8.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(101, '14', '2015-02-05', 'VA', '5.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(85, '14', '2015-02-06', 'VA', '8.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(102, '14', '2015-02-09', 'VA', '4.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(87, '14', '2015-02-10', 'VA', '8.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(88, '14', '2015-02-11', 'VA', '8.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(89, '14', '2015-02-12', 'VA', '8.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(90, '14', '2015-02-13', 'VA', '8.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(107, '14', '2015-03-02', 'VA', '7.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(92, '14', '2015-02-17', 'VA', '8.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(93, '14', '2015-02-18', 'VA', '8.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(106, '14', '2015-02-23', 'VA', '5.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(95, '14', '2015-02-25', 'VA', '8.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(96, '14', '2015-02-26', 'VA', '8.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(97, '14', '2015-02-27', 'VA', '8.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(103, '14', '2015-02-19', 'VA', '8.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(104, '14', '2015-02-20', 'VA', '7.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(105, '14', '2015-02-21', 'VA', '5.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(108, '14', '2015-03-03', 'VA', '4.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(109, '14', '2015-03-04', 'VA', '4.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(110, '14', '2015-03-05', 'VA', '8.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(111, '14', '2015-03-06', 'VA', '8.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(112, '14', '2015-03-09', 'VA', '8.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(113, '14', '2015-03-10', 'VA', '8.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(114, '14', '2015-03-11', 'VA', '8.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(115, '14', '2015-03-12', 'VA', '8.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(116, '14', '2015-03-13', 'VA', '8.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(117, '14', '2015-03-16', 'VA', '8.00', 'BPBD1411', 'FATTURAZIONE ELETTRONICA'),
(118, '14', '2015-03-17', 'VA', '8.00', 'BPBD1414', 'FATTURAZIONE ELETTRONICA'),
(119, '14', '2015-03-18', 'VA', '8.00', 'BPBD1414', 'FATTURAZIONE ELETTRONICA'),
(120, '14', '2015-03-23', 'VA', '8.00', 'BPBD1414', 'FATTURAZIONE ELETTRONICA'),
(121, '14', '2015-03-25', 'VA', '8.00', 'BPBD1414', 'FATTURAZIONE ELETTRONICA'),
(122, '14', '2015-03-26', 'VA', '8.00', 'BPBD1414', 'FATTURAZIONE ELETTRONICA'),
(123, '14', '2015-03-27', 'VA', '8.00', 'BPBD1414', 'FATTURAZIONE ELETTRONICA'),
(124, '14', '2015-03-30', 'VA', '8.00', 'BPBD1414', 'FATTURAZIONE ELETTRONICA'),
(125, '14', '2015-03-31', 'VA', '8.00', 'BPBD1414', 'FATTURAZIONE ELETTRONICA'),
(126, '14', '2015-04-01', 'VA', '8.00', 'BPBD1414', 'FATTURAZIONE'),
(127, '14', '2015-04-02', 'VA', '8.00', 'BPBD1414', 'FATTURAZIONE'),
(128, '14', '2015-04-07', 'VA', '8.00', 'BPBD1414', 'INSERIMENTO MANUALE FIRME'),
(129, '14', '2015-04-08', 'VA', '8.00', 'BPBD1414', 'INSERIMENTO MANUALE FIRME'),
(130, '14', '2015-04-09', 'VA', '8.00', 'BPBD1414', 'INSERIMENTO MANUALE FIRME'),
(131, '14', '2015-04-10', 'VA', '8.00', 'BPBD1415', 'UNIONE FIDUCIARIA'),
(132, '14', '2015-04-13', 'VA', '8.00', 'BPBD1415', 'UNIONE FIDUCIARIA'),
(133, '14', '2015-04-14', 'VA', '6.00', 'BPBD1503', 'UNIONE FIDUCIARIA'),
(134, '14', '2015-04-15', 'VA', '8.00', 'BPBD1415', 'UNIONE FIDUCIARIA'),
(135, '14', '2015-04-16', 'VA', '8.00', 'BPBD1415', 'TRANSACTION MONITOR'),
(136, '14', '2015-04-17', 'VA', '8.00', 'BPBD1415', 'TRANSACTION MONITOR'),
(137, '14', '2015-04-20', 'VA', '8.00', 'BPBD1415', 'TRANSACTION MONITOR'),
(138, '14', '2015-04-21', 'VA', '6.00', 'BPBD1503', 'FATTURAZIONE'),
(139, '14', '2015-04-22', 'VA', '8.00', 'BPBD1415', 'FATTURAZIONE'),
(140, '14', '2015-04-23', 'VA', '7.00', 'BPBD1503', 'FATTURAZIONE'),
(141, '14', '2015-04-24', 'VA', '8.00', 'BPBD1415', 'FATTURAZIONE'),
(142, '14', '2015-04-27', 'VA', '8.00', 'BPBD1415', 'POTERI DI FIRMA'),
(143, '14', '2015-04-28', 'VA', '8.00', 'BPBD1415', 'POTERI DI FIRMA'),
(144, '14', '2015-04-29', 'VA', '8.00', 'BPBD1503', 'POTERI FIRMA'),
(145, '33', '2015-05-15', 'PR', '10.00', 'BPBD1419', 'FIRMA FIAT+ DOCUMENTO RCI  RENATO'),
(146, '33', '2015-05-14', 'PR', '8.00', 'BPBD1419', 'FIRMA MESSAGGI FIAT'),
(147, '33', '2015-05-13', 'PG', '8.00', 'BPBD1419', 'FIRMA MESSAGGI FIAT'),
(148, '33', '2015-05-18', 'PR', '9.00', 'BPBD1419', 'CONTROLLO FIRMA FIAT'),
(149, '33', '2015-05-19', 'PG', '10.00', 'BPBD1419', 'FIRMA FIAT GENERAZIONE CODICE'),
(150, '33', '2015-05-20', 'PR', '10.00', 'BPBD1419', 'FIRMA FIAT SCT SDD E STORE PROC'),
(151, '33', '2015-05-21', 'DO', '9.00', 'BPBD1419', 'DOCUMENTAZIONE FIRMA FIAT'),
(152, 'cb', '2015-05-22', 'PR', '8.00', 'BPBD1501', 'TRANSACTION MONITOR'),
(155, '33', '2015-05-25', 'IN', '10.00', 'BPBD1419', 'INSTALLAZIONE IN COLLAUDO '),
(154, '33', '2015-05-22', 'DO', '10.00', 'BPBD1419', 'DOCUMENTAZIONE FIAT'),
(156, '14', '2015-05-04', 'AN', '8.00', 'BPBD1503', 'YCB'),
(157, '14', '2015-05-05', 'DO', '8.00', 'BPBD1503', 'YCB'),
(158, '14', '2015-05-06', 'PR', '4.00', 'BPBD1503', 'YOU INVOICE'),
(159, '14', '2015-05-07', 'DO', '8.00', 'BPBD1503', 'YCB'),
(160, '14', '2015-05-08', 'DO', '8.00', 'BPBD1503', 'YCB'),
(161, '14', '2015-05-11', 'AN', '8.00', 'BPBD1503', 'QLIKVIEW'),
(162, '14', '2015-05-12', 'AN', '8.00', 'BPBD1503', 'UNIONE FIDUCIARIA'),
(163, '14', '2015-05-13', 'AN', '8.00', 'BPBD1503', 'QLIKVIEW UNIONE FIDUCIARIA'),
(164, '14', '2015-05-14', 'AN', '8.00', 'BPBD1503', 'MODIFICA CONTRATTI'),
(165, '14', '2015-05-15', 'AN', '8.00', 'BPBD1503', 'QLIKVIEW, TRANSACTION MONITOR, STAMPE'),
(166, '14', '2015-05-18', 'DO', '8.00', 'BPBD1503', 'YCB'),
(167, '14', '2015-05-19', 'DO', '8.00', 'BPBD1503', 'YCB'),
(168, '14', '2015-05-20', 'DO', '8.00', 'BPBD1503', 'YCB'),
(169, '14', '2015-05-21', 'DO', '8.00', 'BPBD1501', 'YCB'),
(170, '14', '2015-05-22', 'DO', '8.00', 'BPBD1503', 'YCB'),
(171, '14', '2015-05-25', 'DO', '8.00', 'BPBD1503', 'YCB'),
(172, '14', '2015-05-26', 'AN', '8.00', 'BPBD1503', 'YCB'),
(176, '14', '2015-05-27', 'DO', '8.00', 'BPBD1503', 'YCB'),
(174, '33', '2015-05-26', 'PG', '8.00', 'BPBD1419', 'FIAT CONTROLLO FIRME ONLINE'),
(177, '33', '2015-05-27', 'PR', '10.00', 'BPBD1419', 'AGGIORNAMENTO COPY COBOL');

-- --------------------------------------------------------

--
-- Struttura della tabella `activitytypes`
--

CREATE TABLE IF NOT EXISTS `activitytypes` (
  `idType` int(11) NOT NULL AUTO_INCREMENT,
  `codType` varchar(2) DEFAULT NULL,
  `descrType` varchar(50) DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idType`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dump dei dati per la tabella `activitytypes`
--

INSERT INTO `activitytypes` (`idType`, `codType`, `descrType`, `order`) VALUES
(1, 'PG', 'PROGETTAZIONE', 0),
(2, 'AN', 'ANALISI', 0),
(3, 'PR', 'PROGRAMMAZIONE', 0),
(4, 'AS', 'ASSISTENZA', 0),
(5, 'TE', 'TEST', 0),
(6, 'IN', 'INSTALLAZIONE', 0),
(7, 'AP', 'AVVIAMENTO PROCEDURA', 0),
(8, 'DO', 'DOCUMENTAZIONE', 0),
(9, 'AD', 'ADDESTRAMENTO', 0),
(10, 'VA', 'VARIE', 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `courses`
--

CREATE TABLE IF NOT EXISTS `courses` (
  `idCourses` int(11) NOT NULL AUTO_INCREMENT,
  `Argomento` varchar(100) DEFAULT NULL,
  `Data` date DEFAULT NULL,
  `Tipologia` int(11) DEFAULT NULL,
  `idResources` int(11) DEFAULT NULL,
  PRIMARY KEY (`idCourses`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dump dei dati per la tabella `courses`
--

INSERT INTO `courses` (`idCourses`, `Argomento`, `Data`, `Tipologia`, `idResources`) VALUES
(1, 'Corso Analista programmatore', '1988-01-01', NULL, 14),
(2, 'Indottrinamento al Sistema di Qualità Aziendale ISO 9001', '1998-09-21', NULL, 14),
(3, 'Corso di formazione su Project Management c/o Prforma', '2006-02-07', NULL, 14),
(4, 'Indottrinamento al sistema di Qualità Aziendale ISO 9001:2008', '2009-02-27', NULL, 14),
(5, 'Corso COBOL fusione Bancopopolare', '2012-02-01', NULL, 14),
(6, 'Corso interno VERA catalogo prodotti remote banking', '2012-03-01', NULL, 14),
(7, 'Formazione generale e specifica in materia di sicurezza sul lavoro', '2014-04-23', NULL, 14);

-- --------------------------------------------------------

--
-- Struttura della tabella `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `idCust` int(11) NOT NULL AUTO_INCREMENT,
  `codCust` varchar(20) NOT NULL,
  `descrCust` varchar(255) NOT NULL,
  `isEnabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`idCust`),
  UNIQUE KEY `customer_uk` (`codCust`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

--
-- Dump dei dati per la tabella `customers`
--

INSERT INTO `customers` (`idCust`, `codCust`, `descrCust`, `isEnabled`) VALUES
(31, 'SGSBP', 'SGSBP', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `evaluations`
--

CREATE TABLE IF NOT EXISTS `evaluations` (
  `idEvaluation` int(11) NOT NULL AUTO_INCREMENT,
  `commento` varchar(255) DEFAULT NULL,
  `Data` date DEFAULT NULL,
  `idResources` int(11) NOT NULL,
  PRIMARY KEY (`idEvaluation`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dump dei dati per la tabella `evaluations`
--

INSERT INTO `evaluations` (`idEvaluation`, `commento`, `Data`, `idResources`) VALUES
(1, 'Non vi è la necessità di far eseguire ulteriori aggiornamenti', '2010-01-31', 14),
(2, 'La risorsa risulta ben inserita nel profilo aziendale, non ci sono richieste di ulteriori aggiornamenti da parte del cliente per cui lavora', '2010-01-31', 14),
(3, 'Corsi eseguiti con esito positivo. Risorsa idonea al profilo aziendale', '2012-04-01', 14),
(4, 'La risorsa non necessita di corsi di aggiornamento e risulta idonea al profilo aziendale', '2013-05-02', 14),
(5, 'Eseguito con  esito positivo visita medica aziendale per videoterminalista', '2013-11-08', 14),
(6, 'Risorsa idonea al profilo aziendale', '2014-05-12', 14);

-- --------------------------------------------------------

--
-- Struttura della tabella `offers`
--

CREATE TABLE IF NOT EXISTS `offers` (
  `idOffer` int(11) NOT NULL AUTO_INCREMENT,
  `codOffer` varchar(20) NOT NULL,
  `descrOffer` varchar(255) NOT NULL,
  `codOffice` varchar(20) NOT NULL,
  `codResp` varchar(20) NOT NULL,
  `hour` int(11) NOT NULL,
  `isEnabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`idOffer`),
  UNIQUE KEY `offer_uk` (`codOffer`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

--
-- Dump dei dati per la tabella `offers`
--

INSERT INTO `offers` (`idOffer`, `codOffer`, `descrOffer`, `codOffice`, `codResp`, `hour`, `isEnabled`) VALUES
(18, 'BPBD1501', 'MANUTENZIONE CORRETTIVA PROCEDURA MWRB', '03322', 'GS00182', 240, 1),
(19, 'BPBD1502', 'ASSISTENZA SISTEMISTICA HOMEBANKING PER LA REALIZZAZIONE DEGLI INTERVENTI PREVISTI PER IL I TRIMESTRE 2015', '03437', 'GS01357', 1040, 1),
(20, 'BPBD1503', 'REMOTE BANKING: INTERVENTI EVOLUTIVI - I TRANCHE (SVILUPPO MAINFRAME)', '03322', 'GS00182', 960, 1),
(21, 'BPBD1504', 'REMOTE BANKING: INTERVENTI EVOLUTIVI - I TRANCHE (SVILUPPO DIPARTIMENTALE)', '03322', 'GS00182', 240, 1),
(22, 'BPBD1505', 'YOUINVOICE-GESTIONE CONTRATTI IN FILIALE', '03322', 'GS00182', 1200, 1),
(23, 'BPBD1506', 'VANTAGGIO EVOLUZIONE YOUCARDBUSINESS RICARICHE MASSIVE', '03322', 'GS00182', 312, 1),
(24, 'BPBD1507', 'VANTAGGIO-FIRMA DIGITALE: APERTURA CONTRATTI YCB DA VANTAGGIO (SVILUPPI DIPARTIMENTALI)', '03322', 'GS00182', 800, 1),
(25, 'BPBD1508', 'VANTAGGIO-FIRMA DIGITALE: APERTURA CONTRATTI YCB DA VANTAGGIO (SVILUPPI MAINFRAME)', '03322', 'GS00182', 400, 1),
(26, 'BPBD1411', 'VANTAGGIO - EVOLUZIONE GESTIONE AUTORIZZAZIONI FLUSSI SXGA', '03322', 'GS00182', 1920, 1),
(27, 'BPBD1413', 'VANTAGGIO - EVOLUZIONE GESTIONE AUTORIZZAZIONE FLUSSI SXGA', '03322', 'GS00182', 480, 1),
(28, 'BPBD1415', 'VANTAGGIO -EVOLUZIONI CBI: SCT, SDD, SEDA, BONIFICO ESTERO XML', '03322', 'GS00182', 320, 1),
(29, 'BPBD1418', 'IDEA 3514 EVOLUZIONE INTERNET BANKING: IMPLEMENTAZIONI DI SICUREZZA (PSD2), REVISIONE GRAFICA COMPLETA DEL SERVIZIO YOUWEB', '03321', 'GS00148', 504, 1),
(30, 'BPBD1419', 'VANTAGGIO - BANCA PAPERLESS / FIRMA DIGITALE', '03322', 'GS00182', 800, 1),
(31, 'BPBD1420', 'EVOLUZIONE YOUWEB, IMPLEMENTAZIONE SICUREZZA (SEMAFORI SMASH, NUOVI PARAMETRI DI ANALISI COMPORTAMENTALE, REVISIONE SEZIONE SICUREZZA, REVISIONE HOME PAGE F24)', '03321', 'GS00148', 880, 1),
(32, 'BPBD1421', 'ASSISTENZA SISTEMISTICA HOME BANKING PER LA REALIZZAZIONE DEGLI INTERVENTI PREVISTI PER IL IV TRIMESTRE 2014', '03437', 'GS01357', 960, 1),
(33, 'BPBD1414', 'AUTORIZZATIVO -YOUCARD BIZ', '03322', 'GS00182', 480, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `offices`
--

CREATE TABLE IF NOT EXISTS `offices` (
  `idOffice` int(11) NOT NULL AUTO_INCREMENT,
  `codOffice` varchar(20) NOT NULL,
  `descrOffice` varchar(255) NOT NULL,
  `isEnabled` tinyint(1) NOT NULL,
  `codCust` varchar(20) NOT NULL,
  PRIMARY KEY (`idOffice`),
  UNIQUE KEY `office_uk` (`codOffice`,`codCust`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dump dei dati per la tabella `offices`
--

INSERT INTO `offices` (`idOffice`, `codOffice`, `descrOffice`, `isEnabled`, `codCust`) VALUES
(8, '03322', 'APPLICAZIONI CANALI PER IMPRESE', 1, 'SGSBP'),
(9, '03321', 'APPLICAZIONI BANCA ON LINE', 1, 'SGSBP'),
(10, '03437', 'SISTEMI DISTRIBUITI', 1, 'SGSBP');

-- --------------------------------------------------------

--
-- Struttura della tabella `resources`
--

CREATE TABLE IF NOT EXISTS `resources` (
  `idRes` int(11) NOT NULL AUTO_INCREMENT,
  `codRes` varchar(50) DEFAULT '0',
  `Nome` varchar(50) DEFAULT '0',
  `Cognome` varchar(50) DEFAULT '0',
  `Indirizzo` varchar(50) DEFAULT '0',
  `Livello` varchar(50) DEFAULT '0',
  `Qualifica` varchar(50) DEFAULT '0',
  `PosizioneOrganigramma` varchar(50) DEFAULT '0',
  `DataNascita` date DEFAULT '0000-00-00',
  `Telefono` varchar(50) DEFAULT '0',
  `ProfiloScolastico` varchar(50) DEFAULT '0',
  `CorrispondenzaProfilo` bit(1) DEFAULT b'0',
  `Esperienza` int(11) DEFAULT '0',
  `isEnabled` bit(1) DEFAULT b'0',
  PRIMARY KEY (`idRes`),
  UNIQUE KEY `Indice 2` (`codRes`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dump dei dati per la tabella `resources`
--

INSERT INTO `resources` (`idRes`, `codRes`, `Nome`, `Cognome`, `Indirizzo`, `Livello`, `Qualifica`, `PosizioneOrganigramma`, `DataNascita`, `Telefono`, `ProfiloScolastico`, `CorrispondenzaProfilo`, `Esperienza`, `isEnabled`) VALUES
(1, '14', 'Fulvio', 'Brun', 'Via L.Galvani,1,37138,Verona', 'CCC', 'Socio', 'UTM/ASS/FQ', '1967-10-07', '3384615311', 'Perito Commerciale', b'1', 22, b'1'),
(2, '33', 'Riccardo', 'Prandini', 'Via Piave,23,37012,Bussolengo', 'XXX', 'Imbianchino', '0', '1985-02-26', '3471088660', 'Ingegnere Informatico', b'0', 6, b'1');

-- --------------------------------------------------------

--
-- Struttura della tabella `responsibles`
--

CREATE TABLE IF NOT EXISTS `responsibles` (
  `idResp` int(11) NOT NULL AUTO_INCREMENT,
  `codResp` varchar(20) NOT NULL,
  `isEnabled` tinyint(1) NOT NULL,
  `codOffice` varchar(20) NOT NULL,
  `descrResp` varchar(255) NOT NULL,
  PRIMARY KEY (`idResp`),
  UNIQUE KEY `responsible_uk` (`codResp`,`codOffice`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

--
-- Dump dei dati per la tabella `responsibles`
--

INSERT INTO `responsibles` (`idResp`, `codResp`, `isEnabled`, `codOffice`, `descrResp`) VALUES
(28, 'GS00182', 1, '03322', 'Federico Scamperle'),
(29, 'GS00148', 1, '03321', 'Franco Tebaldi'),
(30, 'GS01357', 1, '03437', 'Alessandro Catenazzi');

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `idUser` int(11) NOT NULL AUTO_INCREMENT,
  `codUser` varchar(60) NOT NULL,
  `username` varchar(60) NOT NULL,
  `password` varchar(60) NOT NULL,
  `isAccount` tinyint(1) NOT NULL DEFAULT '0',
  `isAdmin` tinyint(1) NOT NULL DEFAULT '0',
  `isResp` tinyint(1) NOT NULL DEFAULT '0',
  `isUser` tinyint(1) NOT NULL DEFAULT '1',
  `isEnabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idUser`),
  UNIQUE KEY `user_uk` (`codUser`),
  UNIQUE KEY `accesso_uk` (`username`,`password`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=69 ;

--
-- Dump dei dati per la tabella `users`
--

INSERT INTO `users` (`idUser`, `codUser`, `username`, `password`, `isAccount`, `isAdmin`, `isResp`, `isUser`, `isEnabled`) VALUES
(49, 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 0, 1, 0, 1, 1),
(50, '33', 'rp', '00639c71ba1dbde84db84b3eb15d6820', 0, 0, 0, 1, 1),
(52, '2', 'cc', 'e0323a9039add2978bf5b49550572c7c', 0, 0, 0, 1, 1),
(53, '14', 'fb', '35ce1d4eb0f666cd136987d34f64aedc', 0, 0, 0, 1, 1),
(54, '32', 'cb', 'd0d7fdb6977b26929fb68c6083c0b439', 0, 0, 0, 1, 1),
(55, 'rp1', 'rp1', '28e8bd7bb34043a2d09f3a8b27f4149f', 0, 0, 0, 1, 1),
(56, '36', 'gm', '92073d2fe26e543ce222cc0fb0b7d7a0', 0, 0, 0, 1, 1),
(57, '34', 'ms', 'ee33e909372d935d190f4fcb2a92d542', 0, 0, 0, 1, 1),
(58, 'mm', 'mm', 'b3cd915d758008bd19d0f2428fbb354a', 0, 0, 0, 1, 1),
(59, 'cf', 'cf', '4e29342d9904d64e9e25fd3b92558e2f', 0, 0, 0, 1, 1),
(68, 'GS00182', 'fsc', '26d0e472e93a1a01aee710c6c4d07264', 0, 0, 1, 0, 1);
--
-- Database: `spc`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `assegnazioni`
--

CREATE TABLE IF NOT EXISTS `assegnazioni` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_calendario_servizio` int(11) DEFAULT NULL,
  `id_calendario_operatore` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_assegnazioni_calendario_servizi` (`id_calendario_servizio`),
  KEY `FK_assegnazioni_calendario_operatori` (`id_calendario_operatore`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dump dei dati per la tabella `assegnazioni`
--

INSERT INTO `assegnazioni` (`id`, `id_calendario_servizio`, `id_calendario_operatore`) VALUES
(1, 1, 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `calendario_operatori`
--

CREATE TABLE IF NOT EXISTS `calendario_operatori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `operatore` int(11) NOT NULL DEFAULT '0',
  `Data` date NOT NULL DEFAULT '0000-00-00',
  `Dalle` time NOT NULL DEFAULT '00:00:00',
  `Alle` time NOT NULL DEFAULT '00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `operatore_Data_Dalle_Alle` (`operatore`,`Data`,`Dalle`,`Alle`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dump dei dati per la tabella `calendario_operatori`
--

INSERT INTO `calendario_operatori` (`id`, `operatore`, `Data`, `Dalle`, `Alle`) VALUES
(1, 1, '2015-05-25', '00:00:00', '23:59:00'),
(3, 1, '2015-05-26', '00:00:00', '23:59:00'),
(4, 1, '2015-05-28', '00:00:00', '23:59:00');

-- --------------------------------------------------------

--
-- Struttura della tabella `calendario_servizi`
--

CREATE TABLE IF NOT EXISTS `calendario_servizi` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Servizio` int(11) DEFAULT NULL,
  `Data` date DEFAULT NULL,
  `Dalle` time DEFAULT NULL,
  `Alle` time DEFAULT NULL,
  `Descrizione` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Servizio_Data_Dalle_Alle` (`Servizio`,`Data`,`Dalle`,`Alle`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dump dei dati per la tabella `calendario_servizi`
--

INSERT INTO `calendario_servizi` (`Id`, `Servizio`, `Data`, `Dalle`, `Alle`, `Descrizione`) VALUES
(1, 1, '2015-05-01', '11:10:42', '14:10:48', NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `operatori`
--

CREATE TABLE IF NOT EXISTS `operatori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(50) DEFAULT NULL,
  `Cognome` varchar(50) DEFAULT NULL,
  `Altro` varchar(50) DEFAULT NULL,
  `Recapito_SMS1` varchar(50) DEFAULT NULL,
  `Recapito_SMS2` varchar(50) DEFAULT NULL,
  `Recapito_SMS3` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dump dei dati per la tabella `operatori`
--

INSERT INTO `operatori` (`id`, `Nome`, `Cognome`, `Altro`, `Recapito_SMS1`, `Recapito_SMS2`, `Recapito_SMS3`) VALUES
(1, 'Mario', 'Rossi', 'ss', '1234567890', NULL, NULL),
(2, 'Paolo', 'Bianchi', NULL, '3333333333', '222222222', NULL),
(3, 'Luca', 'Neri', NULL, '9475926592', NULL, NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `servizi`
--

CREATE TABLE IF NOT EXISTS `servizi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(50) DEFAULT NULL,
  `Descrizione` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dump dei dati per la tabella `servizi`
--

INSERT INTO `servizi` (`id`, `Nome`, `Descrizione`) VALUES
(1, 'Fiera del Riso', 'Parcheggi'),
(2, 'Rally Due Valli', 'Percorso'),
(3, 'Fiera del Riso', 'Viabilità');

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `assegnazioni`
--
ALTER TABLE `assegnazioni`
  ADD CONSTRAINT `FK_assegnazioni_calendario_operatori` FOREIGN KEY (`id_calendario_operatore`) REFERENCES `calendario_operatori` (`id`),
  ADD CONSTRAINT `FK_assegnazioni_calendario_servizi` FOREIGN KEY (`id_calendario_servizio`) REFERENCES `calendario_servizi` (`Id`);

--
-- Limiti per la tabella `calendario_operatori`
--
ALTER TABLE `calendario_operatori`
  ADD CONSTRAINT `FK__operatori` FOREIGN KEY (`operatore`) REFERENCES `operatori` (`id`);

--
-- Limiti per la tabella `calendario_servizi`
--
ALTER TABLE `calendario_servizi`
  ADD CONSTRAINT `FK_calendarioservizi_servizi` FOREIGN KEY (`Servizio`) REFERENCES `servizi` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
