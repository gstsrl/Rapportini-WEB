<?php
require_once './SYS_validatingPostFunction.php';
require_once './SYS_validatingGetFunction.php';
require_once './SYS_paramClass.php';
require_once './SYS_mysqliConnClass.php';
require_once './SYS_queryClass.php';
require_once './pdf/FPDF/fpdf.php';
require_once './BE_feste.php';
require_once './BE_color.php';

class PDF extends FPDF {
    
    
    // Page header
    function Header() {
        // Logo
        $this->Image('GST_logo.jpg', 257, 3, 30);
        // Arial bold 15
        $this->SetFont('Arial', 'B', 15);
        // Background color
        $this->genericColor(BE_color::$grigio);
        // Title
        $title = 'ALTRO DAL '.$this->from->format("Y-m-d"). ' AL '.$this->to->format("Y-m-d");
        $w = $this->GetStringWidth($title)+6;
        $this->SetX((210-$w)/2);
        $this->SetY(15);
        $w = 0;
        $this->Cell($w, 9, $title, 1, 1, 'C', true);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(0, 6, 'File: '.$this->fileName.$this->fileExt.' ', 0, 1, 'L');
        // Line break

        $this->Ln(0);
        
        $this->genericColor(BE_color::$bianco);
        if($this->stdHeader){
               
            $this->Cell(15, 6, 'MESE:', 'LTB', 0, 'L', true);
            $this->Cell(55, 6, $this->mesi[''.(((int)$this->month)-1)], 'TB', 0, 'L', true);
            $this->Cell(15, 6, 'ANNO:', 'TB', 0, 'L', true);
            $this->Cell(55, 6, $this->year, 'TB', 0, 'L', true);
            
            
            
            
        }else{
            $this->Cell(15, 6, '', 'LTB', 0, 'L', true);
            $this->Cell(55, 6, '', 'TB', 0, 'L', true);
            $this->Cell(15, 6, '', 'TB', 0, 'L', true);
            $this->Cell(55, 6, '', 'TB', 0, 'L', true);
        }
        
        $this->Cell(0, 6, 'foglio n' . iconv('UTF-8', 'windows-1252', html_entity_decode("&deg;")) . ' ' . $this->PageNo() . ' di {nb}', 'TBR', 1, 'L', true);
        $this->Ln(2);

    }

    // Page footer
    function Footer() {
        
    // Position at 1.5 cm from bottom
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        // Page number
        //$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
        // Move to the right
        //$this->Cell(80);
        $str = iconv('UTF-8', 'windows-1252', html_entity_decode("&copy;"));
        $this->Cell(0, 10, $str . ' G.S.T. Srl - Riproduzione vietata ', 0, 0, 'L');
        $this->Cell(0, 10, ''.$this->fileName.' del '.$this->fileDate.'', 0, 0, 'R');

    }
    
    
    function HolidayTable($activityOfUsers,$typeList){
        $this->genericColor(BE_color::$bianco);
        $this->Cell(60, 5,"LEGENDA:", 1, 0, 'L', true);
        foreach ($typeList as $key2 => $value2) {

                $fcol=$typeList[$key2]['color'];
                $ftype=$typeList[$key2]['type'];
                $fdesc=$typeList[$key2]['descrOffer'];
                $this->genericColor($fcol);
                $w=($this->GetStringWidth($fdesc))+2;
                
                $this->Cell($w, 5,$fdesc, 1, 0, 'L', true);
        }
        $this->Ln();
        $this->Ln();
        
        
        $feste = new Feste ();
        $this->genericColor(BE_color::$bianco);
        $ggMese = cal_days_in_month(CAL_GREGORIAN, (int)$this->month, (int)$this->year); 
       
        
        //HEADER OF holiday
        $this->Cell(60, 5, 'Nome', 1, 0, 'L', true);
        
        for ($i = 1; $i <= $ggMese; $i++) {
            $this->genericColor(BE_color::$bianco);
            $isFesta=$feste->isFestaYMd((int)$this->year, (int)$this->month, (int)$i);
            if($isFesta){
               $this->genericColor(BE_color::$grigio);
            }else{
                $this->genericColor(BE_color::$bianco);
            }
            $this->Cell(6, 5, $i, 1, 0, 'C', true);
            
        }
        $this->genericColor(BE_color::$bianco);
        $this->Cell(6, 5, '', 1, 0, 'L', true);
        $this->Ln();
        
        //EACH ROW OF USER
        
        $this->genericColor(BE_color::$bianco);
        foreach ($activityOfUsers as $key => $value) {
            $this->Cell(60, 5, $value['name'], 1, 0, 'L', true);
            $dv=$value['days'];
            for ($i = 1; $i <= $ggMese; $i++) {
                $isFesta=$feste->isFestaYMd((int)$this->year, (int)$this->month, (int)$i);
               
                $x=$dv[''.(int)$this->year][''.(int)$this->month][''.(int)$i][0];            
                $t=$dv[''.(int)$this->year][''.(int)$this->month][''.(int)$i][1];            
                if (!empty($x)) {
                    $fcol= BE_color::$bianco;
                    foreach ($typeList as $key1 => $value1) {
                        if($typeList[$key1]['type']==$t){
                            $fcol=$typeList[$key1]['color'];
                        }
                    }
                    $this->genericColor($fcol);
                    
                }else{
                    if($isFesta){
                        $this->genericColor(BE_color::$grigio);
                    }else{
                       $this->genericColor(BE_color::$bianco);
                    }
                }
                $this->Cell(6, 5, $x, 1, 0, 'C', true);
                $this->genericColor(BE_color::$bianco);
            }
            
            $this->genericColor(BE_color::$bianco);
            $this->Cell(6, 5, '', 1, 0, 'L', true);
            
            $this->genericColor(BE_color::$bianco);
            
            //$xx=$this->GetX();
            //$ps=210;
            /*
           
            */
                       
            $this->Ln();
            $this->genericColor(BE_color::$bianco);
        }
           
    }
    public function HolidaySumMonthTable($activityOfUsers,$typeList) {
        $ggMese = cal_days_in_month(CAL_GREGORIAN, (int)$this->month, (int)$this->year);
        
        $tot=0;
        foreach ($typeList as $key2 => $value2) {
            $fdesc=$typeList[$key2]['descrOffer'];
            $tot=$tot+($this->GetStringWidth($fdesc))+2;
            
        }
        
        
        $this->genericColor(BE_color::$bianco);
        $this->Cell(60, 5, 'Nome', 1, 0, 'L', true);
        $this->Cell($tot, 5, 'Totale', 1, 0, 'L', true);
        $this->genericColor(BE_color::$bianco);
        
        
        $this->Ln();
        
        $this->Cell(60, 5, '', 1, 0, 'L', true);
        foreach ($typeList as $key2 => $value2) {

                $fcol=$typeList[$key2]['color'];
                $ftype=$typeList[$key2]['type'];
                $fdesc=$typeList[$key2]['descrOffer'];
                $this->genericColor($fcol);
                $w=($this->GetStringWidth($fdesc))+2;
                
                $this->Cell($w, 5,$fdesc, 1, 0, 'L', true);
        }
        $this->genericColor(BE_color::$bianco);
        
        
        
        $this->Ln();
        foreach ($activityOfUsers as $key => $value) {
            $totMese=array(); 
            $this->Cell(60, 5, $value['name'], 1, 0, 'L', true);
            $dv=$value['days'];
                
            for ($i = 1; $i <= $ggMese; $i++) {
                $x=$dv[''.(int)$this->year][''.(int)$this->month][''.(int)$i][0];            
                $t=$dv[''.(int)$this->year][''.(int)$this->month][''.(int)$i][1];
                if (!empty($x)) {
                     $totMese[$t]=$totMese[$t]+$x;   
                }
                           
            }
            
            
            foreach ($typeList as $key2 => $value2) {

                $fcol=$typeList[$key2]['color'];
                $ftype=$typeList[$key2]['type'];
                $fdesc=$typeList[$key2]['descrOffer'];
                $w=($this->GetStringWidth($fdesc))+2;
                $this->genericColor($fcol);
                $this->Cell($w, 5, $totMese[$ftype], 1, 0, 'L', true);
            }
            $this->genericColor(BE_color::$bianco);
            $this->Ln();
        }
        
    }
    public function HolidaySumYearTable($activityOfUsers,$typeList) {
        
        
        $tot=0;
        foreach ($typeList as $key2 => $value2) {
            $fdesc=$typeList[$key2]['descrOffer'];
            $tot=$tot+($this->GetStringWidth($fdesc))+2;
            
        }
        
        
        $this->genericColor(BE_color::$bianco);
        $this->Cell(60, 5, 'Nome', 1, 0, 'L', true);
        $this->Cell($tot, 5, 'Totale', 1, 0, 'L', true);
        $this->genericColor(BE_color::$bianco);
        
        
        $this->Ln();
        
        $this->Cell(60, 5, '', 1, 0, 'L', true);
        foreach ($typeList as $key2 => $value2) {

                $fcol=$typeList[$key2]['color'];
                $ftype=$typeList[$key2]['type'];
                $fdesc=$typeList[$key2]['descrOffer'];
                $this->genericColor($fcol);
                $w=($this->GetStringWidth($fdesc))+2;
                
                $this->Cell($w, 5,$fdesc, 1, 0, 'L', true);
        }
        $this->genericColor(BE_color::$bianco);
        
        
        
        $this->Ln();
        
        
        foreach ($activityOfUsers as $key => $value) {
            $this->Cell(60, 5, $value['name'], 1, 0, 'L', true);
            $Y=$value['days'];
            $totAnni=array();
           
            foreach ($Y as $key1 => $value1) {
                $M=$value1;
                $totAnno=array();
                foreach ($M as $key2 => $value2) {
                    $D=$value2;
                    $totMese=array();
                    $t=-1;
                    foreach ($D as $key3 => $value3) {
                        $x=$value3[0];
                        $t=$value3[1];        
                        if (!empty($x)) {
                            $totMese[$t]=$totMese[$t]+$x;
                        }
                    }
                    
                    foreach ($totMese as $key => $value) {
                        $totAnno[$key]=$totAnno[$key]+$totMese[$key];
                    }                    
                }
                foreach ($totAnno as $key => $value) {
                        $totAnni[$key]=$totAnni[$key]+$totAnno[$key];
                }                
            }
            
            foreach ($typeList as $key4 => $value4) {
                        $fcol=$typeList[$key4]['color'];
                        $ftype=$typeList[$key4]['type'];
                        $fdesc=$typeList[$key4]['descrOffer'];
                        $w=($this->GetStringWidth($fdesc))+2;
                        $this->genericColor($fcol);
                        $this->Cell($w, 5, $totAnni[$ftype], 1, 0, 'L', true);
            }
            
            $this->genericColor(BE_color::$bianco);
            //$this->Cell(60, 5, $totAnni, 1, 0, 'L', true);
            $this->Ln();
            //$totUser=$totUser+$totAnni;
        }
         
        
    }
    
    var $giorni = array('Domenica','Lunedi','Martedi','Mercoledi','Giovedi','Venerdi','Sabato');
    var $mesi = array('Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre');
    var $fileName='Mass1.1';
    var $fileExt='.odt';
    var $fileDate='02/10/2013';
    
    var $stdHeader=TRUE;
    
    var $from;
    var $to;
    var $month;
    var $year;     
    
   // var $cliente;
   // var $incaricato;
   // var $responsabile;
   // var $dataCreazione;
   // var $totGiorni;
   // var $totOre;
  
    function SetStdHeader($stdHeader) {
        $this->stdHeader = $stdHeader;
    }
    function SetFrom($from) {
        $this->from = $from;
    }
    
    function SetTo($to){
        $this->to=$to;
    }
    
    function SetMonth($month){
        $this->month=$month;
    }
    
    function SetYear($year){
        $this->year=$year;
    }
    
    public function genericColor($c) {
        $this->SetTextColor(0, 0, 0);
        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor($c[0], $c[1], $c[2]);
    }
    
    
    
   

    

}





$from_GET           = is_valid_get_string('from');
$to_GET             = is_valid_get_string('to');
$codUser_GET        = is_valid_get_stringArray('codUser');
$types_GET          = is_valid_get_stringArray('types');

$users = array();


$parametri = new Params();
$mysqlConn = new mysqliConnClass($parametri);
$mysqliConn = $mysqlConn->connect();

if ($codUser_GET['isSetted'] && $codUser_GET['isValid']){
    
    foreach ($codUser_GET['value'] as &$value) {
        $descrUsr = queryClass::getDescrUsr($mysqliConn, $value);
        $users[$value] =  $descrUsr ;
    }
    unset($value);
}

$activityList=  queryClass::listHolidays($mysqliConn,$from_GET['value'],$to_GET['value'],$types_GET['value'] ,$codUser_GET['value']);
$typeList=queryClass::listOffersByTypes($mysqliConn,$types_GET['value']);

$mysqlConn->disconnect();

BE_color::init();
foreach ($typeList as $key => $value) {
    $t=$typeList[$key]['type'];
    switch ($t){
        case 0:
            $typeList[$key]['color']=BE_color::$bianco;
        break;
        case 1:
            $typeList[$key]['color']=BE_color::$giallo;
        break;
        case 2:
            $typeList[$key]['color']=BE_color::$rosso;
        break;
        case 3:
            $typeList[$key]['color']=BE_color::$blue;
        break;
        case 4:
            $typeList[$key]['color']=BE_color::$celeste;
        break;
        case 5:
            $typeList[$key]['color']=BE_color::$viola;
        break;
        case 6:
            $typeList[$key]['color']=BE_color::$verde;
        break;
    }

}





$ol=organize($users,$activityList);



$dfd = new DateTime($from_GET['value']);
$dtd = new DateTime($to_GET['value']);        

$interval = $dfd->diff($dtd);


$dmd = clone $dfd;

// Instanciation of inherited class
$pdf = new PDF();

$pdf->SetFrom($dfd);
$pdf->SetTo($dtd);
$pdf->SetYear($dmd->format("Y"));
$pdf->SetMonth($dmd->format("m"));  



//$pdf->SetDataCreazione($ds);
//$pdf->SetTitle("Rapportino");
//$pdf->SetAuthor("AUTH");
$pdf->AliasNbPages();

$pdf->SetStdHeader(TRUE);


$pdf->AddPage('L');
$pdf->HolidayTable($ol,$typeList);
$pdf->AddPage('L');
$pdf->HolidaySumMonthTable($ol,$typeList);



//Passo tutti i giorni e se cambio mese comincio una nuova pagina
while($dmd<$dtd){
    $monthOld=(int)$dmd->format("m");
    $dmd=$dmd->add(new DateInterval('P1D'));
    $monthNew=(int)$dmd->format("m");
    if(!($monthOld === $monthNew)){
        $pdf->SetYear($dmd->format("Y"));
        $pdf->SetMonth($dmd->format("m"));        
        $pdf->AddPage('L');
        $pdf->HolidayTable($ol,$typeList);
        $pdf->AddPage('L');
        $pdf->HolidaySumMonthTable($ol,$typeList);
    }
}
$pdf->SetStdHeader(FALSE);

$pdf->AddPage('L');
$pdf->HolidaySumYearTable($ol,$typeList);
        

ob_end_clean();
$pdf->Output('Ferie.pdf', 'D');

function organize($users,$activityList){
    $organizedList=array();
   
    foreach ($users as $keyU => $valueU) {
        $days=array();
        $organizedList[$keyU]['name']=$valueU;
        
        foreach ($activityList as $keyA => $valueA) {
            if((int)$valueA['codUser']===$keyU){
                $dataA= new DateTime($valueA['activityDate']);
                $days[''.(int)$dataA->format("Y")][''.(int)$dataA->format("m")][''.(int)$dataA->format("d")][0]=$valueA['activityDuration'];
                $days[''.(int)$dataA->format("Y")][''.(int)$dataA->format("m")][''.(int)$dataA->format("d")][1]=$valueA['type'];
            }
        }
        
        
        $organizedList[$keyU]['days']=$days;
        
    }
    return $organizedList;
    
}
?>