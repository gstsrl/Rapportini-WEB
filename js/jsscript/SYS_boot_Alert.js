/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function ( $ ) {
 
    $.fn.bootAlert = function( options ) {
 
        // This is the easiest way to have default options.
        var settings = $.extend({
            // These are the defaults.
            class: "alert-warning",
            warnTtl:"",
            warnMsg:"",
            warnMsgCause:"",
            warnMsgCod:""
        }, options );

        var warnTtlElement=this.find( 'h4#warnTtl' );
        var warnTtlTexEmpty=(!settings.warnTtl || settings.warnTtl.length === 0);
        //Manca e deve esserci
        if(warnTtlElement.length===0 && !warnTtlTexEmpty){
            this.append( '<h4 id="warnTtl" >'+settings.warnTtl+'</h4>' );
         //c'e ma non deve esserci   
        }else if(warnTtlElement.length===1 && warnTtlTexEmpty){
            warnTtlElement.remove();
        //c'e e deve esserci il testo    
        }else if(warnTtlElement.length===1 && !warnTtlTexEmpty){
            warnTtlElement.text(settings.warnTtl);
        } 
        
        var warnMsgElement=this.find( 'p#warnMsg' );
        var warnMsgTexEmpty=(!settings.warnMsg || settings.warnMsg.length === 0);
        //Manca e deve esserci
        if(warnMsgElement.length===0 && !warnMsgTexEmpty){
            this.append( '<p id="warnMsg">'+settings.warnMsg+'</p>' );
         //c'e ma non deve esserci   
        }else if(warnMsgElement.length===1 && warnMsgTexEmpty){
            warnMsgElement.remove();
        //c'e e deve esserci il testo    
        }else if(warnMsgElement.length===1 && !warnMsgTexEmpty){
            warnMsgElement.text(settings.warnMsg);
        } 
        
         var warnMsgCauseElement=this.find( 'p#warnMsgCause' );
        var warnMsgCauseTexEmpty=(!settings.warnMsgCause || settings.warnMsgCause.length === 0);
        //Manca e deve esserci
        if(warnMsgCauseElement.length===0 && !warnMsgCauseTexEmpty){
            this.append( '<p id="warnMsgCause">'+settings.warnMsgCause+'</p>' );
         //c'e ma non deve esserci   
        }else if(warnMsgCauseElement.length===1 && warnMsgCauseTexEmpty){
            warnMsgCauseElement.remove();
        //c'e e deve esserci il testo    
        }else if(warnMsgCauseElement.length===1 && !warnMsgCauseTexEmpty){
            warnMsgCauseElement.text(settings.warnMsgCause);
        } 
        
        var warnMsgCodElement=this.find( 'p#warnMsgCod' );
        var warnMsgCodTexEmpty=(!settings.warnMsgCod || settings.warnMsgCod.length === 0);
        //Manca e deve esserci
        if(warnMsgCodElement.length===0 && !warnMsgCodTexEmpty){
            this.append( '<p id="warnMsgCod">'+settings.warnMsgCod+'</p>' );
         //c'e ma non deve esserci   
        }else if(warnMsgCodElement.length===1 && warnMsgCodTexEmpty){
            warnMsgCodElement.remove();
        //c'e e deve esserci il testo    
        }else if(warnMsgCodElement.length===1 && !warnMsgCodTexEmpty){
            warnMsgCodElement.text(settings.warnMsgCod);
        } 
 
        this.removeClass('alert-success');
        this.removeClass('alert-info');
        this.removeClass('alert-warning');
        this.removeClass('alert-danger');
        this.addClass(settings.class);
        this.addClass('in');
        
 
    };
 
}( jQuery ));

