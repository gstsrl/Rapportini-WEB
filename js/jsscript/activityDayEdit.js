$(document).ready(function () {
    $('a#post-link').click(function () {
        $(this).attr('href')

        $('body').append($('<form/>', {
            id: 'formDayLink',
            method: 'POST',
            action: $(this).attr('href')
        }));

        $('#formDayLink').append($('<input/>', {
            type: 'hidden',
            name: 'month',
            value: $(this).attr('month')
        }));
        $('#formDayLink').append($('<input/>', {
            type: 'hidden',
            name: 'year',
            value: $(this).attr('year')
        }));
        $('#formDayLink').append($('<input/>', {
            type: 'hidden',
            name: 'day',
            value: $(this).attr('day')
        }));
//                    $('#formDayLink').append($('<input/>', {
//                        type: 'hidden',
//                        name: 'field2',
//                        value: 'bar'
//                    }));
        $('#formDayLink').submit();
        return false;
    });

    $('a#post-form-link').click(function () {
        $(this).attr('href')

        $('body').append($('<form/>', {
            id: 'formDayFormLink',
            method: 'POST',
            action: $(this).attr('href')
        }));

        $('#formDayFormLink').append($('<input/>', {
            type: 'hidden',
            name: 'month',
            value: $(this).attr('month')
        }));
        $('#formDayFormLink').append($('<input/>', {
            type: 'hidden',
            name: 'year',
            value: $(this).attr('year')
        }));
        $('#formDayFormLink').append($('<input/>', {
            type: 'hidden',
            name: 'day',
            value: $(this).attr('day')
        }));
        $('#formDayFormLink').append($('<input/>', {
            type: 'hidden',
            name: 'activityDescription',
            value: $('#activityDescription').val()
        }));
        $('#formDayFormLink').append($('<input/>', {
            type: 'hidden',
            name: 'activityDuration',
            value: $('#activityDuration').val()
        }));
        $('#formDayFormLink').append($('<input/>', {
            type: 'hidden',
            name: 'activityType',
            value: $('#activityType').val()
        }));
        $('#formDayFormLink').append($('<input/>', {
            type: 'hidden',
            name: 'activityOfferta',
            value: $('#activityOfferta').val()
        }));
        
        $('#formDayFormLink').submit();
        return false;
    });

    var datavalue = {limit: -1};//codCustomer: codCustomerV, descrCustomer: descrCustomerV, submit: "Add"};
    $.ajax({
        type: "POST",
        url: "BE_offerList.php",
        data: datavalue,
        cache: false,
        dataType: "json",
//                  
        success: function (data) {
            var options = $("#activityOfferta");
            //var totale=data.total;
            var righe=data.rows;
            if(data.total>0){
                $.each(righe, function () {
                    if (this !== null && this.isEnabled === 1) {
                        var opt= $("<option />").val(this.codOffer).html(this.codOffer.concat(" - ", this.descrOffer));
                        if(! ($("#activityOfferta").attr('value')) || ($("#activityOfferta").attr('value')).localeCompare(this.codOffer)){
                        
                        }else{
                            opt=opt.attr('selected', true);
                        }
                        opt=opt.attr('type', this.type);
                        options.append(opt);
                    }
                });
            }
            $('#activityOfferta').change(function() {
                var txt = $('#activityOfferta').find(":selected").val();
                var tipologia= $('#activityOfferta').find(":selected").attr('type');
                switch (tipologia) {
                    case "0":
                        //$('#activityDescription').val("");
                        break;
                    default:    
                        $('#activityDescription').val(txt);
                        $('#activityDuration').val("8");
                        $('#activityType').find('option[value="VA"]').prop('selected', true);
                        break;
   
                }
               
            });
        }

    });
    
    $.ajax({
        type: "POST",
        url: "BE_activityTypeList.php",
        data: datavalue,
        cache: false,
        dataType: "json",
                  
        success: function (data) {
            var options = $("#activityType");
            //var totale=data.total;
            var righe = data.rows;
            if (data.total > 0) {
                $.each(righe, function () {
                    if (this !== null ) {
                        if(! ($("#activityType").attr('value')) ||$("#activityType").attr('value').localeCompare(this.codType)){
                            options.append($("<option />").val(this.codType).html(this.codType.concat(" - ", this.descrType)));
                        }else{
                            options.append($("<option selected />").val(this.codType).html(this.codType.concat(" - ", this.descrType)));
                        }
                    }
                });
            }
        }

    });


    $("#editActivityForm").submit(function (event) {
        console.log("Handler for .submit() called.");
        event.preventDefault();
        var activityOffertaV = $('#activityOfferta').val();
        var activityDurationV = $('#activityDuration').val();
        var activityDescriptionV = $('#activityDescription').val();
        var activityDayV = $('#activityDay').val();
        var activityMonthV = $('#activityMonth').val();
        var activityYearV = $('#activityYear').val();
        var activityUserV = $('#activityUser').val();
        var activityTypeV = $('#activityType').val();
        var activityIdV = $('#activityId').val();


        // Checking for blank fields.
        if (activityTypeV === '' || activityOffertaV === '' || activityDurationV === '' || activityIdV == '') {

        } else {
            var datavalue = {activityOfferta: activityOffertaV, activityType: activityTypeV, activityDuration: activityDurationV, activityDescription: activityDescriptionV,
                activityDay: activityDayV, activityMonth: activityMonthV, activityYear: activityYearV, activityUser: activityUserV,activityId: activityIdV, submit: "Edit"};
            if ($.trim(activityOffertaV).length > 0 && $.trim(activityTypeV).length > 0 && $.trim(activityDurationV).length > 0 && $.trim(activityDescriptionV).length > 0)
            {
                $.ajax({
                    type: "POST",
                    url: "BE_activityEdit.php",
                    data: datavalue,
                    cache: false,
                    dataType: "json",
                    // beforeSend: function(){ $("#login").val('Connecting...');},
                    success: function (data) {
                        if (data) {
                            console.log(data);
                            if (data["retCode"] === "R0005") {
                                $('#le-alert').bootAlert({class: "alert-success", warnTtl: "OK", warnMsg: "Attivita aggiornata", warnMsgCod: data["retCode"]});
                            } else {
                                $('#le-alert').bootAlert({class: "alert-warning", warnTtl: "Error", warnMsg: "Attivita NON aggiornata", warnMsgCause: "Errore di aggiornamento", warnMsgCod: data["retCode"]});
                            }
                        }
                    }
                });
            }
        }
        return false;
    });
    $('.close').click(function () {
        $(this).parent().removeClass('in'); // hides alert with Bootstrap CSS3 implem
    });

    $('input[type=text]').keyup(function () {
        //$(this).val($(this).val().toUpperCase());
    });

    $("#codCustomer").keyup(function () {


        var codCustomerV = $("#codCustomer").val();
        if (codCustomerV === '') {
            $("#codCustomerUsed").removeClass("glyphicon-ok");
            $("#codCustomerUsed").addClass("glyphicon-remove");
            $("#codCustomerUsed").css("color", "#FF0004");
        } else {
            var datavalue = {codCustomer: codCustomerV, submit: "Find"};
            if ($.trim(codCustomerV).length > 0)
            {
                $.ajax({
                    type: "POST",
                    url: "BE_customerExist.php",
                    data: datavalue,
                    cache: false,
                    dataType: "json",
//                  beforeSend: function(){ $("#login").val('Connecting...');},
                    success: function (data) {
                        if (data) {
                            console.log(data);
                            if (data["retCode"] == "R0004") {
                                $("#codCustomerUsed").removeClass("glyphicon-remove");
                                $("#codCustomerUsed").addClass("glyphicon-ok");
                                $("#codCustomerUsed").css("color", "#00A41E");
                            }
                            if (data["retCode"] == "R0003") {
                                $("#codCustomerUsed").removeClass("glyphicon-ok");
                                $("#codCustomerUsed").addClass("glyphicon-remove");
                                $("#codCustomerUsed").css("color", "#FF0004");
                            }
                        }
                    }
                });
            }
        }
    });
});


