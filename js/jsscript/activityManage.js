$(document).ready(function () {
    $('a#post-link').click(function () {
        $(this).attr('href')
        
        $('body').append($('<form/>', {
            id: 'formDayLink',
            method: 'POST',
            action: $(this).attr('href')
        }));

        $('#formDayLink').append($('<input/>', {
            type: 'hidden',
            name: 'month',
            value: $(this).attr('month')
        }));
        $('#formDayLink').append($('<input/>', {
            type: 'hidden',
            name: 'year',
            value: $(this).attr('year')
        }));
        $('#formDayLink').append($('<input/>', {
            type: 'hidden',
            name: 'day',
            value: $(this).attr('day')
        }));
//                    $('#formDayLink').append($('<input/>', {
//                        type: 'hidden',
//                        name: 'field2',
//                        value: 'bar'
//                    }));
        $('#formDayLink').submit();
        return false;
    });
});


$(function () {
    $('#tableId').bootstrapTable(); // init via javascript

    $(window).resize(function () {
        $('#tableId').bootstrapTable('resetView');
    });
});

function actionFormatter(value, row, index) {

    //isEnabled = Boolean(row.isEnabled);
    //if (isEnabled)
        return [
        '<a class="edit ml10" href="javascript:void(0)" title="Edit">',
            '<i class="glyphicon glyphicon-pencil"></i>',
            '</a>',    
        '<a class="delete ml10" href="javascript:void(0)" title="Delete">',
            '<i class="glyphicon glyphicon-trash"></i>',
            '</a>'
            
        ].join('');
}


window.actionEvents = {
    'click .delete': function (e, value, row, index) {
        //var datavalue = {idAct: row.idAct, codUser: row.codUser, activityDate: row.activityDate, activityDuration: row.activityDuration,activityOfferta: row.activityOfferta,activityDescription:row.activityDescription};
        $.ajax({
            type: "POST",
            url: "BE_activityDelete.php",
            data: row,
            cache: false,
            dataType: "json",
            beforeSend: function () {
            //    $("#login").val('Connecting...');
            },
            success: function (data) {
                
//                if (data) {
//                    console.log(data);
//
//                    if (data["retCode"] === "R0001") {
//                        //$("body").load("menu.php").hide().fadeIn(1500).delay(6000);
//                        //or
//                        window.location.href = "menu.php";
//                    }
//                    if (data["retCode"] === "E0003") {
//                        $('#le-alert').addClass('in');
//                    }
//                }
            }
        });
        setTimeout( "$('#tableId').bootstrapTable('refresh');",1500 );
        setTimeout( "$('#tableId').bootstrapTable('refresh');",0 );
        //alert('You click lock icon, row: ' + JSON.stringify(row));
        console.log(value, row, index);
    },
    'click .edit': function (e, value, row, index) {
           
        //alert('You click edit icon, row: ' + JSON.stringify(row));
        
        console.log(value, row, index);
        $('body').append($('<form/>', {
            id: 'formActivityEditLink',
            method: 'POST',
            action: 'activityDayEdit.php'
        }));
        
         $('#formActivityEditLink').append($('<input/>', {
            type: 'hidden',
            name: 'activityDescription',
            value: row.activityDescription
        }));
        $('#formActivityEditLink').append($('<input/>', {
            type: 'hidden',
            name: 'month',
            value: $('a#post-link').attr('month')
        }));
        $('#formActivityEditLink').append($('<input/>', {
            type: 'hidden',
            name: 'year',
            value: $('a#post-link').attr('year')
        }));
        $('#formActivityEditLink').append($('<input/>', {
            type: 'hidden',
            name: 'day',
            value: $('a#post-link').attr('day')
        }));
         $('#formActivityEditLink').append($('<input/>', {
            type: 'hidden',
            name: 'activityDate',
            value: row.activityDate
        }));
         $('#formActivityEditLink').append($('<input/>', {
            type: 'hidden',
            name: 'activityDuration',
            value: row.activityDuration
        }));
        $('#formActivityEditLink').append($('<input/>', {
            type: 'hidden',
            name: 'activityOfferta',
            value: row.activityOfferta
        }));
 $('#formActivityEditLink').append($('<input/>', {
            type: 'hidden',
            name: 'activityType',
            value: row.activityType
        }));
$('#formActivityEditLink').append($('<input/>', {
            type: 'hidden',
            name: 'codUser',
            value: row.codUser
        }));
$('#formActivityEditLink').append($('<input/>', {
            type: 'hidden',
            name: 'idAct',
            value: row.idAct
        }));

        
        
        
        $('#formActivityEditLink').submit();
    
    }
    

};


