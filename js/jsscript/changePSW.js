$(document).ready(function () {
    $("#changePswForm").submit(function (event) {
        console.log("Handler for .submit() called.");
        event.preventDefault();
       
        var usernameV = $('#username').val();
        var passOldV = $('#passOld').val();
        var passV = $('#pass').val();
        var pass2V = $('#pass2').val();
        
      

       

        // Checking for blank fields.
        if (usernameV===''||passOldV === '' || passV === '' || pass2V === ''
                || ($("#pass").val() !== $("#pass2").val())) {

        } else {
            var datavalue = {username: usernameV, passOld: passOldV, pass: passV, pass2: pass2V, submit: "Change"};
            if ($.trim(passOldV).length > 0 && $.trim(passV).length > 0 )
            {
                $.ajax({
                    type: "POST",
                    url: "BE_changePSW.php",
                    data: datavalue,
                    cache: false,
                    dataType: "json",
//                    beforeSend: function(){ $("#login").val('Connecting...');},
                    success: function (data) {
                        if (data) {
                            console.log(data);
                            if (data["retCode"] === "R0002") {
                                $('#le-alert').bootAlert({class: "alert-success", warnTtl: "OK", warnMsg: "Password variata", warnMsgCod: data["retCode"]});
                            } else if (data["retCode"] === "E0004") {
                                $('#le-alert').bootAlert({class: "alert-warning", warnTtl: "Error", warnMsg: "Password NON variata", warnMsgCause: "Utente esistente", warnMsgCod: data["retCode"]});
                            } else if (data["retCode"] === "E0005") {
                                $('#le-alert').bootAlert({class: "alert-warning", warnTtl: "Error", warnMsg: "Password NON variata", warnMsgCause: "Password non coincidenti", warnMsgCod: data["retCode"]});
                            } else if (data["retCode"] === "E0006") {
                                $('#le-alert').bootAlert({class: "alert-warning", warnTtl: "Error", warnMsg: "Password NON variata", warnMsgCause: "Errore di inserimento", warnMsgCod: data["retCode"]});
                            }
                        }
                    }
                });
            }
        }
        return false;
    });
    $('.close').click(function () {
        $(this).parent().removeClass('in'); // hides alert with Bootstrap CSS3 implem
    });

    $("input[type=password]").keyup(function () {
        if ($("#pass").val() === $("#pass2").val()) {
            $("#pwmatch").removeClass("glyphicon-remove");
            $("#pwmatch").addClass("glyphicon-ok");
            $("#pwmatch").css("color", "#00A41E");
        } else {
            $("#pwmatch").removeClass("glyphicon-ok");
            $("#pwmatch").addClass("glyphicon-remove");
            $("#pwmatch").css("color", "#FF0004");
        }

    });
});