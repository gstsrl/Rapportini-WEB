$(document).ready(function () {
    $("#addCustomerForm").submit(function (event) {
        console.log("Handler for .submit() called.");
        event.preventDefault();
        var codCustomerV = $('#codCustomer').val();
        var descrCustomerV = $('#descrCustomer').val();

        console.log(codCustomerV);
        console.log(descrCustomerV);

        // Checking for blank fields.
        if (codCustomerV === '' || descrCustomerV === '') {

        } else {
            var datavalue = {codCustomer: codCustomerV, descrCustomer: descrCustomerV, submit: "Add"};
            if ($.trim(codCustomerV).length > 0 && $.trim(descrCustomerV).length > 0)
            {
                $.ajax({
                    type: "POST",
                    url: "BE_customerAdd.php",
                    data: datavalue,
                    cache: false,
                    dataType: "json",
//                  beforeSend: function(){ $("#login").val('Connecting...');},
                    success: function (data) {
                        if (data) {
                            console.log(data);
                            if (data["retCode"] === "R0002") {
                                $('#le-alert').bootAlert({class: "alert-success", warnTtl: "OK", warnMsg: "Customer inserito", warnMsgCod: data["retCode"]});
                            } else {
                                $('#le-alert').bootAlert({class: "alert-warning", warnTtl: "Error", warnMsg: "Customer NON inserito", warnMsgCause: "Errore di inserimento", warnMsgCod: data["retCode"]});
                            } 
                        }
                    }
                });
            }
        }
        return false;
    });
    $('.close').click(function () {
        $(this).parent().removeClass('in'); // hides alert with Bootstrap CSS3 implem
    });

    $('input[type=text]').keyup(function () {
        $(this).val($(this).val().toUpperCase());
    });

    $("#codCustomer").keyup(function () {


        var codCustomerV = $("#codCustomer").val();
        if (codCustomerV === '') {
            $("#codCustomerUsed").removeClass("glyphicon-ok");
            $("#codCustomerUsed").addClass("glyphicon-remove");
            $("#codCustomerUsed").css("color", "#FF0004");
        } else {
            var datavalue = {codCustomer: codCustomerV, submit: "Find"};
            if ($.trim(codCustomerV).length > 0)
            {
                $.ajax({
                    type: "POST",
                    url: "BE_customerExist.php",
                    data: datavalue,
                    cache: false,
                    dataType: "json",
//                  beforeSend: function(){ $("#login").val('Connecting...');},
                    success: function (data) {
                        if (data) {
                            console.log(data);
                            if (data["retCode"] == "R0004") {
                                $("#codCustomerUsed").removeClass("glyphicon-remove");
                                $("#codCustomerUsed").addClass("glyphicon-ok");
                                $("#codCustomerUsed").css("color", "#00A41E");
                            }
                            if (data["retCode"] == "R0003") {
                                $("#codCustomerUsed").removeClass("glyphicon-ok");
                                $("#codCustomerUsed").addClass("glyphicon-remove");
                                $("#codCustomerUsed").css("color", "#FF0004");
                            }
                        }
                    }
                });
            }
        }
    });
});


