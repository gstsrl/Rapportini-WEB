$(function () {
    $('#tableId').bootstrapTable(); // init via javascript

    $(window).resize(function () {
        $('#tableId').bootstrapTable('resetView');
    });
});

function actionFormatter(value, row, index) {

    isEnabled = Boolean(row.isEnabled);
    if (isEnabled)
        return [
            '<a class="lock ml10" href="javascript:void(0)" title="Lock">',
            '<i class="glyphicon glyphicon-lock"></i>',
            '</a>'
        ].join('');
}


window.actionEvents = {
    'click .lock': function (e, value, row, index) {
        var datavalue = {idCust: row.idCust, codCust: row.codCust, descrCust: row.descrCust, isEnabled: row.isEnabled};
        $.ajax({
            type: "POST",
            url: "BE_customerDisable.php",
            data: row,
            cache: false,
            dataType: "json",
            beforeSend: function () {
                $("#login").val('Connecting...');
            },
            success: function (data) {
                
//                if (data) {
//                    console.log(data);
//
//                    if (data["retCode"] === "R0001") {
//                        //$("body").load("menu.php").hide().fadeIn(1500).delay(6000);
//                        //or
//                        window.location.href = "menu.php";
//                    }
//                    if (data["retCode"] === "E0003") {
//                        $('#le-alert').addClass('in');
//                    }
//                }
            }
        });
        $('#tableId').bootstrapTable('refresh');
        //alert('You click lock icon, row: ' + JSON.stringify(row));
        console.log(value, row, index);
    }

};


