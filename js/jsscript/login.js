$(document).ready(function(){
    $( "#loginForm" ).submit(function( event ) {
        console.log( "Handler for .submit() called." );
        event.preventDefault();
        var usernameV=$('#username').val();
        var passwordV=$('#password').val();
        console.log(usernameV);
        console.log(passwordV);
         // Checking for blank fields.
        if( usernameV ==='' || passwordV ===''){
            //$('input[type="text"],input[type="password"]').css("border","2px solid red");
            //$('input[type="text"],input[type="password"]').css("box-shadow","0 0 3px red");
            //$('#le-alert').addClass('in'); // shows alert with Bootstrap CSS3 implem
        }else{
            var datavalue=  { username: usernameV, pass: passwordV, submit:"Login" };
            if($.trim(usernameV).length>0 && $.trim(passwordV).length>0)
            {
                $.ajax({
                    type: "POST",
                    url: "BE_loginDo.php",
                    data: datavalue,
                    cache: false,
                    dataType: "json",
                    beforeSend: function(){ $("#login").val('Connecting...');},
                    success: function(data){
                        if(data){
                            console.log(data);
                        
                            if( data["retCode"] ==="R0001"){
                                
                                
                                window.location.href = "menu.php";
                            }if( data["retCode"] ==="E0003"){ 
                                $('#le-alert').addClass('in');
                            }
                        }
                    }
                });
            }
        }
        return false;
    });
    $('.close').click(function () {
        $(this).parent().removeClass('in'); // hides alert with Bootstrap CSS3 implem
    });
});
