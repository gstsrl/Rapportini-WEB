
$(document).ready(function () {
    initCalendar('.calendar', '.calendar-next', '.calendar-prev', 'calendar.php');
});

function initCalendar(clendarUid, calNavNext, calNavPrev, calendarPHP) {
    //chiamo la funzione del calendario passandogli il backend
    
    var params = {};
    params=queryParams();
    //if(){
        callCalendar(clendarUid, calendarPHP,params['month'], params['year']);
    //}else{
    //callCalendar(clendarUid, calendarPHP);
    //}

    //Applico la funzione del calendario
    $('body').delegate(calNavNext, 'click', function (e) {
        e.preventDefault();
        callCalendar(clendarUid, calendarPHP, $(this).attr('month'), $(this).attr('year'));
    });

    //Applico la funzione del calendario
    $('body').delegate(calNavPrev, 'click', function (e) {
        e.preventDefault();
        callCalendar(clendarUid, calendarPHP, $(this).attr('month'), $(this).attr('year'));
    });
}


function callCalendar(uid, url, month, year) {
    var datavalue = {month: month, year: year};
    $.ajax({
        type: "POST",
        url: url,
        data: datavalue,
        cache: false,
        dataType: "json",
        success: function (data) {
            if (data) {
                var calendarData = data.data;



                var refPanelCalendar = $("<div class='panel-heading text-center'></div>");
                var titleRow = $("<div class='row'></div>");
                var titlePrevCol = $("<div class='col-md-3 col-xs-4'></div>");
                var titlePrevLink = $("<a class='calendar-prev ajax-navigation btn btn-default btn-sm' href='#' month='" + calendarData.prevVis_month + "' year='" + (calendarData.prevVis_year) + "'><span class='glyphicon glyphicon-arrow-left'></span></a>");
                var prevYear = calendarData.currVis_year - 1;
                var titlePrevLink2 = $("<a class='calendar-prev ajax-navigation btn btn-default btn-sm' href='#' month='" + calendarData.currVis_month + "' year='" + (prevYear) + "'><span class='glyphicon glyphicon-backward'></span></a>");
                var titlePrevLink3 = $("<a class='calendar-prev ajax-navigation btn btn-default btn-sm' href='#' month='" + calendarData.today_month + "' year='" + calendarData.today_year + "'><span class='glyphicon glyphicon-arrow-down'></span></a>");
                var titleCurrentCol = $("<div class='col-md-6 col-xs-4'></div>");
                var titleCurrentText = $("<strong>" + calendarData.currVis_month + "-" + calendarData.currVis_year + "</strong>");
                var titleNextCol = $("<div class='col-md-3 col-xs-4'></div>");
                var titleNextLink = $("<a class='calendar-next ajax-navigation btn btn-default btn-sm' href='#' month='" + calendarData.nextVis_month + "' year='" + (calendarData.nextVis_year) + "'><span class='glyphicon glyphicon-arrow-right'></span></a>");
                var nextYear = (calendarData.currVis_year - 1) + 2;
                var titleNextLink2 = $("<a class='calendar-next ajax-navigation btn btn-default btn-sm' href='#' month='" + calendarData.currVis_month + "' year='" + (nextYear) + "'><span class='glyphicon glyphicon-forward'></span></a>");
                var titleNextLink3 = $("<a class='calendar-next ajax-navigation btn btn-default btn-sm' href='#' month='" + calendarData.today_month + "' year='" + calendarData.today_year + "'><span class='glyphicon glyphicon-arrow-down'></span></a>");
                var monthTable = $("<table class='table table-bordered'></table>");
                var dayName = $("<tr><th>Lun</th><th>Mar</th><th>Mer</th><th>Gio</th><th>Ven</th><th>Sab</th><th>Dom</th></tr>");

                titlePrevCol.append(titlePrevLink2);
                titlePrevCol.append(titlePrevLink);
                titlePrevCol.append(titlePrevLink3);
                titleCurrentCol.append(titleCurrentText);
                titleNextCol.append(titleNextLink3);
                titleNextCol.append(titleNextLink);
                titleNextCol.append(titleNextLink2);

                titleRow.append(titlePrevCol);
                titleRow.append(titleCurrentCol);
                titleRow.append(titleNextCol);



                refPanelCalendar.append(titleRow);



                monthTable.append(dayName);

                var dayMatrix = calendarData.dayMatrix;
                for (var i = 0; i < dayMatrix.length; i++) {
                    var dayRow = $("<tr></tr>");
                    var dayMatrixRow = dayMatrix[i];
                    for (var j = 0; j < dayMatrixRow.length; j++) {
                        var dayElem = dayMatrixRow[j];
                        var dayLbl = dayElem.dayLbl;
                        var numActivity = dayElem.numActivity;
                        var totHour = dayElem.totHour;
                        var isHoliday = dayElem.isHoliday;
                        
                        var dayCell = $("<td>&nbsp;</td>");
                        if (dayLbl.toString().replace(/^\s+|\s+$/gm, '') === '') {
                        } else {
                            var dayCellSpan = "";
                            var stile = "";
                            if(isHoliday){
                                stile="style='background: #1203;'";
                            }
                            if (numActivity > 0) {
                                dayCellSpan = "&nbsp;<span class='badge'>A:"+ numActivity +";H:"+totHour+ "</span>";
                            }
                            dayCell = $("<td "+stile+" ><a href='activityDayView.php' id='post-link' month='" + calendarData.currVis_month + "' year='" +  calendarData.currVis_year + "' day='"+dayLbl+"'>" + ('0' + dayLbl).slice(-2) + dayCellSpan + "</a></td>");
                            
                        }


                        dayRow.append(dayCell);
                    }
                    monthTable.append(dayRow);
                }

                $(uid).empty();
                $(uid).append(refPanelCalendar);

                $(uid).append(monthTable);

                $('a#post-link').click(function () {
                    $('body').append($('<form/>', {
                        id: 'formDayLink',
                        method: 'POST',
                        action: $(this).attr('href')
                    }));
               
                    $('#formDayLink').append($('<input/>', {
                        type: 'hidden',
                        name: 'month',
                        value: $(this).attr('month')
                    }));
                    $('#formDayLink').append($('<input/>', {
                        type: 'hidden',
                        name: 'year',
                        value: $(this).attr('year')
                    }));
                    $('#formDayLink').append($('<input/>', {
                        type: 'hidden',
                        name: 'day',
                        value: $(this).attr('day')
                    }));
//                    $('#formDayLink').append($('<input/>', {
//                        type: 'hidden',
//                        name: 'field2',
//                        value: 'bar'
//                    }));
                    $('#formDayLink').submit();
                    return false;
                });

            }

        }
    });


}

