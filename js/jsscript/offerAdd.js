$(document).ready(function () {
    var datavalue = {limit: -1};//codCustomer: codCustomerV, descrCustomer: descrCustomerV, submit: "Add"};
    $.ajax({
        type: "POST",
        url: "BE_customerList.php",
        data: datavalue,
        cache: false,
        dataType: "json",
//                  
        success: function (data) {
            var options = $("#codCustomer");
            //var totale=data.total;
            var righe = data.rows;
            if (data.total > 0) {
                $.each(righe, function () {
                    if (this !== null && this.isEnabled === 1) {
                        options.append($("<option />").val(this.codCust).html(this.descrCust));
                    }
                });
            }
            //---------------------------------------
            {
                var datavalue = {limit: -1};//codCustomer: codCustomerV, descrCustomer: descrCustomerV, submit: "Add"};
                $.ajax({
                    type: "POST",
                    url: "BE_officeList.php",
                    data: datavalue,
                    cache: false,
                    dataType: "json",
                    //                  
                    success: function (data) {
                        var options = $("#codOffice");
                        //var totale=data.total;
                        var righe = data.rows;
                        if (data.total > 0) {
                            $.each(righe, function () {
                                if (this !== null && this.isEnabled === 1) {
                                    options.append($("<option />").addClass(this.codCust).val(this.codOffice).html(this.descrOffice));
                                }
                            });
                        }
                        //---------------------------------------
                        {
                            var datavalue = {limit: -1};//codCustomer: codCustomerV, descrCustomer: descrCustomerV, submit: "Add"};
                            $.ajax({
                                type: "POST",
                                url: "BE_responsibleList.php",
                                data: datavalue,
                                cache: false,
                                dataType: "json",
                                //                  
                                success: function (data) {
                                    var options = $("#codResp");
                                    //var totale=data.total;
                                    var righe = data.rows;
                                    if (data.total > 0) {
                                        $.each(righe, function () {
                                            if (this !== null && this.isEnabled === 1) {
                                                options.append($("<option />").addClass(this.codOffice).val(this.codResp).html(this.descrResp));
                                            }
                                        });
                                    }


                                    //---------------------------------------
                                    $("#codOffice").chained("#codCustomer");
                                    $("#codResp").chained("#codOffice");
                                    //---------------------------------------
                                }

                            });
                        }
                        //---------------------------------------
                    }

                });
            }
            //---------------------------------------

        }

    });
});

$(document).ready(function () {

    $("#addOfferForm").submit(function (event) {
        console.log("Handler for .submit() called.");
        event.preventDefault();
        var codCustomerV = $('#codCustomer').val();
        var codOfficeV = $('#codOffice').val();
        var codRespV = $('#codResp').val();
        var codOfferV = $("#codOffer").val();
        var descrOfferV = $("#descrOffer").val();
        var hourV = $("#hour").val();
        var typeV=$('input[name=typeOffer]:checked', '#addOfferForm').val(); 
        
        
        

        // Checking for blank fields.
        if (codOfferV===''|| codCustomerV === '' || codOfficeV === '' || codRespV === '' || descrOfferV === ''||typeV==='') {

        } else {
            var datavalue = {codCustomer: codCustomerV, codOffice: codOfficeV, codResp: codRespV,codOffer:codOfferV,descrOffer:descrOfferV,hour:hourV,type:typeV, submit: "Add"};
            if ($.trim(codCustomerV).length > 0 && $.trim(codOfficeV).length > 0 && $.trim(codRespV).length > 0 && $.trim(descrOfferV).length > 0)
            {
                $.ajax({
                    type: "POST",
                    url: "BE_offerAdd.php",
                    data: datavalue,
                    cache: false,
                    dataType: "json",
//                  beforeSend: function(){ $("#login").val('Connecting...');},
                    success: function (data) {
                        if (data) {
                            console.log(data);
                            if (data["retCode"] === "R0002") {
                                $('#le-alert').bootAlert({class: "alert-success", warnTtl: "OK", warnMsg: "Offerta inserita", warnMsgCod: data["retCode"]});
                            } else {
                                $('#le-alert').bootAlert({class: "alert-warning", warnTtl: "Error", warnMsg: "Offerta NON inserita", warnMsgCause: "Errore di inserimento", warnMsgCod: data["retCode"]});
                            }
                        }
                    }
                });
            }
        }
        return false;
    });
    $('.close').click(function () {
        $(this).parent().removeClass('in'); // hides alert with Bootstrap CSS3 implem
    });
    $('input[type=text]').keyup(function () {
        $(this).val($(this).val().toUpperCase());
    });

    $("#codOffer").keyup(function () {


        var codCustomerV = $("#codCustomer").val();
        var codOfficeV = $("#codOffice").val();
        var codRespV = $("#codResp").val();
        var codOfferV = $("#codOffer").val();
        if (codOfferV === '') {
            $("#codOfferUsed").removeClass("glyphicon-ok");
            $("#codOfferUsed").addClass("glyphicon-remove");
            $("#codOfferUsed").css("color", "#FF0004");
        } else {
            var datavalue = {codOffer:codOfferV,codCustomer: codCustomerV, codOffice: codOfficeV, codResp: codRespV, submit: "Find"};
            if ($.trim(codCustomerV).length > 0 && $.trim(codOfficeV).length > 0 && $.trim(codRespV).length > 0) {
                $.ajax({
                    type: "POST",
                    url: "BE_offerExist.php",
                    data: datavalue,
                    cache: false,
                    dataType: "json",
                    //                  beforeSend: function(){ },
                    success: function (data) {
                        if (data) {
                            console.log("responsibleExist:");
                            console.log(data);
                            if (data["retCode"] == "R0004") {
                                $("#codOfferUsed").removeClass("glyphicon-remove");
                                $("#codOfferUsed").addClass("glyphicon-ok");
                                $("#codOfferUsed").css("color", "#00A41E");
                            }
                            if (data["retCode"] == "R0003") {
                                $("#codOfferUsed").removeClass("glyphicon-ok");
                                $("#codOfferUsed").addClass("glyphicon-remove");
                                $("#codOfferUsed").css("color", "#FF0004");
                            }
                        }
                    }
                });
            }
        }
    });



    $("#createUser").click(function () {
        if (this.checked) {
            if ($("#codUserUsed").hasClass("glyphicon-ok")) {
                $("#codUserUsed").css("color", "#00A41E");
            } else if ($("#codUserUsed").hasClass("glyphicon-remove")) {
                $("#codUserUsed").css("color", "#FF0004");
            }
            $("#codUserUsedLbl").css("color", "");
        } else {
            $("#codUserUsed").css("color", "#EEEEEE");
            $("#codUserUsedLbl").css("color", "#EEEEEE");
        }
    });


});


