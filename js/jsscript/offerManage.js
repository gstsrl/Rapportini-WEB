$(function () {
    $('#tableId').bootstrapTable(); // init via javascript

    $(window).resize(function () {
        $('#tableId').bootstrapTable('resetView');
    });
});

function actionFormatter(value, row, index) {

    isEnabled = Boolean(row.isEnabled);
    if (isEnabled)
        return [
            '<a class="lock ml10" href="javascript:void(0)" title="Lock">',
            '<i class="glyphicon glyphicon-lock"></i>',
            '</a>'
        ].join('');
}


window.actionEvents = {
    'click .lock': function (e, value, row, index) {
        var datavalue = {idCust: row.idCust, codCust: row.codCust, descrCust: row.descrCust, isEnabled: row.isEnabled};
        $.ajax({
            type: "POST",
            url: "BE_offerDisable.php",
            data: row,
            cache: false,
            dataType: "json",
            beforeSend: function () { },
            success: function (data) { }
            
        });
        $('#tableId').bootstrapTable('refresh');
        
        console.log(value, row, index);
    }

};


