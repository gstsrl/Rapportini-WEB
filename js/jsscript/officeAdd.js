$(document).ready(function () {
    var datavalue = {limit:-1};//codCustomer: codCustomerV, descrCustomer: descrCustomerV, submit: "Add"};
    $.ajax({
        type: "POST",
        url: "BE_customerList.php",
        data: datavalue,
        cache: false,
        dataType: "json",
//                  
        success: function (data) {
            var options = $("#codCustomer");
            //var totale=data.total;
            var righe=data.rows;
            if(data.total>0){
                $.each(righe, function () {
                    if(this !== null && this.isEnabled===1){
                        options.append($("<option />").val(this.codCust).html(this.descrCust));
                    }
                });
            }
        }

    });
});
$(document).ready(function () {
    $("#addOfficeForm").submit(function (event) {
        console.log("Handler for .submit() called.");
        event.preventDefault();
        var codCustomerV = $('#codCustomer').val();
        var codOfficeV = $('#codOffice').val();
        var descrOfficeV = $('#descrOffice').val();
        console.log(codCustomerV);
        console.log(codOfficeV);
        console.log(descrOfficeV);
        // Checking for blank fields.
        if (codCustomerV === '' || codOfficeV === '' || descrOfficeV=== '' ) {

        } else {
            var datavalue = {codCustomer: codCustomerV,codOffice:codOfficeV,descrOffice: descrOfficeV, submit: "Add"};
            if ($.trim(codCustomerV).length > 0 && $.trim(codOfficeV).length > 0 && $.trim(descrOfficeV).length > 0)
            {
                $.ajax({
                    type: "POST",
                    url: "BE_officeAdd.php",
                    data: datavalue,
                    cache: false,
                    dataType: "json",
//                  beforeSend: function(){ $("#login").val('Connecting...');},
                    success: function (data) {
                        if (data) {
                            console.log(data);
                            if (data["retCode"] === "R0002") {
                                $('#le-alert').bootAlert({class: "alert-success", warnTtl: "OK", warnMsg: "Office inserito", warnMsgCod: data["retCode"]});
                            } else {
                                $('#le-alert').bootAlert({class: "alert-warning", warnTtl: "Error", warnMsg: "Office NON inserito", warnMsgCause: "Errore di inserimento", warnMsgCod: data["retCode"]});
                            }
                        }
                    }
                });
            }
        }
        return false;
    });
    $('.close').click(function () {
        $(this).parent().removeClass('in'); // hides alert with Bootstrap CSS3 implem
    });
    $('input[type=text]').keyup(function () {
        $(this).val($(this).val().toUpperCase());
    });
   
    $("#codOffice").keyup(function () {


        var codCustomerV = $("#codCustomer").val();
        var codOfficeV = $("#codOffice").val();
        if (codOfficeV === '') {
            $("#codOfficeUsed").removeClass("glyphicon-ok");
            $("#codOfficeUsed").addClass("glyphicon-remove");
            $("#codOfficeUsed").css("color", "#FF0004");
        } else {
            var datavalue = {codCustomer: codCustomerV,codOffice:codOfficeV, submit: "Find"};
            if ($.trim(codCustomerV).length > 0 && $.trim(codOfficeV).length > 0){
                $.ajax({
                    type: "POST",
                    url: "BE_officeExist.php",
                    data: datavalue,
                    cache: false,
                    dataType: "json",
//                  beforeSend: function(){ },
                    success: function (data) {
                        if (data) {
                            console.log(data);
                            if (data["retCode"] == "R0004") {
                                $("#codOfficeUsed").removeClass("glyphicon-remove");
                                $("#codOfficeUsed").addClass("glyphicon-ok");
                                $("#codOfficeUsed").css("color", "#00A41E");
                            }
                            if (data["retCode"] == "R0003") {
                                $("#codOfficeUsed").removeClass("glyphicon-ok");
                                $("#codOfficeUsed").addClass("glyphicon-remove");
                                $("#codOfficeUsed").css("color", "#FF0004");
                            }
                        }
                    }
                });
            }
        }
    });
});


