$(document).ready(function () {
    $('#period').datepicker({
        format: "yyyy-mm",
        minViewMode: 1,
        todayBtn: "linked",
        clearBtn: true,
        language: "it",
        autoclose: true
    })
            .on('changeDate', function (e) {
                checkActivityPresence();
            });
    $("#activityPresence").removeClass("glyphicon-ok");
    $("#activityPresence").removeClass("glyphicon-remove");
    //$("#activityPresence").addClass("glyphicon-ok");
    //$("#activityPresence").css("color", "#00A41E");
    $("#activityPresence").addClass("glyphicon-remove");
    $("#activityPresence").css("color", "#FF0004");





});


//Imposto 

$(document).ready(function () {
    var datavalue = {limit: -1};
    $.ajax({
        type: "POST",
        url: "BE_userList.php",
        data: datavalue,
        cache: false,
        dataType: "json",
////                  
        success: function (data) {
            var options = $("#codUser");
            //var totale=data.total;
            var righe = data.rows;
            if (data.total > 0) {
                $.each(righe, function () {
                    if (this !== null && this.isEnabled === 1 && isUserOK(this.codUser)) {
                        options.append($("<option />").val(this.codUser).html(this.nome+" "+this.cognome));
                    }
                });
            }
        }
    });


    $("#codUser").on('change', function (e) {
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;
        checkActivityPresence();
    });

});

function checkActivityPresence() {

    var x1 = $('#period').val();
    var x2 = $('#codUser option:selected').val();
    var x3=x1.split("-");
    var y=parseInt(x3[0]);
    var m=parseInt(x3[1]);
    var datavalue = {year: y, month: m, user: x2};
    $.ajax({
        type: "POST",
        url: "BE_activityListMonthly.php",
        data: datavalue,
        cache: false,
        dataType: "json",
////                  
        success: function (data) {
            if (data.total > 0) {
                $("#activityPresence").removeClass("glyphicon-remove");
                $("#activityPresence").removeClass("glyphicon-ok");
                $("#activityPresence").addClass("glyphicon-ok");
                $("#activityPresence").css("color", "#00A41E");
                retriveCombinations(datavalue);




            } else {
                $("#activityPresence").removeClass("glyphicon-remove");
                $("#activityPresence").removeClass("glyphicon-ok");
                $("#activityPresence").addClass("glyphicon-remove");
                $("#activityPresence").css("color", "#FF0004");


                var optionsCust = $("#codCustomer");
                var optionsOff = $("#codOffice");
                var optionsResp = $("#codResp");
                optionsCust.empty();
                optionsOff.empty();
                optionsResp.empty();
            }

        }
    });

    function   retriveCombinations(datavalue) {
        $.ajax({
            type: "POST",
            url: "BE_reportGenerationCombination.php",
            data: datavalue,
            cache: false,
            dataType: "json",
////                  
            success: function (data) {
                var optionsCust = $("#codCustomer");
                var optionsOff = $("#codOffice");
                var optionsResp = $("#codResp");
                optionsCust.empty();
                optionsOff.empty();
                optionsResp.empty();
                //var totale=data.total;
                var righe = data.rows;
                if (data.total > 0) {
                    var listaCust = {};
                    var listaOffices = {};
                    var listaResp = {};
                    $.each(righe, function () {
                        if (this !== null) {
                            var uid = ''.concat(this.codCust);
                            if (!(uid in listaCust)) {
                                listaCust[uid] = uid;
                                optionsCust.append($("<option />").val(this.codCust).html(this.descrCust));
                            }
                            var uid = ''.concat(this.codOffice);
                            if (!(uid in listaOffices)) {
                                listaOffices[uid] = uid;
                                optionsOff.append($("<option />").addClass(this.codCust).val(this.codOffice).html(this.descrOffice));
                            }
                            var uid = ''.concat(this.codResp);
                            if (!(uid in listaResp)) {
                                listaResp[uid] = uid;
                                //optionsResp.append($("<option />").addClass(this.codOffice.concat('\\',this.codCust)).val(this.codResp).html(this.descrResp));
                                optionsResp.append($("<option />").addClass(this.codOffice).val(this.codResp).html(this.descrResp));
                            }
                        }
                    });
                    $("#codOffice").chained("#codCustomer");
                    $("#codResp").chained("#codOffice");
                    //$("#codResp").chained("#codOffice, #codCustomer");
                } else {

                }
            }
        });
    }
   
}


$(document).ready(function () {
    $('.close').click(function () {
        $(this).parent().removeClass('in'); // hides alert with Bootstrap CSS3 implem
    });
    $('input[type=text]').keyup(function () {
        $(this).val($(this).val().toUpperCase());
    });

});


$(document).ready(function () {
    $("#reportGenerationForm").submit(function (event) {
        console.log("Handler for .submit() called.");
        event.preventDefault();
        var periodV = $('#period').val();
        var codUserV = $('#codUser').val();
        var codCustomerV = $('#codCustomer').val();
        var codOfficeV = $('#codOffice').val();
        var codRespV = $('#codResp').val();

        console.log(periodV);
        console.log(codUserV);
        console.log(codCustomerV);
        console.log(codOfficeV);
        console.log(codRespV);

        if (periodV === '' || codUserV === '' || codCustomerV === '' || codOfficeV === '' || codRespV === '') {

        } else {
            var datavalue = {period: periodV, codUser: codUserV, codCustomer: codCustomerV, codOffice: codOfficeV, codResp: codRespV, submit: "Report"};
            if ($.trim(periodV).length > 0 && $.trim(codUserV).length > 0 && $.trim(codCustomerV).length > 0 && $.trim(codOfficeV).length > 0 && $.trim(codRespV).length > 0)
            {
                var str = jQuery.param( datavalue );
                window.open('reportPDF.php?'+str);
            }
        }




       
        
    


//        // Checking for blank fields.
//        if (periodV === '' || codUserV === '' || codCustomerV === '' || codOfficeV === '' || codRespV === '') {
//
//        } else {
//            var datavalue = {period: periodV, codUser: codUserV, codCustomer: codCustomerV, codOffice: codOfficeV, codResp: codRespV, submit: "Report"};
//            if ($.trim(periodV).length > 0 && $.trim(codUserV).length > 0 && $.trim(codCustomerV).length > 0 && $.trim(codOfficeV).length > 0 && $.trim(codRespV).length > 0)
//            {
//                $.ajax({
//                    type: "POST",
//                    url: "newEmptyPHP.php",
//                    data: datavalue,
//                    cache: false,
////                    dataType: "json",
//                    success: function(data){
//                        var win = window.open();
//                        win.document.write(data);
//                    }
////                    beforeSend: function(){ $("#login").val('Connecting...');},
////                    success: function (data) {
////                        if (data) {
////                            console.log(data);
////                            if (data["retCode"] === "R0002") {
////                                $('#le-alert').bootAlert({class: "alert-success", warnTtl: "OK", warnMsg: "Utente inserito", warnMsgCod: data["retCode"]});
////                            } else if (data["retCode"] === "E0004") {
////                                $('#le-alert').bootAlert({class: "alert-warning", warnTtl: "Error", warnMsg: "Utente NON inserito", warnMsgCause: "Utente esistente", warnMsgCod: data["retCode"]});
////                            } else if (data["retCode"] === "E0005") {
////                                $('#le-alert').bootAlert({class: "alert-warning", warnTtl: "Error", warnMsg: "Utente NON inserito", warnMsgCause: "Password non coincidenti", warnMsgCod: data["retCode"]});
////                            } else if (data["retCode"] === "E0006") {
////                                $('#le-alert').bootAlert({class: "alert-warning", warnTtl: "Error", warnMsg: "Utente NON inserito", warnMsgCause: "Errore di inserimento", warnMsgCod: data["retCode"]});
////                            }
////                        }
////                    }
//                });
//            }
//        }
        return false;
    });
});