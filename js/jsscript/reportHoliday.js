$(document).ready(function () {
    
    var df = new Date();
    var dt = new Date();
    //var yearf = df.getFullYear();
    //var monthf= df.getMonth();
    //var dayf= df.getDate();
    
    //var yeart = dt.getFullYear();
    //var montht= dt.getMonth();
    //var dayt= dt.getDate();
    
    df.setMonth(0);
    df.setDate(1);
    dt.setMonth(11);
    dt.setDate(31);
    
    //http://stevenlevithan.com/assets/misc/date.format.js
    var datestringf = (df.getFullYear() + "-" + ("0"+(df.getMonth()+1)).slice(-2) + "-" + ("0"+(df.getDate())).slice(-2));
    var datestringt = (dt.getFullYear() + "-" + ("0"+(dt.getMonth()+1)).slice(-2) + "-" + ("0"+(dt.getDate())).slice(-2));
    
    
    $('#start').val(datestringf);
    $('#end').val(datestringt);
   
    
    $('#sandbox-container .input-daterange').datepicker({
        format: "yyyy-mm-dd",
        //startDate: df,
        //endDate: dt,
    }).on('changeDate', function (e) {
                 getData();
    });
    
     $('#typeOffer:checked').each(function() {
        $(this).change(function(event) {
            getData();
        });
    });
    
                 
    $('#codUser').multiselect({
        includeSelectAllOption: true
    });
    
    
    getData();
      
    function getData(){
        $("#codUser").empty();
        
        var selected = [];
        $('#typeOffer:checked').each(function() {
            selected.push($(this).val());
        });
        
        var datavalue = {from: $('#start').val(), to:$('#end').val(),types:selected };
        $.ajax({
            type: "POST",
            url: "BE_userOnActivity.php",
            data: datavalue,
            cache: false,
            dataType: "json",
            success: function (data) {
                var options = $("#codUser");
                var totale=data.total;
                var righe = data.rows;
                if (data.total > 0) {
                    $.each(righe, function () {
                       //if (this !== null && this.isEnabled === 1 && isUserOK(this.codUser)) {
                            options.append($("<option />").val(this.codUser).html(this.nome+" "+this.cognome).attr('selected','selected'));
                       // }
                    });
                }
                $('#codUser').multiselect('rebuild');
            }
            
        });
        return true;
    }
});

$(document).ready(function () {
    $("#reportHolidayForm").submit(function (event) {
        console.log("Handler for .submit() called.");
        event.preventDefault();
        var fromV = $('#start').val();
        var toV = $('#end').val();
        var codUserV = $('#codUser').val();
        
        var selected = [];
        $('#typeOffer:checked').each(function() {
            selected.push($(this).val());
        });
        
        console.log(fromV);
        console.log(toV);
        console.log(codUserV);
        

        if (fromV === '' || toV === '' || codUserV === '' ) {

        } else {
            var datavalue = {from: fromV, to: toV,types:selected, codUser: codUserV, submit: "Report"};
            if ($.trim(fromV).length > 0 && $.trim(toV).length > 0 && $.trim(codUserV).length > 0 )
            {
                var str = jQuery.param( datavalue );
                window.open('feriePDF.php?'+str);
            }
        }
        
        return false;
    });
});
