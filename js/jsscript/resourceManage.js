$(function () {
    $('#tableId').bootstrapTable(); // init via javascript

    $(window).resize(function () {
        $('#tableId').bootstrapTable('resetView');
    });
});

function actionFormatter(value, row, index) {

    isEnabled = Boolean(row.isEnabled);
    if (!isEnabled){
        return [
            '<a class="unlock ml10" href="javascript:void(0)" title="UnlockLock">',
            '<i class="glyphicon glyphicon-ok-sign"></i>',
            '</a>'
        ].join('');
    }else{
        return [
            '<a class="lock ml10" href="javascript:void(0)" title="Lock">',
            '<i class="glyphicon glyphicon-remove-sign"></i>',
            '</a>'
        ].join('');
    }
}


window.actionEvents = {
    'click .lock': function (e, value, row, index) {
        var datavalue = {idResource: row.idUser, codResource: row.codResp};
        $.ajax({
            type: "POST",
            url: "BE_resourceDisable.php",
            data: row,
            cache: false,
            dataType: "json",
            beforeSend: function () { },
            success: function (data) { }
            
        });
        $('#tableId').bootstrapTable('refresh');
        console.log(value, row, index);
    },    
    'click .unlock': function (e, value, row, index) {
        var datavalue = {idResource: row.idUser, codResource: row.codResp};
        $.ajax({
            type: "POST",
            url: "BE_resourceEnable.php",
            data: row,
            cache: false,
            dataType: "json",
            beforeSend: function () { },
            success: function (data) { }
            
        });
        $('#tableId').bootstrapTable('refresh');
        
        console.log(value, row, index);
    }
   
};


