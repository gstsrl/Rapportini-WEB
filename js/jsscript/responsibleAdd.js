$(document).ready(function () {
    var datavalue = {limit:-1};//codCustomer: codCustomerV, descrCustomer: descrCustomerV, submit: "Add"};
    $.ajax({
        type: "POST",
        url: "BE_customerList.php",
        data: datavalue,
        cache: false,
        dataType: "json",
//                  
        success: function (data) {
            var options = $("#codCustomer");
            //var totale=data.total;
            var righe = data.rows;
            if (data.total > 0) {
                $.each(righe, function () {
                    if (this !== null && this.isEnabled===1) {
                        options.append($("<option />").val(this.codCust).html(this.descrCust));
                    }
                });
            }
            //---------------------------------------
            {
                var datavalue = {limit:-1};//codCustomer: codCustomerV, descrCustomer: descrCustomerV, submit: "Add"};
                $.ajax({
                    type: "POST",
                    url: "BE_officeList.php",
                    data: datavalue,
                    cache: false,
                    dataType: "json",
                    //                  
                    success: function (data) {
                        var options = $("#codOffice");
                        //var totale=data.total;
                        var righe = data.rows;
                        if (data.total > 0) {
                            $.each(righe, function () {
                                if (this !== null && this.isEnabled===1) {
                                    options.append($("<option />").addClass(this.codCust).val(this.codOffice).html(this.descrOffice));
                                }
                            });
                        }
                        //---------------------------------------
                        $("#codOffice").chained("#codCustomer");
                        //---------------------------------------
                    }

                });
            }
            //---------------------------------------

        }

    });
});

$(document).ready(function () {

    $("#addResponsibleForm").submit(function (event) {
        console.log("Handler for .submit() called.");
        event.preventDefault();
        var codCustomerV = $('#codCustomer').val();
        var codOfficeV = $('#codOffice').val();
        var codRespV = $('#codResp').val();
        var descrRespV = $('#descrResp').val();
        var createUserV = $('#createUser').prop('checked');
        console.log(codCustomerV);
        console.log(codOfficeV);
        console.log(codRespV);
        console.log(descrRespV);
        // Checking for blank fields.
        if (codCustomerV === '' || codOfficeV === '' || codRespV === '' || descrRespV === '') {

        } else {
            var datavalue = {codCustomer: codCustomerV, codOffice: codOfficeV, codResp: codRespV, descrResp: descrRespV, createUser: createUserV, submit: "Add"};
            if ($.trim(codCustomerV).length > 0 && $.trim(codOfficeV).length > 0 && $.trim(codRespV).length > 0 && $.trim(descrRespV).length > 0)
            {
                $.ajax({
                    type: "POST",
                    url: "BE_responsibleUserAdd.php",
                    data: datavalue,
                    cache: false,
                    dataType: "json",
//                  beforeSend: function(){ $("#login").val('Connecting...');},
                    success: function (data) {
                        if (data) {
                            console.log(data);
                            if (data["retCode"] === "R0002") {
                                $('#le-alert').bootAlert({class: "alert-success", warnTtl: "OK", warnMsg: "Responsible inserito", warnMsgCod: data["retCode"]});
                            } else {
                                $('#le-alert').bootAlert({class: "alert-warning", warnTtl: "Error", warnMsg: "Responsible NON inserito", warnMsgCause: "Errore di inserimento", warnMsgCod: data["retCode"]});
                            }
                        }
                    }
                });
            }
        }
        return false;
    });
    $('.close').click(function () {
        $(this).parent().removeClass('in'); // hides alert with Bootstrap CSS3 implem
    });
    $('input[type=text]').keyup(function () {
        $(this).val($(this).val().toUpperCase());
    });

    $("#codResp").keyup(function () {


        var codCustomerV = $("#codCustomer").val();
        var codOfficeV = $("#codOffice").val();
        var codRespV = $("#codResp").val();
        if (codRespV === '') {
            $("#codRespUsed").removeClass("glyphicon-ok");
            $("#codRespUsed").addClass("glyphicon-remove");
            $("#codRespUsed").css("color", "#FF0004");
        } else {
            var datavalue = {codCustomer: codCustomerV, codOffice: codOfficeV, codResp: codRespV, submit: "Find"};
            if ($.trim(codCustomerV).length > 0 && $.trim(codOfficeV).length > 0 && $.trim(codRespV).length > 0) {
                $.ajax({
                    type: "POST",
                    url: "BE_responsibleUserExist.php",
                    data: datavalue,
                    cache: false,
                    dataType: "json",
                    //                  beforeSend: function(){ },
                    success: function (data) {
                        if (data) {
                            console.log("responsibleExist:");
                            console.log(data);
                            if (data["retCode"] == "R0004") {
                                $("#codRespUsed").removeClass("glyphicon-remove");
                                $("#codRespUsed").addClass("glyphicon-ok");
                                $("#codRespUsed").css("color", "#00A41E");
                            }
                            if (data["retCode"] == "R0003") {
                                $("#codRespUsed").removeClass("glyphicon-ok");
                                $("#codRespUsed").addClass("glyphicon-remove");
                                $("#codRespUsed").css("color", "#FF0004");
                            }
                        }
                    }
                });
            }
        }
    });

    

    $("#createUser").click(function () {
        if (this.checked) {
            if ($("#codUserUsed").hasClass("glyphicon-ok")) {
                $("#codUserUsed").css("color", "#00A41E");
            } else if ($("#codUserUsed").hasClass("glyphicon-remove")) {
                $("#codUserUsed").css("color", "#FF0004");
            }
            $("#codUserUsedLbl").css("color", "");
        } else {
            $("#codUserUsed").css("color", "#EEEEEE");
            $("#codUserUsedLbl").css("color", "#EEEEEE");
        }
    });


});


