$(document).ready(function () {
    $("#addUserForm").submit(function (event) {
        console.log("Handler for .submit() called.");
        event.preventDefault();
        var usernameV = $('#username').val();
        var passV = $('#pass').val();
        var pass2V = $('#pass2').val();
        console.log(usernameV);
        console.log(passV);
        console.log(pass2V);
        var isAdminV = $('#isAdmin').prop('checked');
        var isUserV = $('#isUser').prop('checked');
        var isAccountV = $('#isAccount').prop('checked');

        console.log(isAdminV);
        console.log(isUserV);
        console.log(isAccountV);

        // Checking for blank fields.
        if (usernameV === '' || passV === '' || pass2V === ''
                || ($("#pass").val() !== $("#pass2").val())) {

        } else {
            var datavalue = {username: usernameV, pass: passV, pass2: pass2V, isAdmin: isAdminV, isUser: isUserV, isAccount: isAccountV, submit: "Add"};
            if ($.trim(usernameV).length > 0 && $.trim(passV).length > 0 && $.trim(isAdminV).length > 0 && $.trim(isUserV).length > 0 && $.trim(isAccountV).length > 0)
            {
                $.ajax({
                    type: "POST",
                    url: "BE_userAdd.php",
                    data: datavalue,
                    cache: false,
                    dataType: "json",
//                    beforeSend: function(){ $("#login").val('Connecting...');},
                    success: function (data) {
                        if (data) {
                            console.log(data);
                            if (data["retCode"] === "R0002") {
                                $('#le-alert').bootAlert({class: "alert-success", warnTtl: "OK", warnMsg: "Utente inserito", warnMsgCod: data["retCode"]});
                            } else if (data["retCode"] === "E0004") {
                                $('#le-alert').bootAlert({class: "alert-warning", warnTtl: "Error", warnMsg: "Utente NON inserito", warnMsgCause: "Utente esistente", warnMsgCod: data["retCode"]});
                            } else if (data["retCode"] === "E0005") {
                                $('#le-alert').bootAlert({class: "alert-warning", warnTtl: "Error", warnMsg: "Utente NON inserito", warnMsgCause: "Password non coincidenti", warnMsgCod: data["retCode"]});
                            } else if (data["retCode"] === "E0006") {
                                $('#le-alert').bootAlert({class: "alert-warning", warnTtl: "Error", warnMsg: "Utente NON inserito", warnMsgCause: "Errore di inserimento", warnMsgCod: data["retCode"]});
                            }
                        }
                    }
                });
            }
        }
        return false;
    });
    $('.close').click(function () {
        $(this).parent().removeClass('in'); // hides alert with Bootstrap CSS3 implem
    });

    $("#username").keyup(function () {


        var codUserV = $("#username").val();
        if (codUserV === '') {
            $("#codUserUsed").removeClass("glyphicon-ok");
            $("#codUserUsed").addClass("glyphicon-remove");
            $("#codUserUsed").css("color", "#FF0004");
        } else {
            var datavalue = {codUser: codUserV, submit: "Find"};
            if ($.trim(codUserV).length > 0)
            {
                $.ajax({
                    type: "POST",
                    url: "BE_userExist.php",
                    data: datavalue,
                    cache: false,
                    dataType: "json",
                    success: function (data) {
                        if (data) {
                            console.log(data);
                            if (data["retCode"] == "R0004") {
                                $("#codUserUsed").removeClass("glyphicon-remove");
                                $("#codUserUsed").addClass("glyphicon-ok");
                                $("#codUserUsed").css("color", "#00A41E");
                            }else if (data["retCode"] == "R0003") {
                                $("#codUserUsed").removeClass("glyphicon-ok");
                                $("#codUserUsed").addClass("glyphicon-remove");
                                $("#codUserUsed").css("color", "#FF0004");
                            }
                        }
                    }
                });
            }
        }
    });




    $("input[type=password]").keyup(function () {
        if ($("#pass").val() === $("#pass2").val()) {
            $("#pwmatch").removeClass("glyphicon-remove");
            $("#pwmatch").addClass("glyphicon-ok");
            $("#pwmatch").css("color", "#00A41E");
        } else {
            $("#pwmatch").removeClass("glyphicon-ok");
            $("#pwmatch").addClass("glyphicon-remove");
            $("#pwmatch").css("color", "#FF0004");
        }

    });
});