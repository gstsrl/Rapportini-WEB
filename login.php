<?php
?>
<!DOCTYPE html>
<!--[if IE 8]><html class="no-js lt-ie9" lang="en" ><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" ><!--<![endif]-->
    <html>
        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">

            <title>Login Rapportini</title>
            <!-- Fogli di stile -->
            <link href="css/bootstrap.css" rel="stylesheet" media="screen">
            <link href="css/stili-custom.css" rel="stylesheet" media="screen">
            <!-- Modernizr -->
            <script src="js/modernizr.custom.js"></script>
            <!-- respond.js per IE8 -->
            <!--[if lt IE 9]>
            <script src="js/respond.min.js"></script>
            <![endif]-->
        </head>
        <body>
            <div class="wrapper ">
                <div class="container">
                    <div class="row vertical-center-row">
                        <div class="col-md-4 col-md-offset-4">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 id="PleaseLogin" class="panel-title" data-i18n="login.pleaseLoginMsg"></h3>
                                </div>
                                <div class="panel-body">
                                    <form id="loginForm" accept-charset="UTF-8" role="form" >
                                        <fieldset>
                                            <div class="form-group">
                                                <input autocomplete="off" auto-capitalization="off" autocorrect="off" id="username" class="form-control" data-i18n="[placeholder]login.usernameMsg;" name="username" type="text" required="">
                                            </div>
                                            <div class="form-group">
                                                <input autocomplete="off" auto-capitalization="off" autocorrect="off" id="password" class="form-control" data-i18n="[placeholder]login.passwordMsg;" name="password" type="password" value="" required="">
                                            </div>
                                            <!--<div class="checkbox">
                                                <label>
                                                    <input name="remember" type="checkbox" value="Remember Me"> Remember Me
                                                </label>
                                            </div>-->
                                            <!--<button id="login" class="btn btn-lg btn-success btn-block" type="submit" data-i18n="login.loginMsg"></button>-->
                                            <button id="login" class="btn btn-lg btn-success btn-block"  data-i18n="login.loginMsg"></button>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                       
                        <div class="col-md-4 col-md-offset-4" >
                            <div id="le-alert" class="alert alert-warning alert-block fade" role="alert">
                                <button type="button" class="close">&times;</button>
                                <h4 id="warnTtlMsg" data-i18n="login.warnTtlMsg;"></h4>
                                <p id="warnMsg" data-i18n="login.warnMsg;"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
            <!-- jQuery e plugin JavaScript  -->
            <script src="js/jquery-2.1.3.min.js"></script>
            <script src="js/i18next-1.7.7.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script src="js/translation/loginT.js"></script>
            <script src="js/jsscript/login.js"></script>
            
            
        </body>
    </html>