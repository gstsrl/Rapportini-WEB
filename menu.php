<?php
require_once './SYS_loginClass.php';
require_once './SYS_paramClass.php';
require_once './SYS_mysqliConnClass.php';
require_once './SYS_queryClass.php';

$parametri= new Params();
$mysqlConn= new mysqliConnClass($parametri);
$mysqliConn = $mysqlConn->connect();
$authCHECK_LOGIN = loginClass::checkUser2($mysqliConn);
if ($authCHECK_LOGIN) {
  
    $usernameCHECK_ROLES =loginClass::getUidUser();
    $res=queryClass::checkUserRoleOnDb($mysqliConn, $usernameCHECK_ROLES);
    $mysqlConn->disconnect();
?>
 <!DOCTYPE html>
<!--[if IE 8]><html class="no-js lt-ie9" lang="en" ><![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" ><!--<![endif]-->
    <html>
        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Menu</title>
            <!-- Fogli di stile -->
            <link href="css/bootstrap.css" rel="stylesheet" media="screen">
            <link href="css/stili-custom.css" rel="stylesheet" media="screen">
            <!-- Modernizr -->
            <script src="js/modernizr.custom.js"></script>
            <!-- respond.js per IE8 -->
            <!--[if lt IE 9]>
            <script src="js/respond.min.js"></script>
            <![endif]-->
        </head>
        <body>
             <nav class="navbar navbar-default navbar-static-top">
                    <div class="container">
                        <ol class="breadcrumb">
                            <li class="active">Menu</li>
                        </ol>
                    </div>
                </nav>
            <!--<div class="wrapper ">-->
                <div class="container">
                   
                    <div class="row vertical-center-row">   
    
    
<?php    
    if ($res['isEnabledCHECK_ROLES']) {
        if ($res['isAdminCHECK_ROLES']) {
?>                        
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">Admin Area</div>
                                <div class="panel-body">
                                    <p><span class="glyphicon glyphicon-user" aria-hidden="true"></span><a href=userAdd.php>Click Here to Register a new User</a></p>
                                    <p><span class="glyphicon glyphicon-user" aria-hidden="true"></span><a href=resourceManage.php>Click Here to Register a new Resource</a></p>
                                    <p><span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span><a href=customerManage.php>Click Here to Register a Customer</a></p>
                                    <p><span class="glyphicon glyphicon-home" aria-hidden="true"></span><a href=officeManage.php>Click Here to Register a Office</a></p>
                                    <p><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span><a href=responsibleManage.php>Click Here to Register a Responsible</a></p>
                                    <p><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span><a href=offerManage.php>Click Here to Register a Offer</a></p>
                                </div>
                            </div>

                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">User Area</div>
                                <div class="panel-body">
                                    <p><span class="glyphicon glyphicon-time" aria-hidden="true"></span><a href=reportGeneration.php>Report</a></p>
                                    <p><span class="glyphicon glyphicon-glass" aria-hidden="true"></span><a href=reportHoliday.php>Holiday</a></p>
                                </div>
                            </div>
                        </div>
                        
<?php
        }else if ($res['isUserCHECK_ROLES']) {
?>                        
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">User Area</div>
                                <div class="panel-body">
                                    <p><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span><a href=activtyMonthView.php>Monthly activities</a></p>
                                    <p><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span><a href=activityDayView.php>List Today activities</a></p>
                                    <p><span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span><a href=activityDayAdd.php>Add Today activities</a></p>
                                    <p><span class="glyphicon glyphicon-time" aria-hidden="true"></span><a href=reportGeneration.php>Report</a></p>
                                    <p><span class="glyphicon glyphicon-glass" aria-hidden="true"></span><a href=reportHoliday.php>Holiday</a></p>
                                </div>
                            </div>
                        </div>
<?php
        }
        if ($res['isRespCHECK_ROLES']) {
?>                      
            

<?php
        }
?> 
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading"><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>Personal Area</div>
                                <div class="panel-body">
                                    <p><span class="glyphicon glyphicon-cog" aria-hidden="true"></span><a href=changePSW.php>Change Password</a></p>
                                    <p><span class="glyphicon glyphicon-off" aria-hidden="true"></span><a href=logout.php>Logout</a></p>
                                </div>
                            </div>
                        </div>
<?php
    } else {
?>                    
                        <p>Utente non abilitato</p>    
<?php
    }
?>                
                    </div>
                </div>
            <!-- jQuery e plugin JavaScript  -->
            <script src="js/jquery-2.1.3.min.js"></script>
            <script src="js/i18next-1.7.7.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
<!--            <script src="js/tranlation/login.js"></script>
            <script src="js/jsscript/login.js"></script>-->
        </body>
    </html>         
                        
                        
 

<?php
    
} else {
    $mysqlConn->disconnect();
    header("Location: logout.php");
}
?>                 