<?php
require_once './SYS_loginClass.php';
require_once './SYS_paramClass.php';
require_once './SYS_mysqliConnClass.php';
require_once './SYS_queryClass.php';

$parametri = new Params();
$mysqlConn = new mysqliConnClass($parametri);
$mysqliConn = $mysqlConn->connect();
$authCHECK_LOGIN = loginClass::checkUser2($mysqliConn);
if ($authCHECK_LOGIN) {

    $usernameCHECK_ROLES = loginClass::getUidUser();
    $res = queryClass::checkUserRoleOnDb($mysqliConn, $usernameCHECK_ROLES);
    $mysqlConn->disconnect();
    ?>
    <!DOCTYPE html>
    <!--[if IE 8]><html class="no-js lt-ie9" lang="en" ><![endif]-->
    <!--[if gt IE 8]><!--><html class="no-js" ><!--<![endif]-->
        <html>
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">

                <title>Gestione Offerte</title>
                <!-- Fogli di stile -->
                <link href="css/bootstrap.css" rel="stylesheet" media="screen">
                <link href="css/bootstrap-table.min.css" rel="stylesheet" media="screen">
                <link href="css/stili-custom.css" rel="stylesheet" media="screen">
                <!-- Modernizr -->
                <script src="js/modernizr.custom.js"></script>
                <!-- respond.js per IE8 -->
                <!--[if lt IE 9]>
                <script src="js/respond.min.js"></script>
                <![endif]-->
            </head>
            <body>
                <nav class="navbar navbar-default navbar-static-top">
                    <div class="container">
                        <ol class="breadcrumb">
                            <li><a href="menu.php"> Menu</a></li>
                            <li class="active">Gestione Offerte</li>
                        </ol>
                    </div>
                </nav>
                <!--<div class="wrapper ">-->
                <div class="container">

                    <div class="row vertical-center-row">   


                        <?php
                        if ($res['isEnabledCHECK_ROLES'] && $res['isAdminCHECK_ROLES']) {
                            ?>         

                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">Offerte List</div>
                                    <div class="panel-body">
                                        <div id="toolbar" class="btn-group">
                                            <a class="btn btn-default" href="offerAdd.php"><i class="glyphicon glyphicon-plus"></i>Add Offer</a>
                                        </div>
                                        <table id="tableId"
                                            data-toggle="table"
                                               data-url="./BE_offerList.php"
                                               data-method="post"
                                               data-content-type="application/x-www-form-urlencoded"
                                               data-pagination="true"
                                               data-side-pagination="server"
                                               data-page-size="4"
                                               data-page-list="[2,4,5, 10, 20, 50, 100, 200]"
                                               data-search="true"
                                               data-show-refresh="true"
                                               data-show-toggle="true"
                                               data-show-columns="true"
                                               data-height="300"
                                               data-toolbar="#toolbar">
                                            <thead>
                                                <tr>
                                                    <th data-field="codOffer"    data-align="center" data-sortable="true">codOffer</th>
                                                    <th data-field="descrOffer"  data-align="left" data-sortable="true">descrOffer</th>
                                                    <th data-field="hour"  data-align="left" data-sortable="true">hour</th>
                                                    <th data-field="descrResp"  data-align="left" data-sortable="true">descrResp</th>
                                                    <th data-field="descrOffice"  data-align="left" data-sortable="true">descrOffice</th>
                                                    <th data-field="descrCust"  data-align="left" data-sortable="true">descrCust</th>
                                                    <th data-field="action"     data-align="center" data-formatter="actionFormatter" data-events="actionEvents">Action</th>
                                                </tr>
                                            </thead>
                                        </table>

                                      


                                    </div>
                                </div>
                            </div>
                            
                            <?php
                        } else {
                            ?>                    
                            <p>Utente non abilitato</p>    
                            <?php
                        }
                        ?>                
                    </div>
                </div>
                <!-- jQuery e plugin JavaScript  -->
                <script src="js/jquery-2.1.3.min.js"></script>
                <script src="js/i18next-1.7.7.min.js"></script>
                <script src="js/bootstrap.min.js"></script>
                <script src="js/bootstrap-table.min.js"></script>

                <script src="js/translation/offerManageT.js"></script>
                <script src="js/jsscript/offerManage.js"></script>
            </body>
        </html>         
        <?php
    } else {
        $mysqlConn->disconnect();
        header("Location: logout.php");
    }
    ?>


