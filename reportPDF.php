<?php
require_once './SYS_validatingPostFunction.php';
require_once './SYS_validatingGetFunction.php';
require_once './SYS_paramClass.php';
require_once './SYS_mysqliConnClass.php';
require_once './SYS_queryClass.php';
require_once './pdf/FPDF/fpdf.php';

class PDF extends FPDF {

    
    // Page header
    function Header() {
        // Logo
        $this->Image('GST_logo.jpg', 170, 3, 30);
        // Arial bold 15
        $this->SetFont('Arial', 'B', 15);
        // Background color
        $this->SetTextColor(0, 0, 0);
        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(230, 230, 230);
        // Title
        $title = 'RAPPORTO RIEPILOGATIVO ATTIVIT' . iconv('UTF-8', 'windows-1252', html_entity_decode("&Agrave;")) . ' MENSILE';
        //$w = $this->GetStringWidth($title)+6;
        //$this->SetX((210-$w)/2);
        $this->SetY(15);
        $w = 0;
        $this->Cell($w, 9, $title, 1, 1, 'C', true);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell(0, 6, 'File: Mass1.1.odt', 0, 1, 'L');
        // Line break

        $this->Ln(0);
        $this->SetFillColor(255, 255, 255);
        $this->Cell(15, 6, 'MESE:', 'LTB', 0, 'L', true);
        $this->Cell(55, 6, $this->month, 'TB', 0, 'L', true);
        $this->Cell(15, 6, 'ANNO:', 'TB', 0, 'L', true);
        $this->Cell(55, 6, $this->year, 'TB', 0, 'L', true);
        $this->Cell(0, 6, 'foglio n' . iconv('UTF-8', 'windows-1252', html_entity_decode("&deg;")) . ' ' . $this->PageNo() . ' di {nb}', 'TBR', 1, 'L', true);
        $this->Ln(2);


        $w = 190;
        $wm = $w / 2;
        $h = 9;
        $a = 15;

        $this->Cell($a, $h, 'CLIENTE:', 'LTB', 0, 'L', true);
        $this->Cell($wm - $a, $h, $this->cliente, 'TBR', 0, 'L', true);
        $b = 30;
        $this->Cell($b, $h, 'INCARICATO G.S.T.:', 'LTB', 0, 'L', true);
        $this->Cell($wm - $b, $h,  $this->incaricato, 'TBR', 1, 'L', true);
        $c = 30;
        $this->Cell($c, $h, 'RESPONSABILE:', 'LTB', 0, 'L', true);
        $this->Cell($wm - $c, $h, $this->responsabile, 'TBR', 0, 'L', true);
        $d = 40;
        $this->Cell($d, $h, 'FIGURA PROFESSIONALE:', 'LTB', 0, 'L', true);
        $this->Cell($wm - $d, $h, '-', 'TBR', 1, 'L', true);

        $this->Ln(2);
    }

    // Page footer
    function Footer() {
        // Position at 1.5 cm from bottom
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        // Page number
        //$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
        // Move to the right
        //$this->Cell(80);
        $str = iconv('UTF-8', 'windows-1252', html_entity_decode("&copy;"));
        $this->Cell(0, 10, $str . ' G.S.T. Srl - Riproduzione vietata ', 0, 0, 'L');
        $this->Cell(0, 10, 'Mass1.1 del 02/10/2013 ', 0, 0, 'R');
    }

    // Colored table
    function FancyTable($header, $data) {
        // Colors, line width and bold font
        //$this->SetFillColor(255, 0, 0);
        //$this->SetTextColor(255);
        //$this->SetDrawColor(128, 0, 0);
        $this->SetTextColor(0, 0, 0);
        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(230, 230, 230);


        $this->SetLineWidth(.3);
        $this->SetFont('', 'B');
        // Header
        $w = array(20, 25, 20, 95, 30);
        for ($i = 0; $i < count($header); $i++) {
            $this->Cell($w[$i], 5, $header[$i], 1, 0, 'C', true);
        }
        $this->Ln();
        // Color and font restoration
        //$this->SetFillColor(224, 235, 255);
        //$this->SetTextColor(0);
        $this->SetFont('');
        // Data
        $fill = false;
        $contarighe=0;
        foreach ($data as $row) {
            $contarighe++;
            $this->Cell($w[0], 5, $row[0], 'LR', 0, 'C', $fill);
            
            //$orelav='8';
            //if((intval($row[1]))>8){
            //    $orelav='8'.' + '.(intval($row[1])-8);    
            //}else{
                $orelav=$row[1];
            //}
                
            $this->Cell($w[1], 5, $orelav, 'LR', 0, 'C', $fill);
            $this->Cell($w[2], 5, $row[2], 'LR', 0, 'C', $fill);
            $this->Cell($w[3], 5, iconv('UTF-8', 'windows-1252',html_entity_decode($row[3], ENT_QUOTES)), 'LR', 0, 'L', $fill);
            $this->Cell($w[4], 5, $row[4], 'LR', 0, 'C', $fill);
            $this->Ln();
            $fill = !$fill;
            if($this->needNewPageContinuous($contarighe)){
                $contarighe=0; 
            }
        }
       
        $contarighe++;
        // Closing line
        $this->SetFont('', 'B');
        $this->Cell($w[0], 5, 'TOTALE', 'TLRB', 0, 'C', $fill);
        $this->Cell($w[1], 5, '', 'TLRB', 0, 'C', $fill);
        $this->Cell($w[2], 5, 'GIORNI: '.$this->totGiorni, 'TLRB', 0, 'C', $fill);
        $this->Cell($w[3], 5, 'ORE: '.$this->totOre, 'TLRB', 0, 'C', $fill);
        $this->Cell($w[4], 5, '', 'TLRB', 0, 'C', $fill);
        $this->Ln();
        $contarighe++;
        if($this->needNewPageBlock($contarighe)){
            $contarighe=0;
        }
            
    }
    function needNewPageContinuous($rownumber){
        if($rownumber>35){
            $this->AddPage();
            return TRUE;
        }
        return FALSE;
    }
    function needNewPageBlock($rownumber){
        if($rownumber>30){
            $this->AddPage();
            return TRUE;
        }
        return FALSE;
    }
    
    function LegendaTable() {
        $this->SetTextColor(0, 0, 0);
        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(230, 230, 230);

        $h = 5;
        $w = 8;
        $a = 30;
        $b = 22;
        $c = 31;
        $d = 23;
        $e = 23;
        $f = 20;
        $this->SetFont('Arial', 'B', 8);
        $this->SetFillColor(255, 255, 255);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell($a, $h, 'LEGENDA ATTIVIT' . iconv('UTF-8', 'windows-1252', html_entity_decode("&Agrave;")) . ':', 0, 0, 'L', true);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell($w, $h, 'PG', 0, 0, 'C', true);
        $this->SetFont('Arial', 'B', 6);
        $this->Cell($b, $h, 'PROGETTAZIONE', 0, 0, 'L', true);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell($w, $h, 'AN', 0, 0, 'C', true);
        $this->SetFont('Arial', 'B', 6);
        $this->Cell($c, $h, 'ANALISI', 0, 0, 'L', true);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell($w, $h, 'PR', 0, 0, 'C', true);
        $this->SetFont('Arial', 'B', 6);
        $this->Cell($d, $h, 'PROGRAMMAZIONE', 0, 0, 'L', true);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell($w, $h, 'AS', 0, 0, 'C', true);
        $this->SetFont('Arial', 'B', 6);
        $this->Cell($e, $h, 'ASSISTENZA', 0, 0, 'L', true);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell($w, $h, 'TE', 0, 0, 'C', true);
        $this->SetFont('Arial', 'B', 6);
        $this->Cell($f, $h, 'TEST', 0, 1, 'L', true);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell($a, $h, '');
        $this->Cell($w, $h, 'IN', 0, 0, 'C', true);
        $this->SetFont('Arial', 'B', 6);
        $this->Cell($b, $h, 'INSTALLAZIONE', 0, 0, 'L', true);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell($w, $h, 'AP', 0, 0, 'C', true);
        $this->SetFont('Arial', 'B', 6);
        $this->Cell($c, $h, 'AVVIAMENTO PROCEDURA', 0, 0, 'L', true);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell($w, $h, 'DO', 0, 0, 'C', true);
        $this->SetFont('Arial', 'B', 6);
        $this->Cell($d, $h, 'DOCUMENTAZIONE', 0, 0, 'L', true);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell($w, $h, 'AD', 0, 0, 'C', true);
        $this->SetFont('Arial', 'B', 6);
        $this->Cell($e, $h, 'ADDESTRAMENTO', 0, 0, 'L', true);
        $this->SetFont('Arial', 'B', 8);
        $this->Cell($w, $h, 'VA', 0, 0, 'C', true);
        $this->SetFont('Arial', 'B', 6);
        $this->Cell($f, $h, 'VARIE', 0, 0, 'L', true);
    }

    function SignAndNote() {
        $this->SetTextColor(0, 0, 0);
        $this->SetDrawColor(0, 0, 0);
        $this->SetFillColor(230, 230, 230);
        $this->SetFont('Arial', 'B', 8);

        
        $w = 190;
        $wm = $w / 2;
        $h= 10;
        $this->Cell($wm, 10, 'Data ' . $this->dataCreazione, 'LTR', 0, 'L', FALSE);
        $Xp=$this->GetX();
        $Yp=$this->GetY();
        $this->Ln();
        $this->Cell($wm, 10, 'Firma Cliente ________________________________________ ' , 'LR', 1, 'L', FALSE);
        $this->Cell($wm, 15, 'Firma Incaricato G.S.T. ________________________________' , 'LBR', 0, 'L', FALSE);
        $this->SetY($Yp+0);
        $this->SetX($Xp);
        $this->MultiCell($wm, 5,  'Note_______________________________________________________'
                                . '___________________________________________________________'
                                . '___________________________________________________________'
                                . '___________________________________________________________'
                                . '___________________________________________________________'
                                . '___________________________________________________________'
                                . '___________________________________________________________'
                                , 1,  'J', FALSE);

       
    }

    var $month;
    var $year;
    var $cliente;
    var $incaricato;
    var $responsabile;
    var $dataCreazione;
    var $totGiorni;
    var $totOre;
  
    
    function SetMonth($month) {
        $this->month = $month;
    }
    
    function SetYear($year){
        $this->year=$year;
    }
    
    function SetCliente($cliente){
        $this->cliente=$cliente;
    }
    
    function SetIncaricato($incaricato){
        $this->incaricato=$incaricato;
    }
    
    function SetResponsabile($responsabile){
        $this->responsabile=$responsabile;
    }
    
    function SetDataCreazione($dataCreazione){
        $this->dataCreazione=$dataCreazione;
    }
    
    function SetTotOre($totOre){
        $this->totOre=$totOre;
    }
    
    function SetTotGiorni($totGiorni){
        $this->totGiorni=$totGiorni;
    }
}


//{period: periodV, codUser: codUserV, codCustomer: codCustomerV, codOffice: codOfficeV, codResp: codRespV, submit: "Report"};

$period_GET         = is_valid_get_string('period');
$codOffice_GET      = is_valid_get_string('codOffice');
$codCustomer_GET    = is_valid_get_string('codCustomer');
$codUser_GET        = is_valid_get_string('codUser');
$codResp_GET        = is_valid_get_string('codResp');


$parametri = new Params();
$mysqlConn = new mysqliConnClass($parametri);
$mysqliConn = $mysqlConn->connect();

if ($period_GET['isSetted'] && $period_GET['isValid']){
    $pieces = explode("-", $period_GET['value']);
    
    $year=$pieces[0];// piece1
    $anno= intval($year); 
    
    $mons = array(1 => "GENNAIO", 2 => "FEBBRAIO", 3 => "MARZO", 4 => "APRILE", 5 => "MAGGIO", 6 => "GIUGNO", 7 => "LUGLIO", 8 => "AGOSTO", 9 => "SETTEMBRE", 10 => "OTTOBRE", 11 => "NOVEMBRE", 12 => "DICEMBRE");
    $month=$pieces[1]; // piece2;
    $monthi = intval($month);
    $mese =  $mons[$monthi];
}


$descrCust="";
if ($codCustomer_GET['isSetted'] && $codCustomer_GET['isValid']){
    $descrCust = queryClass::getDescrCust($mysqliConn, $codCustomer_GET['value']);
}

$descrResp="";
if ($codResp_GET['isSetted'] && $codResp_GET['isValid']){
    $descrResp = queryClass::getDescrResp($mysqliConn, $codResp_GET['value']);
}

$descrUsr="";
if ($codUser_GET['isSetted'] && $codUser_GET['isValid']){
    $descrUsr = queryClass::getDescrUsr($mysqliConn, $codUser_GET['value']);
}

$activityList=  queryClass::listActivitiesRapp($mysqliConn,$codUser_GET['value'],$codOffice_GET['value'],$codCustomer_GET['value'], $codResp_GET['value'],$anno,$month);

$mysqlConn->disconnect();

$d = new DateTime( $year.'-'.$month.'-01' ); 
$ds=$d->format( 't.m.Y' );


// Instanciation of inherited class
$pdf = new PDF();

$pdf->SetMonth($mese);
$pdf->SetYear($anno);
$pdf->SetCliente($descrCust);
$pdf->SetIncaricato($descrUsr);
$pdf->SetResponsabile($descrResp);
$pdf->SetDataCreazione($ds);
$pdf->SetTitle("Rapportino");
$pdf->SetAuthor("AUTH");

$pdf->AliasNbPages();
$pdf->AddPage();

$header = array('Giorno', 'Ore', 'Attivit' . iconv('UTF-8', 'windows-1252', html_entity_decode("&agrave;")), 'Descrizione', 'Offerta');
$data = array();
$counter = 0;
$giorni =0;
$ore=(float)0.00;
$oldDate='';
foreach ($activityList as $row) {
   
	$data[$counter] = array($row['activityDate'], $row['activityDuration'], $row['activityType'], $row['activityDescription'], $row['activityOfferta']);
    $counter++;
	if(!($row['activityDate']===$oldDate)){
		$giorni++;
	}
	$oldDate=$row['activityDate'];
    $ore= $ore+floatval($row['activityDuration']);
}
$pdf->SetTotGiorni($giorni);
$pdf->SetTotOre($ore);

$pdf->SetFont('Times', '', 10);
$pdf->FancyTable($header, $data);
$pdf->Ln();
$pdf->Ln(1);
$pdf->LegendaTable();
$pdf->Ln();
$pdf->Ln(1);
$pdf->SignAndNote();


ob_end_clean();
$pdf->Output('Rapportino.pdf', 'D');


?>