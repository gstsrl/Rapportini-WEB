<?php
require_once './SYS_loginClass.php';
require_once './SYS_paramClass.php';
require_once './SYS_mysqliConnClass.php';
require_once './SYS_queryClass.php';

$parametri = new Params();
$mysqlConn = new mysqliConnClass($parametri);
$mysqliConn = $mysqlConn->connect();
$authCHECK_LOGIN = loginClass::checkUser2($mysqliConn);
if ($authCHECK_LOGIN) {

    $usernameCHECK_ROLES = loginClass::getUidUser();
    $res = queryClass::checkUserRoleOnDb($mysqliConn, $usernameCHECK_ROLES);
    $mysqlConn->disconnect();
    ?>
    <!DOCTYPE html>
    <!--[if IE 8]><html class="no-js lt-ie9" lang="en" ><![endif]-->
    <!--[if gt IE 8]><!--><html class="no-js" ><!--<![endif]-->
        <html>
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">

                <title>Aggiunta Office</title>
                <!-- Fogli di stile -->
                <link href="css/bootstrap.css" rel="stylesheet" media="screen">
                <link href="css/bootstrap-table.min.css" rel="stylesheet" media="screen">
                <link href="css/stili-custom.css" rel="stylesheet" media="screen">
                <!-- Modernizr -->
                <script src="js/modernizr.custom.js"></script>
                <!-- respond.js per IE8 -->
                <!--[if lt IE 9]>
                <script src="js/respond.min.js"></script>
                <![endif]-->
            </head>
            <body>
                <nav class="navbar navbar-default navbar-static-top">
                    <div class="container">
                        <ol class="breadcrumb">
                            <li><a href="menu.php"> Menu</a></li>
                            <li><a href="responsibleManage.php"> Gestione Responsabili</a></li>
                            <li class="active">Aggiunta Responsabili</li>
                        </ol>
                    </div>
                </nav>
                <!--<div class="wrapper ">-->
                <div class="container">

                    <div class="row vertical-center-row">   


                        <?php
                        if ($res['isEnabledCHECK_ROLES'] && $res['isAdminCHECK_ROLES']) {
                            ?>         


                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="panel panel-default">        
                                    <div class="panel-heading">Add Responsible</div>
                                    <div class="panel-body">
                                        <form id="addResponsibleForm" accept-charset="UTF-8" role="form" class="form-horizontal">
                                            <fieldset>
                                                <!-- Form Name -->
                                                <!--<legend>Form Name</legend>-->
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" for="codCustomer">codCustomer</label>  
                                                    <div class="col-md-4">
                                                        <select id="codCustomer" name="codCustomer" type="text" placeholder="codCustomer" class="form-control input-md" required="">
                                                        </select>
                                                        <span class="help-block">Insert codCustomer</span>  
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" for="codOffice">codOffice</label>  
                                                    <div class="col-md-4">
                                                        <select id="codOffice" name="codOffice" type="text" placeholder="codOffice" class="form-control input-md" required="">
                                                        </select>
                                                        <span class="help-block">Insert codOffice</span>  
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" for="codResp">codResp</label>  
                                                    <div class="col-md-4">
                                                        <input id="codResp" name="codResp" type="text" placeholder="codResp" class="form-control input-md" required="">
                                                        <span class="help-block">Insert codResp</span>  
                                                        <span id="codRespUsed" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> codRespUsed
                                                    </div>
                                                </div>

                                                <!-- Text input-->
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" for="descrResp">descrResp</label>
                                                    <div class="col-md-4">
                                                        <input id="descrResp" name="descrResp" type="text" placeholder="descrResp" class="form-control input-md" required="">
                                                        <span class="help-block">descrResp</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" for="createUser">createUser</label>
                                                    <div class="col-md-4 checkbox">
                                                        <label><input id="createUser" name="createUser" type="checkbox" value="true" checked="checked">createUser</label>
                                                    </div>
                                                </div>
                                                <!-- Button (Double) -->
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" for="Submit"></label>
                                                    <div class="col-md-8">
                                                        <button id="Submit" type="submit"  name="Submit" value="Submit" class="btn btn-primary">Submit</button>
                                                        <button id="Clear"  type="reset"   name="Reset" value="Reset" class="btn btn-default">Reset</button>
                                                    </div>
                                                </div>

                                            </fieldset>
                                        </form>


                                    </div>
                                </div>

                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12" >
                                <div id="le-alert" class="alert alert-warning alert-block fade" role="alert">
                                    <button type="button" class="close">&times;</button>
                                    <h4 id="warnTtl" ></h4>
                                    <p id="warnMsg" ></p>
                                    <p id="warnMsgCause" ></p>
                                    <p id="warnMsgCod" ></p>
                                </div>
                            </div>
                            <?php
                        } else {
                            ?>                    
                            <p>Utente non abilitato</p>    
                            <?php
                        }
                        ?>                
                    </div>
                </div>
                <!-- jQuery e plugin JavaScript  -->
                <script src="js/jquery-2.1.3.min.js"></script>
                <script src="js/i18next-1.7.7.min.js"></script>
                <script src="js/bootstrap.min.js"></script>
                <script src="js/bootstrap-table.min.js"></script>
                <script src="js/jsscript/SYS_boot_Alert.js"></script>
                <script src="js/translation/responsibleAddT.js"></script>
                <script src="js/jsscript/responsibleAdd.js"></script>
                <script src="js/jquery.chained.js"></script>
            </body>
        </html>         
        <?php
    } else {
        $mysqlConn->disconnect();
        header("Location: logout.php");
    }
    ?>


