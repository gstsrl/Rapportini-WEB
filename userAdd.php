
<?php
require_once './SYS_loginClass.php';
require_once './SYS_paramClass.php';
require_once './SYS_mysqliConnClass.php';
require_once './SYS_queryClass.php';

$parametri = new Params();
$mysqlConn = new mysqliConnClass($parametri);
$mysqliConn = $mysqlConn->connect();
$authCHECK_LOGIN = loginClass::checkUser2($mysqliConn);
if ($authCHECK_LOGIN) {

    $usernameCHECK_ROLES = loginClass::getUidUser();
    $res = queryClass::checkUserRoleOnDb($mysqliConn, $usernameCHECK_ROLES);
    $mysqlConn->disconnect();
    ?>
    <!DOCTYPE html>
    <!--[if IE 8]><html class="no-js lt-ie9" lang="en" ><![endif]-->
    <!--[if gt IE 8]><!--><html class="no-js" ><!--<![endif]-->
        <html>
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">

                <title>Menu Rapportini</title>
                <!-- Fogli di stile -->
                <link href="css/bootstrap.css" rel="stylesheet" media="screen">
                <link href="css/stili-custom.css" rel="stylesheet" media="screen">
                <!-- Modernizr -->
                <script src="js/modernizr.custom.js"></script>
                <!-- respond.js per IE8 -->
                <!--[if lt IE 9]>
                <script src="js/respond.min.js"></script>
                <![endif]-->
            </head>
            <body>
                <nav class="navbar navbar-default navbar-static-top">
                    <div class="container">
                        <ol class="breadcrumb">
                            <li><a href="menu.php"> Menu</a></li>
                            <li class="active">Gestione Rapportini</li>

                        </ol>
                    </div>
                </nav>
                <!--<div class="wrapper ">-->
                <div class="container">

                    <div class="row vertical-center-row">   


                        <?php
                        if ($res['isEnabledCHECK_ROLES'] && $res['isAdminCHECK_ROLES']) {
                            ?>                        
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">Add User</div>
                                    <div class="panel-body">
                                        <form id="addUserForm" accept-charset="UTF-8" role="form" class="form-horizontal">
                                            <fieldset>

                                                <!-- Form Name -->
                                                <!--                                                <legend>Form Name</legend>-->

                                                <!-- Text input-->
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" for="username">Username</label>  
                                                    <div class="col-md-4">
                                                        <input id="username" name="username" type="text" placeholder="Username" class="form-control input-md" required="">
                                                        <span class="help-block">Insert Username</span>  
                                                        <span id="codUserUsed" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> codUserUsed
                                                    </div>
                                                </div>

                                                <!-- Password input-->
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" for="pass">Password</label>
                                                    <div class="col-md-4">
                                                        <input id="pass" name="pass" type="password" placeholder="Password" class="form-control input-md" required="">
                                                        <span class="help-block">User password</span>
                                                    </div>
                                                </div>

                                                <!-- Password input-->
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" for="pass2">Password Confirm</label>
                                                    <div class="col-md-4">
                                                        <input id="pass2" name="pass2" type="password" placeholder="Password Confirm" class="form-control input-md" required="">
                                                        <span class="help-block">Confirm password</span>
                                                        <span id="pwmatch" class="glyphicon glyphicon-remove" style="color:#FF0004;"></span> Passwords Match
                                                    </div>
                                                </div>




                                                <!-- Multiple Checkboxes -->
                                                <!--                                                <div class="form-group">
                                                                                                    <label class="col-md-4 control-label" for="checkboxes">User Role</label>
                                                                                                    <div class="col-md-4">
                                                                                                        <div class="checkbox">
                                                                                                            <label for="isAdmin">
                                                                                                                <input type="checkbox" name="checkboxes" id="isAdmin" value="true">
                                                                                                                Admin role
                                                                                                            </label>
                                                                                                        </div>
                                                                                                        <div class="checkbox">
                                                                                                            <label for="isUser">
                                                                                                                <input type="checkbox" name="checkboxes" id="isUser" value="true" checked>
                                                                                                                User role
                                                                                                            </label>
                                                                                                        </div>
                                                                                                        <div class="checkbox">
                                                                                                            <label for="isAccount">
                                                                                                                <input type="checkbox" name="checkboxes" id="isAccount" value="true">
                                                                                                                Account role
                                                                                                            </label>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>-->

                                                <!-- Multiple Checkboxes (inline) -->
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" for="checkboxes">User Role</label>
                                                    <div class="col-md-4">
                                                        <label class="checkbox-inline" for="isAdmin">
                                                            <input type="checkbox" name="isAdmin" id="isAdmin" value="true">
                                                            Admin role
                                                        </label>
                                                        <label class="checkbox-inline" for="isUser">
                                                            <input type="checkbox" name="checkboxes" id="isUser" value="true" checked>
                                                            User role
                                                        </label>
                                                        <label class="checkbox-inline" for="isAccount">
                                                            <input type="checkbox" name="checkboxes" id="isAccount" value="true">
                                                            Account role
                                                        </label>
                                                    </div>
                                                </div>

                                                <!-- Button (Double) -->
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" for="Submit"></label>
                                                    <div class="col-md-8">
                                                        <button id="Submit" type="submit"  name="Submit" value="Submit" class="btn btn-primary">Submit</button>
                                                        <button id="Clear"  type="reset"   name="Reset" value="Reset" class="btn btn-default">Reset</button>
                                                    </div>
                                                </div>

                                            </fieldset>
                                        </form>


                                    </div>
                                </div>

                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12" >
                                <div id="le-alert" class="alert alert-warning alert-block fade" role="alert">
                                    <button type="button" class="close">&times;</button>
                                    <h4 id="warnTtl" ></h4>
                                    <p id="warnMsg" ></p>
                                    <p id="warnMsgCause" ></p>
                                    <p id="warnMsgCod" ></p>
                                </div>
                            </div>
                            <?php
                        } else {
                            ?>                    
                            <p>Utente non abilitato</p>    
                            <?php
                        }
                        ?>                
                    </div>
                </div>
                <!-- jQuery e plugin JavaScript  -->
                <script src="js/jquery-2.1.3.min.js"></script>
                <script src="js/i18next-1.7.7.min.js"></script>
                <script src="js/bootstrap.min.js"></script>
                <script src="js/jsscript/SYS_boot_Alert.js"></script>
                <script src="js/translation/userAddT.js"></script>
                <script src="js/jsscript/userAdd.js"></script>

            </body>
        </html>         
        <?php
    } else {
        $mysqlConn->disconnect();
        header("Location: logout.php");
    }
    ?>
